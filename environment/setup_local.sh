#!/bin/bash
set -euo pipefail

function set_env() {
    # Check running OS
    # Reference: https://stackoverflow.com/questions/3466166/how-to-check-if-running-in-cygwin-mac-or-linux
    case "$(uname -s)" in
        Linux*)     OS=Linux;;
        Darwin*)    OS=Mac;;
        CYGWIN*)    OS=Cygwin;;
        MINGW*)     OS=MinGw;;
        *)          OS="UNKNOWN"
    esac

    SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
    PROJECT_DIR=`cd "${SCRIPT_DIR}" && cd .. && pwd`
    VIRTUALENV_NAME="venv"
    VIRTUALENV_DIR="${PROJECT_DIR}/${VIRTUALENV_NAME}"
    SUPPORTED_PYTHON_VERSIONS="3.6 3.7"

    PYTHONPATH="$(cd ${PROJECT_DIR} && python -c 'import os; print(os.getcwd())')"  # Use python to get correct path in Windows
    DATA_DIR="${PROJECT_DIR}/data"
    #GIT_HASH=`git rev-parse --verify HEAD`
    # TODO: Get this value programmatically
    PROPER_GTK_FOLDER="C:\Program Files\GTK3-Runtime Win64\bin"
}

function install_python36() {
    git clone https://github.com/pyenv/pyenv.git ~/.pyenv

    # Update bashrc
    echo -e '\n# pyenv' >> ~/.bashrc
    echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
    echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
    echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n  eval "$(pyenv init -)"\nfi' >> ~/.bashrc

    export PYENV_ROOT="$HOME/.pyenv"
    export PATH="$PYENV_ROOT/bin:$PATH"
    eval "$(pyenv init -)"
    pyenv install 3.6.8
    pyenv global 3.6.8
}

function check_python_version() {
    PYTHON_VERSION=$(python -c "import sys;t='{v[0]}.{v[1]}'.format(v=list(sys.version_info[:2]));sys.stdout.write(t)";)
    if [[ ! $(echo "${SUPPORTED_PYTHON_VERSIONS}" | grep ${PYTHON_VERSION}) ]]; then
        echo "ERROR: Python ${PYTHON_VERSION} is not in the supported versions (${SUPPORTED_PYTHON_VERSIONS})."
        install_python36
    fi
}

function create_virtual_environment() {
    if [ ! -d "${VIRTUALENV_DIR}" ]; then
        echo "Creating virtual environment..."
        cd "${PROJECT_DIR}"
        python -m pip install virtualenv
        python -m virtualenv ${VIRTUALENV_NAME}
        cd -
    fi
}

function activate_virtual_environment() {
    if [ ${OS} == "Linux" ] || [ ${OS} == "Mac" ]; then
        source ${VIRTUALENV_DIR}/bin/activate
        echo "Linux/Mac virtualenv successfully loaded"
    else
        source ${VIRTUALENV_DIR}/Scripts/activate
        echo "Windows virtualenv successfully loaded"
    fi
}

function install_non_pip_dependencies() {
    # C++ distributable (https://scikit-learn.org/dev/developers/advanced_installation.html#linux-compilers-from-the-system)
    sudo apt-get install -y build-essential

    # GTK+ library (https://weasyprint.readthedocs.io/en/stable/install.html#debian-ubuntu)
    sudo apt-get install -y build-essential python3-dev python3-pip python3-setuptools python3-wheel python3-cffi libcairo2 libpango-1.0-0 libpangocairo-1.0-0 libgdk-pixbuf2.0-0 libffi-dev shared-mime-info
}

function install_pip_dependencies() {
    pip install -r ${PROJECT_DIR}/environment/requirements.txt
}

function export_var() {
    eval "export $1=\$2"
    echo "$1=$2"
}

function export_vars() {
    export_var PYTHONPATH        ${PYTHONPATH}
    export_var DATA_DIR          ${DATA_DIR}
    #export_var GIT_HASH          ${GIT_HASH}
    export_var PROPER_GTK_FOLDER ${PROPER_GTK_FOLDER}
}

function setup_jupyter_kernel() {
    # This step is mainly for Jupyter setup on Virtual Machine since we use Jupyter hub which does not start with a bash script.

    # Create kernel json
    pip install ipykernel
    python -m ipykernel install --user --name=${VIRTUALENV_NAME}

    # Set env vars in kernel json
    # Reference: https://stackoverflow.com/a/53595397
    local kernel_json_file="${HOME}/.local/share/jupyter/kernels/${VIRTUALENV_NAME}/kernel.json"
    local cmd=`cat <<EOF
import os
import json

with open("${kernel_json_file}") as f:
    kernel_json = json.load(f)
    kernel_json["env"] = dict(os.environ)

with open("${kernel_json_file}", "w") as f:
    json.dump(kernel_json, f, indent=4)
EOF`
    python -c "$cmd"
}


# ----- Main ----- #
set_env
check_python_version
create_virtual_environment
activate_virtual_environment

# Only run this for the first installation; multiple users run this at the same time will cause lock issue.
#if [ ${OS} == "Linux" ]; then
#    install_non_pip_dependencies
#fi

install_pip_dependencies
export_vars
if [ ${OS} == "Linux" ]; then
    setup_jupyter_kernel
fi

set +euo pipefail
