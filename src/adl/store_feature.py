import os
import re
import sys
import datetime
import numpy as np
import pandas as pd

from src import config

def gen_external_feature():
    print("| -- gen external features")
    #df = pd.read_csv(os.path.join(config.external_dir, "210225_sephora_external_features_master.csv"))
    df = pd.read_excel(os.path.join(config.external_dir, "210301_sephora_external_features_master.xlsx"), engine="openpyxl")
    print(df.shape)
    return df

def gen_store_features():
    print("| -- gen store features")
    df = pd.read_excel(
        os.path.join(config.ref_dir, "dim_stores_offline_v3.xlsx"), engine="openpyxl"
    )
    today = datetime.datetime.now().strftime("%Y-%m-%d")
    df["close_date"].fillna(today, inplace=True)
    for col in ["open_date", "close_date"]:
        df[col] = pd.to_datetime(df[col], format="%Y-%m-%d")
    df["open_days"] = (df["close_date"] - df["open_date"]).apply(lambda x: x.days)
    df["open_months"] = df["open_days"]/30
    return df

# def gen_sales_features():
#     df = pd.read_csv(os.path.join(config.sdl_dir, "txn_store_category_monthly_v2.csv.gz"))
#     df_cn = df[df["cn_offline"]==1]
#     df_cn_agg = df_cn.groupby()

def gen_traffic_features():
    print("| -- gen traffic features")
    df_traffic_raw = pd.read_csv(os.path.join(config.sdl_dir, "traffic_sdl.csv"))
    df_traffic = df_traffic_raw[~df_traffic_raw["month"].isin(["2020-02", "2020-03"])]
    df_traffic_agg = df_traffic.groupby(["store_code", "year"], as_index=False)[
        ["customer_id", "traffic_combine"]
    ].sum()
    df_traffic_agg["conversion_yearly"] = df_traffic_agg["customer_id"] / df_traffic_agg["traffic_combine"]

    df_traffic_pivot = df_traffic_agg.pivot_table(index=["store_code"], values=["conversion_yearly"], columns=["year"]).reset_index()
    df_traffic_pivot.columns = ["store_code", "conversion_2018", "conversion_2019", "conversion_2020"]
    print(df_traffic_pivot.shape)
    return df_traffic_pivot

def merge_all_features():
    df_store = gen_store_features()
    df_ext = gen_external_feature()
    df_traffic_agg = gen_traffic_features()
    df_feature = df_store.merge(df_ext, on=["store_id"], how="left").merge(df_traffic_agg, on=["store_code"], how="left")
    print("| -- all features merged")

    # sales
    #df_target = gen_sales_features()

    return df_feature #, df_target

if __name__=='__main__':
    df_feature = merge_all_features()
    df_feature.to_csv(os.path.join(config.adl_dir, "store_level_features.csv"), index=False)
    #df_target.to_csv(os.path.join(config.adl_dir, "store_level_sales.csv"), index=False)
    