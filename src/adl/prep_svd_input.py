import os
import sys
import datetime
import numpy as np
import pandas as pd

from src import config
from src.utils import io

def read_promo_product():
    df = pd.read_excel(os.path.join(config.ref_dir, "promo_dim_product.xlsx"), engine="openpyxl")
    return df

def read_all_product():
    df = pd.read_excel(os.path.join(config.ref_dir, "dim_product_with_txn.xlsx"), engine="openpyxl")
    return df

def agg_sales(df, df_product, df_promo_product):

    merge_keys = ["product_id", "brand", "category", "Range", "Segment"]
    #grp_keys = ["customer_id", "brand", "category"]
    grp_keys = ["customer_id", "brand"]

    df_promo_product["ind_promo"] = 1
    df_filter = df.merge(df_promo_product[["product_id", "ind_promo"]], on=["product_id"], how="left")
    df_filter = df_filter[df_filter["ind_promo"]!=1]
    print("| -- promo sku excluded...")
    df_filter_merge = df_filter.merge(df_product[merge_keys], on=["product_id"], how="left")

    df_filter_agg = df_filter_merge.groupby(grp_keys, as_index=False)[["qty", "sales"]].sum()
    df_filter_agg = df_filter_agg[df_filter_agg["customer_id"]!=0]
    
    df_product_list = df_filter_agg[grp_keys[1:]].drop_duplicates()
    print("| -- sales aggregation done...")

    return df_filter_agg, df_product_list

def filter_offline(df):
    dim_store = io.read_excel(os.path.join(config.ref_dir, "dim_stores_offline_v3.xlsx"))
    offline_stores = dim_store["store_id"].unique()
    df_filter = df[df["store_id"].isin(offline_stores)]
    print("| -- offline stores filtered...")
    return df_filter

def run_one_year(year, out_dir):
    year = str(year)
    sel_cols = ["store_id", "customer_id", "product_id", "sales", "qty"]

    df_product = read_all_product()
    df_promo_product = read_promo_product()

    df =  pd.read_parquet("/data/raw_processed/txn_{}_clean.parquet".format(year))[sel_cols]
    df = filter_offline(df)
    df_agg, df_product_list = agg_sales(df, df_product, df_promo_product)
    out_path = out_dir + "/cm_svd_input_raw_{}.csv".format(year)
    df_agg.to_csv(out_path, index=False)
    os.system("gzip {}".format(out_path))
    df_product_list.to_csv(out_dir + "/product_list_{}.csv".format(year), index=False)
    print("| -- run for year {} done...".format(year))

def run_combine_sample(year_lst, out_dir):
    all_p = []
    all_cm = []
    for year in year_lst:
        df_cm = pd.read_csv(out_dir + "/cm_svd_input_raw_{}.csv.gz".format(year))
        df_product_list = pd.read_csv(out_dir + "/product_list_{}.csv".format(year))
        all_cm.append(df_cm)
        all_p.append(df_product_list)

    df_all_p = pd.concat(all_p).drop_duplicates()
    df_all_p["product_key"] = df_all_p.index

    df_all_cm = pd.concat(all_cm)
    df_all_cm_agg = df_all_cm.groupby(["customer_id", "brand"], as_index=False)[["sales", "qty"]].sum()
    df_all_cm_unique = df_all_cm_agg[["customer_id"]].drop_duplicates()
    df_cm_sample = df_all_cm_unique.sample(frac=0.2)
    df_sample = df_all_cm_agg.merge(df_cm_sample, on=["customer_id"]).merge(df_all_p, on=["brand"])

    new_product_list = df_sample[["product_key"]].drop_duplicates()
    df_sample.to_csv(out_dir + "/cm_svd_input_sample.csv", index=False)
    new_product_list.to_csv(out_dir + "/product_list.csv", index=False)

if __name__=='__main__':
    out_dir = os.path.join(config.temp_dir, "xingyu", "svd")
    if sys.argv[1] == "year":
        run_one_year(sys.argv[2], out_dir)
    else:
        year_lst = sys.argv[2].split(",")
        run_combine_sample(year_lst, out_dir)