import os
import sys
import numpy as np
import pandas as pd

from sklearn.preprocessing import MinMaxScaler
from src import config

def read_txn(year):
    year = str(year).split(",")
    all_df = []
    for yr in year:
        df = pd.read_parquet(os.path.join(config.raw_processed, "txn_{}_clean.parquet".format(yr)))
        sel_cols =["store_id", "customer_id", "product_id", "sales", "qty"]
        df = df[sel_cols]
        all_df.append(df)
        print("| -- read txn for year {}".format(year))
    
    df_out = pd.concat(all_df)
    return df

def filter_txn(df):
    df_product = pd.read_excel(os.path.join(config.ref_dir, "dim_product_with_txn.xlsx"), engine="openpyxl")
    category_map = config.svd_category_map

    df_product["new_category"] = df_product["category"].apply(lambda x: category_map.get(x, x))
    
    df_merge = df.merge(df_product[["product_id", "brand", "category", "new_category", "Range", "Segment"]], on=["product_id"])
    df_merge_filter = df_merge[
        (df_merge["sales"]>1)&
        (df_merge["customer_id"]!=0)
    ]
    
    dim_store = pd.read_excel(os.path.join(config.ref_dir, "dim_stores_offline_v5.xlsx"), engine="openpyxl")
    dim_store = dim_store[pd.isnull(dim_store["close_date"])]
    dim_store = dim_store[dim_store["open_date"]<="2020-12-01"]
    print("| -- filtered stores {}...".format(dim_store["store_id"].nunique()))

    df_master = df_merge_filter[df_merge_filter["store_id"].isin(dim_store["store_id"].unique())]
    sel_cat = ["MAKE UP", "SKINCARE", "ACCESSORIES", "BATH_WELLNESS", "FRAGRANCE", "HAIR"]
    df_master = df_master[df_master["new_category"].isin(sel_cat)]
    print("| -- filtered category...")

    df_master = df_master[~df_master["brand"].isin(["LANCOME", "LAUDER", "DIOR"])]
    print("| -- filtered out LED")

    return df_master

def gen_train_score(df_master, tgt, train_sample=0.5):
    grp_keys = ["brand", "new_category"]
    df_agg1 = df_master[["store_id"] + grp_keys].drop_duplicates()
    df_agg1 = df_agg1.groupby(["store_id"], as_index=False)[["new_category"]].count()

    coverage_cutoff = df_agg1['new_category'].quantile(.75)
    sel_stores = df_agg1[df_agg1["new_category"]>=coverage_cutoff]["store_id"].unique()

    df_master = df_master[df_master["store_id"].isin(sel_stores)]

    df_agg = df_master.groupby(["customer_id"] + grp_keys, as_index=False)[["qty", "sales"]].sum()
    print("| -- total # of items before filtering: {}".format(len(df_agg[grp_keys].drop_duplicates())))
    df_cnt_items = df_agg.groupby(["customer_id"], as_index=False)[["qty"]].count()

    threshold = 5 #df_cnt_items["qty"].quantile(.7)
    print("| -- filter customers purchased more than {} items...".format(threshold))

    df_sel_cm = df_cnt_items[df_cnt_items["qty"] >= threshold][["customer_id"]]
    df_sel_cm_sample = df_sel_cm.sample(frac=train_sample, random_state=0)
    df_agg_filter = df_agg.merge(df_sel_cm_sample, on=["customer_id"])
    print("| -- total # of items before filtering: {}".format(len(df_agg_filter[grp_keys].drop_duplicates())))

    product_list = df_agg_filter[grp_keys].drop_duplicates().reset_index(drop=True)
    product_list["product_key"] = product_list.index

    df_train = df_agg_filter.merge(product_list, on=grp_keys)
    
    print("| -- scale target {}...".format(tgt))
    df_train["log_{}".format(tgt)] = np.log(df_train[tgt]+1)

    scaler = MinMaxScaler(feature_range=(1,5))
    df_train["log_{}_scale".format(tgt)] = scaler.fit_transform(df_train[["log_{}".format(tgt)]].values)

    df_score = df_agg.merge(product_list, on=grp_keys)
    df_score["log_{}".format(tgt)] = np.log(df_score[tgt]+1)
    df_score["log_{}_scale".format(tgt)] = scaler.transform(df_score[["log_{}".format(tgt)]].values)

    return product_list, df_train, df_score

if __name__ == "__main__":
    if len(sys.argv) != 4:
        raise Exception("Wrong # of arguments. Usage: python3 svd_adl.py <year> <version> <target>")
    else:
        our_dir = os.path.join(config.temp_dir, "xingyu", "svd")
        version = sys.argv[2]
        df_txn = read_txn(sys.argv[1])
        df_master = filter_txn(df_txn)
        product_list, df_train, df_score = gen_train_score(df_master, sys.argv[3])
        product_list.to_csv(os.path.join(our_dir, "product_list_{}.csv".format(version)), index=False)

        train_data = os.path.join(our_dir, "svd_train_{}.csv".format(version))
        df_train.to_csv(train_data, index=False)
        os.system("gzip {}".format(train_data))

        score_data = os.path.join(our_dir, "svd_score_{}.csv".format(version))
        df_score.to_csv(score_data, index=False)
        os.system("gzip {}".format(score_data))
