import os
import sys
import datetime
import numpy as np
import pandas as pd
import warnings
warnings.filterwarnings("ignore")

from src import config
from src.utils.common import convert_int

def filter_dim_product():
    """
    Filter product dimension tables
    """
    df_txn = pd.read_parquet(os.path.join(config.sdl_dir, "txn_store_sku_monthly_v2.parquet"))
    df_p = df_txn.groupby(["product_id"], as_index=False)[["qty", "sales"]].sum()

    df_product = pd.read_csv(os.path.join(config.raw_dir, "DimProduct_20210223_V2.csv"), header=None, 
    names=['product_id', 'brand_type', 'brand', 'category', 'sku_code', 'target', 'product_name', 'product_name_en', 'Range', 'Segment', 'skincare_function', 'brand_code'],
    low_memory=False)
    df_product['product_id'] = df_product['product_id'].apply(lambda x: convert_int(x))

    df_sel = df_product.merge(df_p, on=['product_id'])
    df_sel.to_excel(os.path.join(config.ref_dir, "dim_product_with_txn.xlsx"), index=False, encoding="utf_8_sig")
    return df_sel

def filter_promo_product(df_product):
    """
    Filter for promo related products

    Args:
        df_product (:obj:`pandas.DataFrame`): raw product dimension dataframe

    Returns:
        df_promo: a Pandas DataFrame which contains promo related products
    """
    core_categories = [
        "MAKE UP ACCESSORIES",
        "MAKE UP",
        "HAIR ACCESSORIES",
        "HAIRCARE",
        "HAIR",
        "SKINCARE",
        "SKINCARE ACCESSORIES",
        "FRAGRANCE",
        "FRAGRANCE ACCESS"
    ]
    df_promo = df_product[
        ~df_product["category"].isin(core_categories)
    ]
    df_promo.to_excel(os.path.join(config.ref_dir, "promo_dim_product.xlsx"), index=False, encoding="utf_8_sig")
    return df_promo

if __name__ == "__main__":
    df_product = filter_dim_product()
    filter_promo_product(df_product)