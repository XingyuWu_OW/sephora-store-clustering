import os
import sys
import datetime
import numpy as np
import pandas as pd

from src import config

def reindex_by_date(df):
    dates = pd.date_range(start=df['date'].min(), end="2020-12-31")
    df = df.set_index('date')
    df_out = df.reindex(dates).fillna(method = "ffill")
    df_out = df_out.reset_index().rename(columns = {"index": "date"})
    
    return df_out

def traffic_sdl():
    dim_store = pd.read_excel(
        os.path.join(config.ref_dir, "dim_stores_offline_v5.xlsx"), engine="openpyxl"
    )[["store_id", "store_code", "open_date", "close_date", "region"]].drop_duplicates()

    df1 = pd.read_csv(os.path.join(config.raw_processed, "traffic_clean.csv"))
    df2 = pd.read_csv(os.path.join(config.raw_dir, "traffic_additional.csv"))
    df2.columns = ["date", "store_code", "traffic_new"]
    df2["date"] = pd.to_datetime(df2["date"], format="%Y%m%d").dt.strftime("%Y-%m-%d")

    df_txn = pd.read_csv(os.path.join(config.raw_processed, "txn_offline_clean_v2.csv"))
    df_master = df_txn.groupby(["store_id", "txn_date"])[["txn_id"]].nunique().reset_index()
    df_master = df_master.merge(dim_store, on=["store_id"]).rename(
        columns={"txn_date": "date", "txn_id": "count_txn"})

    df_master_traffic = df_master.merge(
        df1, on=["store_code", "date"],
        how="left"
    ).merge(
        df2, on=["store_code", "date"],
        how="left"
    )

    df_master_traffic["traffic_combine"] = df_master_traffic["traffic_new"].combine_first(df_master_traffic["traffic_in"])
    df_master_traffic["conversion_daily"] = np.where(
        df_master_traffic["traffic_combine"] > 0,
        df_master_traffic["count_txn"] / df_master_traffic["traffic_combine"],
        np.nan
    )

    dim_date = pd.read_csv(os.path.join(config.ref_dir, "dim_date.csv"))
    df_master_traffic = df_master_traffic.merge(dim_date, on=["date"], how="left")
    df_master_traffic.to_csv(os.path.join(config.sdl_dir, "traffic_sdl.csv"), index=False)

if __name__=='__main__':
    traffic_sdl()