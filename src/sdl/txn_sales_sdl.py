import os
import sys
import datetime
import numpy as np
import pandas as pd
import warnings
warnings.filterwarnings("ignore")

from src import config
from src.utils import common

def read_dim_date(filepath):
    df_date = pd.read_csv(filepath)
    df_date = df_date.rename(columns={"date": "txn_date"})
    return df_date

def read_txn(filepath):
    df = pd.read_parquet(filepath)

    return df

def get_cm_first_purchase(df):
    df_cm = df.groupby(["customer_id"], as_index=False)[["txn_date"]].min()
    df_cm = df_cm.rename(columns={"txn_date": "min_txn_date"})
    return df_cm

def txn_sku_daily_agg(df):
    """
    Aggregate transactions to daily level
    """
    df_agg = df.groupby(["txn_date", "store_id", "product_id"], as_index=False)[["qty", "sales"]].sum()
    
    return df_agg

def txn_sum(df_txn):
    """
    Aggregate transactions to daily, weekly and monthly level respectively
    """
    date_path = os.path.join(config.ref_dir, "dim_date.csv")
    df_date = read_dim_date(date_path)
    
    df_txn_daily = txn_sku_daily_agg(df_txn)

    df_txn_daily_merge = df_txn_daily.merge(df_date, on=["txn_date"], how="left")
    df_txn_weekly = df_txn_daily_merge.groupby(["weekstartdate", "store_id", "product_id"], as_index=False)[["qty", "sales"]].sum()
    
    df_txn_monthly = df_txn_daily_merge.groupby(["month", "store_id", "product_id"], as_index=False).agg({
        "qty": "sum",
        "sales": "sum",
        "year": "min",
        "quarter": "min"
    })

    print("| -- txn aggregation done...")
    return df_txn_daily, df_txn_weekly, df_txn_monthly

def gen_txn_sdl(out_dir):
    """
    Main function to generate transaction SDL
    """
    df1 = read_txn(os.path.join(config.raw_processed, "txn_2018_clean.parquet"))
    df2 = read_txn(os.path.join(config.raw_processed, "txn_2019_clean.parquet")) 
    df3 = read_txn(os.path.join(config.raw_processed, "txn_2020_clean.parquet"))

    df1 = common.reduce_size(df1)
    df2 = common.reduce_size(df2)
    df3 = common.reduce_size(df3)
    
    df1_daily, df1_weekly, df1_monthly = txn_sum(df1) 
    df2_daily, df2_weekly, df2_monthly = txn_sum(df2) 
    df3_daily, df3_weekly, df3_monthly = txn_sum(df3) 
    
    df_txn_daily = pd.concat([df1_daily, df2_daily, df3_daily])
    df_txn_weekly = pd.concat([df1_weekly, df2_weekly, df3_weekly])
    df_txn_monthly = pd.concat([df1_monthly, df2_monthly, df3_monthly])

    df_txn_daily.to_parquet(os.path.join(out_dir, "txn_store_sku_daily_v2.parquet"))
    df_txn_weekly.to_parquet(os.path.join(out_dir, "txn_store_sku_weekly_v2.parquet"))
    df_txn_monthly.to_parquet(os.path.join(out_dir, "txn_store_sku_monthly_v2.parquet"))

if __name__=='__main__':
    if len(sys.argv) == 1:
        out_dir = config.sdl_dir
    else:
        out_dir = sys.argv[1]
    gen_txn_sdl(out_dir) 
    
    

