import os
import sys
import datetime
import numpy as np
import pandas as pd

from src import config
from src.utils.common import convert_int

def load_premium_line():
    df = pd.read_excel(os.path.join(config.ref_dir, "Premium Line.xlsx"), sheet_name="P Line", engine="openpyxl", encoding="utf_8_sig")
    df_premium_line = df[["Brand", "Material"]].rename(columns={"Brand": "brand", "Material": "sku_code"})
    df_premium_line["ind_premium_line"] = 1
    df_premium_line["sku_code"] = df_premium_line["sku_code"].astype("str")
    return df_premium_line

def txn_agg_monthly(df_premium_line):
    """
    Aggregate montly transaction to store x brand x category x month level
    """
    df_txn = pd.read_parquet(os.path.join(config.sdl_dir, "txn_store_sku_monthly_v2.parquet"))
    dim_store = pd.read_excel(os.path.join(config.ref_dir, "dim_stores_offline_v5.xlsx"), engine="openpyxl")[
        ["store_id", "store_code", "store_name", "region", "city", "open_date", "close_date"]
    ].drop_duplicates()
    dim_store["cn_offline"] = 1

    df_product = pd.read_excel(os.path.join(config.ref_dir, "dim_product_with_txn.xlsx"), engine="openpyxl")[
        ["product_id", "sku_code", "brand", "brand_type", "category", "Range", "Segment", "skincare_function"]
    ]
    df_product["product_id"] = df_product["product_id"].apply(lambda x: convert_int(x))
    
    df_merge = df_txn.merge(
        dim_store, on=["store_id"], how="left"
    ).merge(
        df_product, on=["product_id"], how="left"
    ).merge(
        df_premium_line, on=["brand", "sku_code"], how="left"
    )

    df_merge["brand_new"] = np.where(
        df_merge["ind_premium_line"] == 1,
        df_merge["brand"] + "_Premium",
        df_merge["brand"]
    )

    df_merge["cn_offline"].fillna(0, inplace=True)
    for col in ["brand", "brand_type", "category", "Range", "Segment"]:
        df_merge[col].fillna("_NaN", inplace=True)

    df_agg = df_merge.groupby(
        ["month", "year", "store_code", "cn_offline", "brand", "brand_type", "category", "Range", "Segment"], as_index=False
    )[["qty", "sales"]].sum()

    df_agg_out_path = os.path.join(config.sdl_dir, "txn_store_category_monthly_v2.csv")
    df_agg.to_csv(df_agg_out_path, index=False)
    os.system("gzip {}".format(df_agg_out_path))

    df_agg_w_pre = df_merge.groupby(
        ["month", "year", "store_code", "cn_offline", "brand_new", "brand_type", "category", "Range", "Segment"], as_index=False
    )[["qty", "sales"]].sum().rename(columns={"brand_new": "brand"})
    df_agg_out_path2 = os.path.join(config.sdl_dir, "txn_store_category_monthly_v2_w_pre.csv")
    df_agg_w_pre.to_csv(df_agg_out_path2, index=False)

    return df_agg, df_agg_w_pre

def _agg_txn(df, grp_keys):
    df_agg = df.groupby(grp_keys, as_index=False).agg(
        {
            "qty": "sum",
            "sales": ["sum", "count"],
            "month": "min"
        }
    )
    return df_agg

def txn_agg_yearly(df):
    """
    Aggregate transaction to store x brand x category x year level
    """
    exclude_category = [
        "ESTORE",
        "GIFT FOR PURCHASE",
        "GIFT VOUCHERS",
        "IFLS CODES",
        "MAKE UP TESTER",
        "MAKE-UP SAMPLES",
        "OTHERS",
        "SKINCARE DEMO",
        "SKINCARE SAMPLES",
        "TOILETRIES",
        "UNRECOGNIZED BARCODE"
    ]

    df_filter1 = df[
        (df["cn_offline"]==1) & 
        (df["brand"]!="_NaN") &
        ~(df["category"].isin(exclude_category)) &
        (df["sales"] != 0)
    ]

    df_filter2 = df_filter1[df_filter1["month"]!="2020-02"]

    grp_keys = ["store_code", "year", "brand", "brand_type", "category", "Range", "Segment"]
    df_filter_agg1 = _agg_txn(df_filter1, grp_keys)
    df_filter_agg2 = _agg_txn(df_filter2, grp_keys)

    out_cols = grp_keys + ["sum_qty", "sum_sales", "cnt_months_w_sales", "start_month_w_sales"]
    df_filter_agg1.columns = out_cols
    df_filter_agg2.columns = out_cols

    return df_filter_agg1, df_filter_agg2

if __name__=="__main__":
    df_premium_line = load_premium_line()
    df_agg, df_agg_w_pre = txn_agg_monthly(df_premium_line) #only run once and read output next time
    #df_agg = pd.read_csv(os.path.join(config.sdl_dir, "txn_store_category_monthly_v2.csv.gz"))
    #df_agg_w_pre = pd.read_csv(os.path.join(config.sdl_dir, "txn_store_category_monthly_v2_w_pre.csv.gz"))
    #df_yearly_incl, df_yearly = txn_agg_yearly(df_agg)
    df_yearly_incl_w_pre, df_yearly_w_pre = txn_agg_yearly(df_agg_w_pre)
    
    out_path1 = os.path.join(config.sdl_dir, "txn_store_category_yearly_include202002_v2.csv")
    out_path2 = os.path.join(config.sdl_dir, "txn_store_category_yearly_v2.csv")
    out_path3 = os.path.join(config.sdl_dir, "txn_store_category_yearly_include202002_v2_w_pre.csv")
    out_path4 = os.path.join(config.sdl_dir, "txn_store_category_yearly_v2_w_pre.csv")

    #df_yearly_incl.to_csv(out_path1, index=False)
    #df_yearly.to_csv(out_path2, index=False)
    df_yearly_incl_w_pre.to_csv(out_path3, index=False)
    df_yearly_w_pre.to_csv(out_path4, index=False)

