import os
import sys
import pandas as pd
import warnings
warnings.filterwarnings("ignore")

from src import config

def get_store_cm(year, df_product, df_store):
    """
    Count unique # of customers per store x month x brand x category x range x segment
    the granularity is to be consistent with the aggregated CRM data
    """
    year = str(year)
    df = pd.read_parquet(os.path.join(config.raw_processed, "txn_{}_clean.parquet".format(year)))
    df = df[df["customer_id"]!=0]
    df.loc[:, "txn_date"] = pd.to_datetime(df["txn_date"], format="%Y-%m-%d")
    df["period"] = df["txn_date"].dt.strftime("%Y%m")

    df_merge = df.merge(
        df_product[["product_id", "brand", "category", "Segment"]], 
        on=["product_id"], how="left"
    ).merge(
        df_store[["store_id", "store_code"]],
        on=["store_id"]
    ).rename(columns={"Segment": "segment"})

    for col in ["brand", "category", "segment"]:
        df_merge[col].fillna("_NaN", inplace=True)

    df_agg = df_merge.groupby(["store_code", "period", "brand", "category", "segment"])[["customer_id"]].nunique().reset_index()
    df_agg = df_agg.rename(columns={"customer_id": "count_customer"})

    return df_agg

def agg_customer_age(df_age, df_cm_cnt):
	df_age = df_age[df_age["period"] <= 202012].fillna(value=np.nan)

	merged_df = pd.merge(df_cm_cnt, df_age, how="left", on=["store_code", "period", "brand", "category", "segment"])
	merged_df = merged_df[~merged_df["brand"].isin(excluded_brand)]
	merged_df = merged_df[~pd.isnull(merged_df['age'])]

	merged_df["weighted_age"] = merged_df["count_customer"] * merged_df["age"].astype(float)
	df_store_age = merged_df[["store_code", "weighted_age", "count_customer"]].groupby(["store_code"], as_index=False).sum()
	df_store_age["age_new"] = df_store_age["weighted_age"]/df_store_age["count_customer"]
	return df_store_age

if __name__ == "__main__":
    df_store = pd.read_excel(os.path.join(config.ref_dir, "dim_stores_offline_v5.xlsx"), engine="openpyxl")
    df_product = pd.read_excel(os.path.join(config.ref_dir, "dim_product_with_txn.xlsx"), engine="openpyxl")
    age_path = os.path.join(config.raw_dir, "crm", "age.csv")
    df_age = pd.read_csv(age_path)

    df_cm_cnt = get_store_cm("2020", df_product, df_store)
    df_cm_cnt.to_csv(os.path.join(config.sdl_dir, "store_month_brand_cat_seg_cm_cnt_v2.csv"), index=False)

    df_store_age = agg_customer_age(df_age, df_cm_cnt)
    df_store_age.to_csv(os.path.join(config.sdl_dir, "store_cm_age.csv"), index=False)