import os
import sys
import pandas as pd
import warnings
warnings.filterwarnings("ignore")

from src import config

def get_store_cm(year, df_product, df_store):
    """
    Count unique # of customers per store x month x brand x category x range x segment
    the granularity is to be consistent with the aggregated CRM data
    """
    year = str(year)
    df = pd.read_parquet(os.path.join(config.raw_processed, "txn_{}_clean.parquet".format(year)))
    df = df[df["customer_id"]!=0]
    df.loc[:, "txn_date"] = pd.to_datetime(df["txn_date"], format="%Y-%m-%d")
    df["period"] = df["txn_date"].dt.strftime("%Y%m")

    df_merge = df.merge(
        df_product[["product_id", "brand", "category", "Segment"]], 
        on=["product_id"], how="left"
    ).merge(
        df_store[["store_id", "store_code"]],
        on=["store_id"]
    ).rename(columns={"Segment": "segment"})

    for col in ["brand", "category", "segment"]:
        df_merge[col].fillna("_NaN", inplace=True)

    df_agg = df_merge.groupby(["store_code", "period", "brand", "category", "segment"])[["customer_id"]].nunique().reset_index()
    df_agg = df_agg.rename(columns={"customer_id": "count_customer"})

    return df_agg

if __name__ == "__main__":
    df_store = pd.read_excel(os.path.join(config.ref_dir, "dim_stores_offline_v5.xlsx"), engine="openpyxl")
    df_product = pd.read_excel(os.path.join(config.ref_dir, "dim_product_with_txn.xlsx"), engine="openpyxl")

    df_out = get_store_cm("2020", df_product, df_store)
    df_out.to_csv(os.path.join(config.sdl_dir, "store_month_brand_cat_seg_cm_cnt_v2.csv"), index=False)