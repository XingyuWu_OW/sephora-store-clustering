import os
import sys
import datetime
import numpy as np
import pandas as pd

from src import config

def _count_unique_cm(df):
    return df.groupby(["store_id", "txn_date"])[["customer_id"]].nunique().reset_index()

def _get_offline_store():
    df_store_attr = pd.read_csv(os.path.join(config.raw_processed, "store_attributes_quarterly.csv"), low_memory=False)
    df_store_offline = pd.read_excel(os.path.join(config.ref_dir, "dim_stores_offline_v5.xlsx"), engine="openpyxl")
    return df_store_offline

def _filter_offline(df):
    df_store_offline = _get_offline_store()
    return df[df["store_id"].isin(df_store_offline["store_id"].unique())]

def _cm_first_purchase(df):
    df_store_attr = pd.read_csv(os.path.join(config.raw_processed, "store_attributes_quarterly.csv"), low_memory=False)
    df_store_offline = pd.read_excel(os.path.join(config.ref_dir, "dim_stores_offline.xlsx"), engine="openpyxl")

    df_offline = _filter_offline(df)
    df_cm_offline = df_offline.groupby(["customer_id"])[["txn_id"]].nunique().reset_index()

    df_cm = df_offline.groupby(["customer_id"], as_index=False).agg({
        "txn_date": "min",
        "sales": "sum"
    }).rename(columns={"txn_date": "min_txn_date"})

    df_out = df_cm_offline.merge(df_cm, on=["customer_id"], how="left")
    return df_out

def store_daily_purchaser(df1, df2, df3):    
    dim_store = pd.read_csv(os.path.join(config.ref_dir, "dim_store_clean.csv"))[["store_id", "store_code"]]

    df1_agg = _count_unique_cm(df1)
    df2_agg = _count_unique_cm(df2)
    df3_agg = _count_unique_cm(df3)

    df1_cm = _cm_first_purchase(df1)
    df2_cm = _cm_first_purchase(df2)
    df3_cm = _cm_first_purchase(df3)

    df_agg = pd.concat([df1_agg, df2_agg, df3_agg])
    df_cm = pd.concat([df1_cm, df2_cm, df3_cm])
    df_cm_out = df_cm.groupby(["customer_id"], as_index=False).agg(
        {
            "min_txn_date": "min",
            "sales": "sum",
            "txn_id": "sum"
        }
    )
    df_agg = df_agg.merge(dim_store, on=["store_id"], how="left")
    df_agg["store_code"] = df_agg["store_code"].astype("int")
    return df_agg, df_cm_out

def cm_cnt_store(df1, df2, df3):
    df1_offline = _filter_offline(df1)
    df2_offline = _filter_offline(df2)
    df3_offline = _filter_offline(df3)

    df1_dedup = df1_offline[["customer_id", "store_id"]].drop_duplicates()
    df2_dedup = df2_offline[["customer_id", "store_id"]].drop_duplicates()
    df3_dedup = df3_offline[["customer_id", "store_id"]].drop_duplicates()

    df_dedup = pd.concat([df1_dedup, df2_dedup, df3_dedup]).drop_duplicates()

    cm_cnt_store = df_dedup.groupby(["customer_id"])[["store_id"]].nunique().reset_index()

    return cm_cnt_store

def store_daily_purchaser_one_year(df1):    
    dim_store = pd.read_csv(os.path.join(config.ref_dir, "dim_store_clean.csv"))[["store_id", "store_code"]]

    df_agg = _count_unique_cm(df1)

    df_cm = _cm_first_purchase(df1)

    df_agg = df_agg.merge(dim_store, on=["store_id"], how="left")
    df_agg["store_code"] = df_agg["store_code"].astype("int")
    return df_agg, df_cm

if __name__=='__main__':
    out_dir = config.sdl_dir
    df1 = pd.read_parquet(os.path.join(config.raw_processed, "txn_2018_clean.parquet"))
    df2 = pd.read_parquet(os.path.join(config.raw_processed, "txn_2019_clean.parquet")) 
    df3 = pd.read_parquet(os.path.join(config.raw_processed, "txn_2020_clean.parquet"))

    df_out, df_cm = store_daily_purchaser(df1, df2, df3)
    df_out.to_csv(os.path.join(out_dir, "store_cnt_cm_daily_v2.csv"), index=False)
    df_cm.to_parquet(os.path.join(out_dir, "cm_cnt_txn_sales_v2.1.parquet"), index=False)
    df_cm_cnt_store = cm_cnt_store(df1, df2, df3)
    df_cm_cnt_store.to_parquet(os.path.join(out_dir, "cm_cnt_store_v2.parquet"), index=False)

    # # only run for 2019
    # df_out, df_cm = store_daily_purchaser_one_year(df2)
    # df_cm.to_parquet(os.path.join(out_dir, "cm_cnt_txn_sales_2019.parquet"), index=False)
