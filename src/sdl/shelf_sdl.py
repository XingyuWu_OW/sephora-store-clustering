import os
import sys
import pandas as pd

import warnings
warnings.filterwarnings("ignore")

from src import config

def run_shelf_sdl():
    """
    Generate shelf SDL
    """
    map_standard_category = {
        "SKIN CARE": "SKINCARE",
        "DEVICE": "SKINCARE",
        "MEN SKIN": "SKINCARE",
        "HAIR CARE": "HAIRCARE"
    }

    df_shelf = pd.read_csv(os.path.join(config.raw_processed, "space_shelf_meter.csv")).rename(columns={
        "category": "category_from_space"
    })

    for col in ["brand", "category_from_space"]:
        df_shelf[col] = df_shelf[col].str.upper()

    # only keep data of latest week
    sel_weekstartdate = df_shelf["weekstartdate"].max()
    df_shelf_filter = df_shelf[
        df_shelf["weekstartdate"]==sel_weekstartdate
    ]

    df_shelf_filter["category"] = df_shelf_filter["category_from_space"].apply(lambda x: map_standard_category.get(x, x))
    df_shelf_filter.loc[df_shelf_filter["pog_name"]=="TWEEZERMAN/SHO-BI", "category"] = "ACCESSORIES"
    
    return df_shelf_filter

if __name__=='__main__':
    df_shelf_clean = run_shelf_sdl()
    df_shelf_clean.to_csv(os.path.join(config.sdl_dir, "space_shelf_meter_sdl.csv"), index=False)
