import os

# data dir
data_dir = "/data/"
raw_dir = os.path.join(data_dir, "storeclustringdata")
planogram_dir = os.path.join(raw_dir, "store_product_planogram")
raw_processed = os.path.join(data_dir, "raw_processed")
ref_dir = os.path.join(data_dir, "ref")
external_dir = os.path.join(data_dir, "external")
temp_dir = os.path.join(data_dir, "temp")

sdl_dir = os.path.join(data_dir, "sdl")
adl_dir = os.path.join(data_dir, "adl")
model_dir = os.path.join(data_dir, "model")
input_dir = os.path.join(data_dir, "manual_input")