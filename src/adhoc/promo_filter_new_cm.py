import os
import sys
import datetime
import numpy as np
import pandas as pd

from src import config
from src.utils import io

def filter_new_cm(year):
    df = pd.read_parquet("/data/raw_processed/txn_{}_clean.parquet".format(year))
    df["txn_date"] = pd.to_datetime(df["txn_date"], format="%Y-%m-%d")
    print("| -- txn loaded...")

    sel_cols = ["customer_id", "first_txn_date"]
    src_dir = os.path.join(config.temp_dir, "xingyu", "promo")
    df_cm = pd.read_parquet(os.path.join(src_dir, "promo_cm_sales_agg_v2.parquet"))[sel_cols]
    print("| -- cm loaded...")

    df_merge = df.merge(df_cm, on=["customer_id"], how="left")
    print("| -- txn cm merged...")

    df_merge["ind_new"] = np.where(
        (~pd.isnull(df_merge["first_txn_date"])) & (df_merge["txn_date"] == df_merge["first_txn_date"]),
        1,
        0
    )

    df_new_cm = df_merge[
        df_merge["ind_new"]==1
    ]
    print("| -- new cm filtered...")
    df_new_cm.to_parquet("/data/raw_processed/txn_{}_clean_new_cm.parquet".format(year))

if __name__ == "__main__":
    filter_new_cm(sys.argv[1])