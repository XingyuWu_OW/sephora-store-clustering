import os
import sys
import datetime
import numpy as np
import pandas as pd

from src import config
from src.utils import io

def get_dim_store():
    dim_store_all = pd.read_csv(os.path.join(config.raw_dir, "DimStore.csv"))[["store_id", "store_code", "store_name"]]
    df = pd.read_excel(os.path.join(config.ref_dir, "dim_stores_offline_v3.xlsx"), engine="openpyxl")
    df_sel = df[["store_id"]].drop_duplicates()
    df_sel["store_type"] = 1  # offline

    eb_stores = [3, 254, 999, 1017, 1019, 1087, 1211]
    df_eb = pd.DataFrame({"store_id": eb_stores, "store_type": [0]*len(eb_stores)})
    df_out = pd.concat([df_sel, df_eb])
    df_out = df_out.merge(dim_store_all, on=["store_id"])
    return df_out

def daily_sales_by_store(dim_store):
    merge_keys = ["store_id", "txn_date", "brand", "category", "Range", "Segment"]
    df1 = pd.read_csv(os.path.join(config.temp_dir, "xingyu", "promo", "sum_sales_2018.csv"))
    df2 = pd.read_csv(os.path.join(config.temp_dir, "xingyu", "promo", "sum_sales_2019.csv"))
    df3 = pd.read_csv(os.path.join(config.temp_dir, "xingyu", "promo", "sum_sales_2020.csv"))

    df_sales = pd.concat([df1, df2, df3])

    df1 = pd.read_csv(os.path.join(config.temp_dir, "xingyu", "promo", "cnt_qty_2018.csv"))
    df2 = pd.read_csv(os.path.join(config.temp_dir, "xingyu", "promo", "cnt_qty_2019.csv"))
    df3 = pd.read_csv(os.path.join(config.temp_dir, "xingyu", "promo", "cnt_qty_2020.csv"))

    df_qty = pd.concat([df1, df2, df3])
    
    df_sales = df_sales.merge(df_qty, on=merge_keys, how="left").merge(dim_store, on=["store_id"])
    df_sales["year"] = df_sales["txn_date"].str[:4]
    df_sales_agg = df_sales.groupby(
        ["store_id", "year", "brand", "category", "Range", "Segment"], as_index=False
    )[["raw_sales", "sales", "count_txns", "qty"]].sum()
    
    df_out = df_sales_agg.merge(dim_store, on=["store_id"])
    return df_out

if __name__=='__main__':
    out_dir = os.path.join(config.temp_dir, "xingyu")
    dim_store = get_dim_store()
    df_out = daily_sales_by_store(dim_store)
    df_out.to_csv(os.path.join(out_dir, "store_by_year_brand_cat_avg_price.csv"), index=False)