import os
import sys
import datetime
import numpy as np
import pandas as pd

from src import config
from src.utils.common import convert_int

def agg_by_sku():
    df_txn = pd.read_parquet(os.path.join(config.sdl_dir, "txn_store_sku_daily.parquet"))
    df_txn["year"] = df_txn["txn_date"].str[:4]
    df_agg = df_txn.groupby(["product_id", "year"], as_index=False).agg(
        {   
            "qty": "sum",
            "sales": "sum",
            "txn_date": ["min", "max"]
        }
    )
    df_agg.columns = ["product_id", "year", "qty", "sales", "min_date", "max_date"]
    return df_agg

def filter_sku():
    df_product = pd.read_csv(os.path.join(config.raw_dir, "DimProduct_20210223_V2.csv"), low_memory=False)
    df_product['product_id'] = df_product['product_id'].apply(lambda x: convert_int(x))
    df_sku = agg_by_sku()

    df_sku_pivot = df_sku.pivot_table(index=["product_id"], values=["qty"], columns=["year"]).reset_index()
    df_sku_pivot.columns = ["product_id", "qty_2018", "qty_2019", "qty_2020"]
    df_sku_agg = df_sku.groupby(["product_id"], as_index=False).agg(
        {
            "sales": "sum",
            "min_date": "min",
            "max_date": "max"
        }
    )

    df_out = df_product.merge(df_sku_pivot, on=["product_id"]).merge(df_sku_agg, on=["product_id"])
    df_out.to_excel("/data/ref/dim_product_with_txn.xlsx", encoding="utf_8_sig", index=False)

if __name__=='__main__':
    filter_sku()
