import os
import sys
import datetime
import numpy as np
import pandas as pd

from src import config
from src.utils import io

def read_txn(filepath):
    df = pd.read_parquet(filepath)
    return df

def read_promo_product():
    df = pd.read_excel(os.path.join(config.ref_dir, "promo_dim_product.xlsx"), engine="openpyxl")
    return df

def get_store_type():
    df = pd.read_excel(os.path.join(config.ref_dir, "dim_stores_offline_v3.xlsx"), engine="openpyxl")
    df_sel = df[["store_id", "region"]].drop_duplicates()
    df_sel["channel"] = "offline"  # offline

    eb_stores = [3, 254, 999, 1017, 1019, 1087, 1211]
    df_eb = pd.DataFrame({"store_id": eb_stores, "region": ["EB"]*len(eb_stores), "channel": ["online"]*len(eb_stores)})
    df_out = pd.concat([df_sel, df_eb])
    return df_out

def filter_txn(df, df_promo, df_store):
    df1 = df.merge(df_promo[["product_id"]], on=["product_id"])
    df2 = df1.merge(df_store[["store_id", "channel", "region"]], on=["store_id"])
    return df2

def calc_gwp():
    df1 = read_txn(os.path.join(config.raw_processed, "txn_2018_clean.parquet"))
    df2 = read_txn(os.path.join(config.raw_processed, "txn_2019_clean.parquet")) 
    df3 = read_txn(os.path.join(config.raw_processed, "txn_2020_clean.parquet"))

    df_promo = read_promo_product()
    df_product = get_store_type()

    df1_filter = filter_txn(df1, df_promo, df_product)
    df2_filter = filter_txn(df2, df_promo, df_product)
    df3_filter = filter_txn(df3, df_promo, df_product)

    df = pd.concat([df1_filter, df2_filter, df3_filter])
    df_agg = df.groupby(["txn_date", "product_id", "channel", "region"], as_index=False)[["qty"]].sum()
    df_agg.to_csv("/data/temp/xingyu/promo_20210315/daily_promo_sku_qty.csv", index=False)
    return df_agg

if __name__ == "__main__":
    calc_gwp()