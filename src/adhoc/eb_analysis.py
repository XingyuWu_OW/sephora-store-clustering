import os
import sys
import datetime
import numpy as np
import pandas as pd

import warnings
warnings.filterwarnings("ignore")

from src import config
from src.utils.common import convert_int

def read_dim_product(filepath):
    if filepath.endswith("xlsx"):
        df = pd.read_excel(filepath, engine="openpyxl")
    else:
        df = pd.read_csv(
            filepath, 
            low_memory=False, 
            header=None, 
            names=["product_id", "brand_type", "brand", "category", "sku_code", "target", "product_name", "product_name_en", 
            "Range", "Segment", "skincare_function", "brand_code"]
        )
    for col in ["Segment", "Range"]:
        df[col].fillna("_NaN", inplace=True)
    
    return df

def derive_brand_cat_share(df, city_col="city", province_col=None):
    if province_col is not None:
        geo_cols = [province_col, city_col]
    else:
        geo_cols = [city_col]
    df_agg1 = df.groupby(geo_cols+["month"], as_index=False)[["sales"]].sum()
    df_agg1_city = df_agg1.groupby(geo_cols, as_index=False)[["sales"]].mean().rename(columns={"sales": "city_level_sales"})

    df_agg2 = df.groupby(geo_cols+["brand", "category", "month"], as_index=False)[["sales"]].sum()
    df_agg2_brand_cat = df_agg2.groupby(geo_cols+["brand", "category"], as_index=False)[["sales"]].mean()

    df_out = df_agg2_brand_cat.merge(df_agg1_city, on=geo_cols)
    df_out["sales_share"] = df_out["sales"] / df_out["city_level_sales"]

    return df_out

def agg_offline_to_city(df_product, df_store):
    city_mapping = {
        "jiangyin": "wuxi",
        "changshu": "suzhou",
        "kunshan": "suzhou",
        "nantongshi": "nantong",
        "shuhai": "zhuhai"
    }
    df_store["city"] = df_store["city"].apply(lambda x: city_mapping.get(x, x))
    df = pd.read_parquet(os.path.join(config.sdl_dir, "txn_store_sku_monthly_v2.parquet"))
    df_merge = df.merge(df_product[["product_id", "sku_code", "brand", "category", "Range", "Segment"]], on=["product_id"]).merge(
        df_store[["store_id", "store_code", "city"]], on=["store_id"])
    
    df_filter = df_merge[(df_merge["year"].isin([2019, 2020])) & (df_merge["month"]!="2020-02")]

    df_out = derive_brand_cat_share(df_filter)
    return df_out

def agg_eb_to_city(df_product, df_city_en, out_dir):

    def _clean_city_name(x):
        city_exclude = ["亳州",
            "儋州",
            "兰州",
            "台州",
            "宿州",
            "常州",
            "广州",
            "徐州",
            "德州",
            "忻州",
            "惠州",
            "扬州",
            "抚州",
            "朔州",
            "杭州",
            "柳州",
            "梅州",
            "梧州",
            "永州",
            "池州",
            "沧州",
            "泉州",
            "泰州",
            "泸州",
            "温州",
            "湖州",
            "滁州",
            "滨州",
            "漳州",
            "潮州",
            "福州",
            "苏州",
            "荆州",
            "衢州",
            "贺州",
            "赣州",
            "达州",
            "郑州",
            "郴州",
            "鄂州",
            "钦州",
            "锦州",
            "随州"]

        x = str(x)
        for c in ["州", "市", "地区"]:
            if x.endswith(c) and x not in city_exclude:
                x = x.replace(c, "")
        return x
                
    df = pd.read_csv(
        os.path.join(config.raw_dir, "oms_sales_order_sku_level_df.csv"), 
        header=None, 
        names=["store_code", "province_cn", "city_cn", "date", "sku_code", "sku_name", "qty", "brand_raw", "sales"]
    ).drop("sku_name", axis=1)
    df["month"] = df["date"].str[:7]
    df["year"] = df["date"].str[:4]
    df_filter = df[df["year"].isin(["2019", "2020"])]
    df_filter.loc[:, "city_cn"] = np.where(
        df_filter["province_cn"].isin(["上海", "北京", "天津", "重庆"]),
        df_filter["province_cn"],
        df_filter["city_cn"]
    )

    df_filter.loc[:, "city_cn"] = df_filter["city_cn"].apply(lambda x: _clean_city_name(x))

    df_product = df_product[["sku_code", "brand", "category", "Range", "Segment"]].drop_duplicates()
    for df in [df_filter, df_product]:
        df["sku_code"] = df["sku_code"].apply(lambda x: convert_int(x))
    df_merge = df_filter.merge(df_product, on=["sku_code"], how="left")
    print(df_merge.shape)
    print(df_merge[pd.isnull(df_merge["brand"])].head())
    print("records with no brand merged", len(df_merge[pd.isnull(df_merge["brand"])]))

    df_agg = derive_brand_cat_share(df_merge, "city_cn", "province_cn")
    #df_agg.to_excel(os.path.join(out_dir, "eb_sales_by_city_raw.xlsx"), index=False, encoding="utf_8_sig")
    df_agg = df_agg.merge(df_city_en[["city_cn", "province_cn", "city"]].drop_duplicates(), on=["city_cn", "province_cn"])

    df_agg["city"] = np.where(
        df_agg["city_cn"] == "杭州",
        "hangzhou",
        np.where(
            df_agg["city_cn"] == "广州",
            "guangzhou",
            df_agg["city"]
        )
    )

    col_mapping = {}
    for col in ["sales", "city_level_sales", "sales_share"]:
        col_mapping[col] = col + "_eb"
    
    df_agg.rename(columns=col_mapping, inplace=True)
    return df_agg

def run_eb_analysis():
    out_dir = os.path.join(config.temp_dir, "xingyu", "eb_analysis")
    df_product = read_dim_product(os.path.join(config.ref_dir, "dim_product_with_txn.xlsx"))
    df_product_all = read_dim_product(os.path.join(config.raw_dir, "DimProduct_20210223_V2.csv"))
    df_store = pd.read_excel(os.path.join(config.ref_dir, "dim_stores_offline_v5.xlsx"), engine="openpyxl")
    df_city_en = pd.read_excel(os.path.join(config.ref_dir, "dim_city_cn_en.xlsx"), engine="openpyxl")

    df_agg_offline = agg_offline_to_city(df_product, df_store)
    df_agg_eb = agg_eb_to_city(df_product_all, df_city_en, out_dir)

    df_agg_offline.to_csv(os.path.join(out_dir, "offline_sales_by_city.csv"), index=False)
    df_agg_eb.to_excel(os.path.join(out_dir, "eb_sales_by_city.xlsx"), index=False, encoding="utf_8_sig")

    key_cols = ["city", "brand", "category"]
    all_keys = pd.concat([df_agg_offline[key_cols].drop_duplicates(), df_agg_eb[key_cols].drop_duplicates()]).drop_duplicates()
    df_out = all_keys.merge(df_agg_offline, on=key_cols, how="left").merge(df_agg_eb, on=key_cols, how="left")

    df_out.to_excel(os.path.join(out_dir, "offline_eb_sales_by_city_v2.xlsx"), index=False, encoding="utf_8_sig")

if __name__ == "__main__":
    run_eb_analysis()