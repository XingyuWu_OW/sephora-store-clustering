import os
import sys
import datetime
import numpy as np
import pandas as pd

import warnings
warnings.filterwarnings("ignore")

from src import config

def read_sales():
    df_online = pd.read_parquet(os.path.join(config.raw_processed, "txn_eb_clean_v2.parquet"))
    df_online["channel"] = 1
    df_offline = pd.read_csv(os.path.join(config.raw_processed, "txn_offline_clean_v2.csv"))
    df_offline["channel"] = 0
    df = pd.concat([df_offline, df_online])
    df["txn_date"] = pd.to_datetime(df["txn_date"], format="%Y-%m-%d")
    print("| -- txn data loaded...")
    return df

def get_gwp(df, df_gwp, out_dir):
    df_filter = df.merge(df_gwp, on=["product_id"])[
        ["txn_date", "customer_id", "product_id", "brand"]].drop_duplicates().rename(
            columns={"product_id": "gwp_product_id", "txn_date": "gwp_txn_date"}
        ).reset_index(drop=True)
    
    df_filter["key"] = df_filter.index+1
    df_filter.to_parquet(os.path.join(out_dir, "gwp_txn.parquet"), index=False)
    return df_filter

def join_back_sales(df, df_product, df_gwp_filter, out_dir):
    df_txn = df.merge(df_product[["product_id", "brand", "category", "Range", "Segment", "sku_code"]], on=["product_id"])

    df_merge = df_txn.merge(df_gwp_filter, on=["customer_id", "brand"])
    df_merge_after = df_merge[df_merge["txn_date"] > df_merge["gwp_txn_date"]]
    df_merge_after.to_parquet(os.path.join(out_dir, "gwp_txn_after.parquet"), index=False)
    return df_merge_after

def get_transactions_after(df, df_product, df_gwp_filter, out_dir):

    df_gwp_filter = df_gwp_filter[df_gwp_filter["brand"]!= "_NaN"]

    exclude_brand = [
        "FIDELITE",
        "GWP",
        "BRGWP",
        "DIVERS",
        "CHINA PLV",
        "CRM CHINA"
    ]
    df_gwp_filter = df_gwp_filter[df_gwp_filter["customer_id"]!=0]
    print("| ---- total # of gwp transactions to process {}".format(len(df_gwp_filter)))
    all_out = []
    for line in df_gwp_filter.values:
        out = list(line)
        cid = line[1]
        gwp_brand = line[3]
        print("| --- {} got GWP for brand {} ...".format(cid, gwp_brand))
        txn_date = pd.to_datetime(line[0], format="%Y-%m-%d")
        df_tmp = df[
            (df["txn_date"] > txn_date) & 
            (df["customer_id"] == cid) 
        ]

        ind_repurchase = 0
        ind_repurchase_same_brand = 0
        same_brand_sku = ""

        if len(df_tmp) > 0:
            df_tmp = df_tmp.merge(df_product[["product_id", "brand"]], on=["product_id"])
            df_tmp = df_tmp[~df_tmp["brand"].isin(exclude_brand)]
            if len(df_tmp) > 0:
                print("| ---- {} repurchased...".format(cid))
                ind_repurchase = 1
                print("|| ---- check brands", df_tmp["brand"].unique())
                df_filter = df_tmp[df_tmp["brand"]==gwp_brand]
                if len(df_filter) > 0:
                    print("| ---- {} repurchased same brand...".format(cid))
                    ind_repurchase_same_brand = 1
                    skus = [str(i) for i in df_filter["product_id"].unique()]
                    same_brand_sku = "|".join(skus)
        
        out.append([ind_repurchase, ind_repurchase_same_brand, same_brand_sku])
    
    all_out.append(out)

    df_out = pd.DataFrame(all_out, 
        columns=list(df_gwp_filter.columns.values) + ["ind_repurchase", "ind_repurchase_same_brand", "same_brand_sku"])
    
    df_out.to_parquet(os.path.join(out_dir, "gwp_txn_after_v1.parquet"), index=False)

    return df_out

def run():
    df_product = pd.read_excel(os.path.join(config.ref_dir, "dim_product_with_txn.xlsx"), engine="openpyxl")
    df_gwp = pd.read_excel(os.path.join(config.ref_dir, "dim_product_gwp.xlsx"), engine="openpyxl")[["product_id", "brand"]]

    out_dir = os.path.join(config.temp_dir, "xingyu", "gwp")
    df = read_sales()
    #df_gwp_filter = get_gwp(df, df_gwp, out_dir)
    df_gwp_filter = pd.read_parquet(os.path.join(out_dir, "gwp_txn.parquet"))
    df_gwp_after = get_transactions_after(df, df_product, df_gwp_filter, out_dir)

if __name__ == "__main__":
    run()