import os
import pandas as pd

from src import config

def check_2020():
    df_txn = pd.read_parquet(os.path.join(config.raw_processed, "txn_2020_clean.parquet"))
    dim_product = pd.read_excel(os.path.join(config.ref_dir, "dim_product_with_txn.xlsx"), engine="openpyxl")
    #df_txn = df_txn[df_txn["sales"]>0]
    df_txn = df_txn.merge(dim_product, on=["product_id"])
    exclude_cat = ["UNRECOGNIZED BARCODE", "IFLS CODES"]
    df_txn = df_txn[~df_txn["category"].isin(exclude_cat)]
    df_traffic = pd.read_csv(os.path.join(config.sdl_dir, "traffic_sdl.csv"))
    df_traffic = df_traffic[df_traffic["year"]==2020]
    dim_store = pd.read_excel(
        os.path.join(config.ref_dir, "dim_stores_offline_v2.xlsx"), engine="openpyxl"
    )[["store_id", "store_code"]].drop_duplicates()

    df_cnt_txn = df_txn.groupby(["store_id"])[["txn_id"]].nunique().reset_index()
    df_cnt_txn.columns = ["store_id", "cnt_txn"]

    df_cnt_cm = df_txn.groupby(["store_id"])[["customer_id"]].nunique().reset_index()
    df_cnt_cm.columns = ["store_id", "cnt_cm"]

    df_txn_agg = df_txn.groupby(["store_id"], as_index=False)[["sales"]].sum()

    df_traffic_agg = df_traffic.groupby(["store_code"], as_index=False)[["traffic_combine"]].sum()

    df_all = df_txn_agg.merge(df_cnt_txn, on=["store_id"], how="left").merge(
        df_cnt_cm, on=["store_id"], how="left"
    ).merge(
        dim_store, on=["store_id"], how="left"
    ).merge(df_traffic_agg, on=["store_code"], how="left")

    df_all.to_csv("/data/temp/xingyu/2020_KPI_check.csv", index=False)

    return None


if __name__=='__main__':
    check_2020()