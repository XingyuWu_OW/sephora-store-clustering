import os
import sys
import datetime
import numpy as np
import pandas as pd

from src import config

def read_traffic_master():
    df = pd.read_csv("/data/temp/vivi/traffic_master_estimate.csv")
    print(df.head())
    return df

def read_store_cm():
    df = pd.read_csv(os.path.join(config.sdl_dir, "store_cnt_cm_daily.csv"))
    dim_date = pd.read_csv(os.path.join(config.ref_dir, "dim_date.csv")).rename(columns={"date": "txn_date"})
    df = df.merge(dim_date, on=["txn_date"], how="left")
    print(df.head())
    return df


def run():
    df_traffic = read_traffic_master()
    df_store_cm = read_store_cm()

    df_merge = 

if __name__=='__main__':
    run()

