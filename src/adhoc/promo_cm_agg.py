import os
import sys
import datetime
import numpy as np
import pandas as pd

import warnings
warnings.filterwarnings("ignore")

from src import config

def calc_count_txn_pct(df, out_dir):
    df = df[df["count_txn"] > 1]
    cols = ["count_txn_discount_ind_12_pct", "count_txn_discount_ind_15_pct", "count_txn_discount_ind_20_pct", 
    "count_txn_coupon_ind"]
    share_cols = []
    for col in cols:
        share_col = "{}_share".format(col)
        df[share_col] = (df[col] / df["count_txn"]).round(2)
        share_cols.append(share_col)
        if col != "count_txn_coupon_ind":
            share_col = "{}_and_coupon_share".format(col)
            df[share_col] = ((df[col] + df["count_txn_coupon_ind"]) / df["count_txn"]).round(2)
            share_cols.append(share_col)
    
    for col in share_cols:
        df_agg1 = df.groupby([col], as_index=False)[["sales"]].sum()
        df_agg2 = df.groupby([col])[["customer_id"]].nunique().rename(columns={"customer_id": "count_customer"})

        df_agg = df_agg1.merge(df_agg2, on=[col])
        out_path = os.path.join(out_dir, "agg_{}.csv".format(col))
        df_agg.to_csv(out_path, index=False)
    
    return None

if __name__ == "__main__":
    src_dir = os.path.join(config.temp_dir, "xingyu", "promo")
    out_dir = os.path.join(config.temp_dir, "xingyu", "promo_cm")
    df = pd.read_parquet(os.path.join(src_dir, "promo_cm_sales_agg.parquet"))
    calc_count_txn_pct(df, out_dir)