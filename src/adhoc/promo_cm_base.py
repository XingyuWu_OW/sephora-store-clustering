import os
import sys
import datetime
import numpy as np
import pandas as pd

import warnings
warnings.filterwarnings("ignore")

from src import config

def _agg_sales(df, need_first_date=False):
    if need_first_date:
        df.loc[:, "txn_date"] = pd.to_datetime(df["txn_date"], format="%Y-%m-%d")
        df_agg1 = df.groupby(["customer_id"], as_index=False).agg({
            "raw_sales": "sum",
            "sales": "sum",
            "txn_date": ["min", "max"]
        })
        df_agg1.columns = ["customer_id", "raw_sales", "sales", "first_txn_date", "last_txn_date"]
        print("| ---- get first/last date, sum sales")
    else:
        df_agg1 = df.groupby(["customer_id"], as_index=False)[["raw_sales", "sales"]].sum()
        print("| ---- sum sales")

    df_agg2 = df.groupby(["customer_id"])[["txn_id"]].nunique().reset_index().rename(
        columns={"txn_id": "count_txn"}
    )
    print("| ---- count txns")

    df_agg = df_agg2.merge(df_agg1, on=["customer_id"])
    print("| -- one time agg done")
    return df_agg    

def agg_sales(df, coupon_lst):
    df = df[df["customer_id"]!=0]
    df_txn_ind = gen_txn_ind(df, coupon_lst)

    df_agg = _agg_sales(df, need_first_date=True)

    for ind in ["coupon_ind"] + ["discount_ind_{}_pct".format(i) for i in [12, 15, 20]]:
        sel_txn = df_txn_ind[df_txn_ind[ind]==1]
        df_sel = df.merge(sel_txn[["txn_id"]], on=["txn_id"])
        df_sel_agg = _agg_sales(df_sel)
        df_sel_agg = df_sel_agg.rename(columns={
            "count_txn": "count_txn_{}".format(ind),
            "sales": "sales_{}".format(ind),
            "raw_sales": "raw_sales_{}".format(ind)
        })
        df_agg = df_agg.merge(df_sel_agg, on=["customer_id"], how="left")
    
    print("| -- sales aggregated")
    return df_agg

def gen_txn_ind(df, coupon_lst):
    df_filter = df[df["product_id"].isin(coupon_lst)][["txn_id"]].drop_duplicates()
    df_filter["coupon_ind"] = 1

    df_txn = df.groupby(["txn_id"], as_index=False)[["raw_sales", "sales"]].sum()
    df_txn["discount"] = (df_txn["raw_sales"] - df_txn["sales"]) / df_txn["raw_sales"]

    for i in [12, 15, 20]:
        thres = i / 100
        df_txn["discount_ind_{}_pct".format(i)] = np.where(
            df_txn["discount"] >= thres,
            1,
            0
        )
    
    df_txn = df_txn.merge(df_filter, on=["txn_id"], how="left")
    df_txn["coupon_ind"].fillna(0, inplace=True)
    print("| -- transaction indicators generated")
    return df_txn

def combine_agg(df_combine, df_offline_agg=None, df_online_agg=None):

    if df_offline_agg is None:
        df_offline_agg = df_combine[df_combine["channel"]=="offline"]
        df_online_agg = df_combine[df_combine["channel"]=="online"]

    # cross-channel aggregation
    df_combine_agg = df_combine.groupby(["customer_id"], as_index=False).agg({
        "count_txn": "sum",
        "sales": "sum",
        "raw_sales": "sum",
        "first_txn_date": "min",
        "last_txn_date": "max",
        "count_txn_coupon_ind": "sum",
        "sales_coupon_ind": "sum",
        "count_txn_discount_ind_12_pct": "sum",
        "sales_discount_ind_12_pct": "sum",
        "count_txn_discount_ind_15_pct": "sum",
        "sales_discount_ind_15_pct": "sum",
        "count_txn_discount_ind_20_pct": "sum",
        "sales_discount_ind_20_pct": "sum"
    })

    # split channel - pivot
    pivot_cols = ["count_txn", "raw_sales", "sales"]
    df_combine_pivot = df_combine.pivot_table(
        index=["customer_id"], values=pivot_cols, 
        columns=["channel"]
    ).reset_index()
    df_combine_pivot.columns = [
        "customer_id", 
        "count_txn_offline", "count_txn_online", 
        "raw_sales_offline", "raw_sales_online",
        "sales_offline", "sales_online"
    ]

    # add flags on cm who purchased both online and offline
    sel_cols = ["customer_id", "first_txn_date", "last_txn_date"]
    df_both = df_offline_agg[sel_cols].merge(df_online_agg[sel_cols], on=["customer_id"])
    df_both["shop_both_online_offline"] = 1
    df_both["first_txn_date_is_offline"] = np.where(
        df_both["first_txn_date_x"] <= df_both["first_txn_date_y"],
        1,
        0
    )
    df_both["last_txn_date_is_offline"] = np.where(
        df_both["last_txn_date_x"] >= df_both["last_txn_date_y"],
        1,
        0
    )

    df_both["ind_offline_to_online"] = np.where(
        df_both["first_txn_date_is_offline"] == 1,
        1,
        0
    )

    df_both["ind_online_to_offline"] = np.where(
        df_both["first_txn_date_is_offline"] == 0,
        1,
        0
    )

    # merge flags
    df_out = df_combine_agg.merge(df_combine_pivot, on=["customer_id"], how="left").merge(
        df_both[["customer_id", "shop_both_online_offline", "first_txn_date_is_offline", "last_txn_date_is_offline",
        "ind_offline_to_online", "ind_online_to_offline"]],
        on=["customer_id"], how="left"
    )

    # fillna
    for col in ["shop_both_online_offline", "ind_offline_to_online", "ind_online_to_offline"]:
        df_out[col].fillna(0, inplace=True)

    df_out["first_txn_date_is_offline"] = np.where(
        (df_out["shop_both_online_offline"] == 0) & (df_out["count_txn_offline"] > 0),
        1,
        df_out["first_txn_date_is_offline"]
    )

    df_out["last_txn_date_is_offline"] = np.where(
        (df_out["shop_both_online_offline"] == 0) & (df_out["count_txn_offline"] > 0),
        1,
        df_out["last_txn_date_is_offline"]
    )

    for col in ["count_txn_offline", "count_txn_online", "sales_offline", "sales_online", "first_txn_date_is_offline", "last_txn_date_is_offline"]:
        df_out[col].fillna(0, inplace=True)
    
    # additional flags
    df_out["ind_shop_online"] = np.where(
        df_out["count_txn_online"] > 0,
        1,
        0
    )

    df_out["ind_shop_offline"] = np.where(
        df_out["count_txn_offline"] > 0,
        1,
        0
    )

    df_out["sales_bin"] = np.where(
        df_out["sales"] <= 1000,
        "<= 1000",
        np.where(
            df_out["sales"] <= 5000,
            "(1000, 5000]",
            np.where(
                df_out["sales"] <= 20000,
                "(5000, 20000]",
                "> 20000"
            )
        )
    )

    return df_out

def run_promo_cm(out_dir):
    # coupon_lst = [109446, 83634, 200898, 94941, 257057, 88490, 88489, 99887, 168149, 262751, 248056, 248057, 265137, 259984, 
    # 262692, 167636, 260050, 186830, 49602, 198572, 198551, 260998, 83294, 262664, 198616, 262672, 262683, 204722, 185661, 172383]

    # df_online = pd.read_parquet(os.path.join(config.raw_processed, "txn_eb_clean_v2.parquet"))
    # df_online_agg = agg_sales(df_online, coupon_lst)
    # df_online_agg["channel"] = "online"
    # print("| -- online done")
    # df_online = None

    # df_offline = pd.read_csv(os.path.join(config.raw_processed, "txn_offline_clean_v2.csv"))
    # df_offline_agg = agg_sales(df_offline, coupon_lst)
    # df_offline_agg["channel"] = "offline"
    # print("| -- offline done")
    # df_offline = None

    # df_combine = pd.concat([df_online_agg, df_offline_agg])
    # df_combine.to_parquet(os.path.join(out_dir, "promo_cm_sales_by_channel_v2.parquet"), index=False)
    # print("| -- combined data saved")

    df_combine = pd.read_parquet(os.path.join(out_dir, "promo_cm_sales_by_channel_v2.parquet"))
    df_out = combine_agg(df_combine) #, df_offline_agg, df_online_agg)
    df_out.to_parquet(os.path.join(out_dir, "promo_cm_sales_agg_v2.parquet"), index=False)

    return df_out

if __name__ == "__main__":
    out_dir = os.path.join(config.temp_dir, "xingyu", "promo")
    run_promo_cm(out_dir)
