import os
import sys
import numpy as np
import pandas as pd
import warnings
warnings.filterwarnings("ignore")

from src import config

df_store = pd.read_excel(os.path.join(config.ref_dir, "dim_stores_offline_v5.xlsx"), engine="openpyxl")
df1 = pd.read_csv(os.path.join(config.sdl_dir, "store_month_brand_cat_seg_cm_cnt_v2.csv"))
df2 = pd.read_csv(os.path.join(config.raw_dir, "crm", "age.csv"))

for df in [df1, df2]:
    df["period"] = df["period"].astype("str")

df2 = df2[(df2["period"]<="202012") & (df2["store_code"].isin(df_store["store_code"].unique()))]

df_merge = df2.merge(df1, on=["store_code", "period", "brand", "category", "segment"], how="left")
df_merge["age_weight"] = np.where(
    ~pd.isnull(df_merge["count_customer"]),
    df_merge["age"] * df_merge["count_customer"],
    df_merge["age"]
)

exclude_brand = [
    "FIDELITE",
    "GWP",
    "BRGWP",
    "DIVERS",
    "CHINA PLV",
    "CRM CHINA"
]

df_merge = df_merge[~df_merge["brand"].isin(exclude_brand)]

df_agg1 = df_merge.groupby(["store_code"], as_index=False)[["age"]].mean().rename(columns={"age": "age_global_avg"})

df_agg2 = df_merge.groupby(["store_code", "period"], as_index=False)[["age_weight", "count_customer"]].sum()
df_agg2["age_weight_avg_by_month"] = df_agg2["age_weight"] / df_agg2["count_customer"]

df_agg2.merge(df_agg1, on=["store_code"], how="left").to_csv("/data/temp/xingyu/validate_age.csv", index=False)