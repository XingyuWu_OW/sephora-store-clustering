import os
import sys
import datetime
import numpy as np
import pandas as pd

from src import config

def reindex_by_date(df):
    dates = pd.date_range(start=df['date'].min(), end="2020-12-31")
    df = df.set_index('date')
    df_out = df.reindex(dates).fillna(method = "ffill")
    df_out = df_out.reset_index().rename(columns = {"index": "date"})
    
    return df_out

def traffic_sdl():
    dim_store = pd.read_excel(
        "/data/ref/dim_stores_offline.xlsx", engine="openpyxl"
    )[["store_code", "open_date", "close_date"]].drop_duplicates()
    dim_store.fillna("9999-99-99", inplace=True)
    df1 = pd.read_csv(os.path.join(config.raw_processed, "traffic_clean.csv"))
    df2 = pd.read_csv(os.path.join(config.raw_dir, "traffic_additional.csv"))
    df2.columns = ["date", "store_code", "traffic_new"]
    df2["date"] = pd.to_datetime(df2["date"], format="%Y%m%d").dt.strftime("%Y-%m-%d")

    df_master = pd.read_csv("/data/sdl/store_cnt_cm_daily.csv")
    df_master = df_master.rename(columns={"txn_date": "date"})

    df_master_merge = df_master.merge(dim_store, on=["store_code"])
    # df_master_merge_sel = df_master_merge[
    #     (df_master_merge["date"] >= df_master_merge["open_date"]) & 
    #     (df_master_merge["date"] <= df_master_merge["close_date"])
    # ]
    df_master_traffic = df_master_merge.merge(
        df1, on=["store_code", "date"],
        how="left"
    ).merge(
        df2, on=["store_code", "date"],
        how="left"
    )

    df_master_traffic["traffic_combine"] = df_master_traffic["traffic_new"].combine_first(df_master_traffic["traffic_in"])
    df_master_traffic['diff'] = np.where(
        (~pd.isnull(df_master_traffic["traffic_in"])) & (~pd.isnull(df_master_traffic["traffic_new"])),
        (df_master_traffic["traffic_new"] - df_master_traffic["traffic_in"]) / df_master_traffic["traffic_in"],
        np.nan
    )

    print("---- check start ---- ")
    print("total stores", df_master_traffic['store_id'].nunique())
    print(df_master_traffic.shape)
    df_w_traffic = df_master_traffic[~pd.isnull(df_master_traffic["traffic_combine"])]
    print("stores w/ traffic", df_w_traffic['store_id'].nunique())
    print(df_w_traffic.shape)
    
    print("average diff", df_master_traffic['diff'].mean())

    df_missing = df_master_traffic[pd.isnull(df_master_traffic["traffic_combine"])]
    df_missing.groupby(["store_code"], as_index=False)[["date"]].count().to_csv("/data/temp/xingyu/traffic_missing_check.csv", index=False)
    df_master_traffic["conversion"] = np.where(
        df_master_traffic["traffic_combine"] > 0,
        df_master_traffic["customer_id"] / df_master_traffic["traffic_combine"],
        np.nan
    ) 
    df_master_traffic.to_csv("/data/temp/xingyu/traffic_master_check.csv", index=False)

    dim_date = pd.read_csv("/data/ref/dim_date.csv")    

    df_master_traffic_agg = df_master_traffic.merge(
        dim_date[["date", "month"]], how="left"
    ).groupby(["store_code", "month"], as_index=False).agg({
        "traffic_in": "sum",
        "traffic_new": "sum",
        "traffic_combine": ["sum", "count"], 
        "customer_id": "sum",
        "conversion": "mean"
    })

    
    df_master_traffic_agg.columns = ["store_code", "month", "traffic_v1", "traffic_v2", "traffic_final", "cnt_days", "customer_cnt", "conversion_daily_avg"]
    df_master_traffic_agg["conversion_monhtly"] = df_master_traffic_agg["customer_cnt"] / df_master_traffic_agg["traffic_final"]
    df_master_traffic_agg.to_csv("/data/temp/xingyu/traffic_master_agg_monthly.csv", index=False)
    
if __name__=='__main__':
    traffic_sdl()