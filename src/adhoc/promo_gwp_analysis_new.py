import os
import sys
import datetime
import numpy as np
import pandas as pd

import warnings
warnings.filterwarnings("ignore")

from src import config

def read_sales():
    df_online = pd.read_parquet(os.path.join(config.raw_processed, "txn_eb_clean_v2.parquet"))
    df_online["channel"] = 1
    df_offline = pd.read_csv(os.path.join(config.raw_processed, "txn_offline_clean_v2.csv"))
    df_offline["channel"] = 0
    df = pd.concat([df_offline, df_online])
    df["txn_date"] = pd.to_datetime(df["txn_date"], format="%Y-%m-%d")
    print("| -- txn data loaded...")
    return df

def get_gwp(df, df_gwp, out_dir):
    df_filter = df.merge(df_gwp, on=["product_id"])[
        ["txn_date", "customer_id", "product_id", "brand"]].drop_duplicates().rename(
            columns={"product_id": "gwp_product_id", "txn_date": "gwp_txn_date"}
        ).reset_index(drop=True)
    
    df_filter["key"] = df_filter.index+1
    df_filter.to_parquet(os.path.join(out_dir, "gwp_txn.parquet"), index=False)
    return df_filter

def join_back_sales(df, df_product, df_gwp_filter, out_dir):
    df_txn = df.merge(df_product[["product_id", "brand", "category", "Range", "Segment", "sku_code"]], on=["product_id"])

    df_merge = df_txn.merge(df_gwp_filter, on=["customer_id", "brand"])
    df_merge_after = df_merge[df_merge["txn_date"] > df_merge["gwp_txn_date"]]
    df_merge_after.to_parquet(os.path.join(out_dir, "gwp_txn_after.parquet"), index=False)
    return df_merge_after

def get_transactions_after_gwp(df, df_product, df_gwp, out_dir):

    df_gwp_filter = df_gwp[(df_gwp["brand"]!= "_NaN") & (df_gwp["customer_id"]!=0)]

    df_gwp_cm_brand = df_gwp_filter[["customer_id", "brand"]].drop_duplicates()
    df_gwp_cm = df_gwp_cm_brand[["customer_id"]].drop_duplicates()

    df_promo = pd.read_excel(os.path.join(config.ref_dir, "promo_dim_product.xlsx"), engine="openpyxl")[["product_id"]]
    df_promo["ind_promo"] = 1

    df_sel = df.merge(df_gwp_cm, on=["customer_id"])
    df_merge = df_sel.merge(df_product[["product_id", "brand"]], on=["product_id"]).merge(
        df_promo, on=["product_id"], how="left")
    df_merge = df_merge[pd.isnull(df_merge["ind_promo"])]

    df_merge_gwp_brand = df_merge.merge(df_gwp_cm_brand, on=["customer_id", "brand"])
    df_merge_gwp_brand.to_parquet(os.path.join(out_dir, "gwp_cm_same_brand_txn.parquet"), index=False)
    
    return df_merge_gwp_brand

def run():
    df_product = pd.read_excel(os.path.join(config.ref_dir, "dim_product_with_txn.xlsx"), engine="openpyxl")
    df_gwp = pd.read_excel(os.path.join(config.ref_dir, "dim_product_gwp.xlsx"), engine="openpyxl")[["product_id", "brand"]]

    out_dir = os.path.join(config.temp_dir, "xingyu", "gwp")
    df = read_sales()
    #df_gwp_filter = get_gwp(df, df_gwp, out_dir)
    df_gwp_filter = pd.read_parquet(os.path.join(out_dir, "gwp_txn.parquet"))
    df_gwp_after = get_transactions_after_gwp(df, df_product, df_gwp_filter, out_dir)

if __name__ == "__main__":
    run()