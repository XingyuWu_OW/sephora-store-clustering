import os
import sys
import datetime
import numpy as np
import pandas as pd

from src import config
from src.utils import io

def read_promo_product():
    df = pd.read_excel(os.path.join(config.ref_dir, "promo_dim_product.xlsx"), engine="openpyxl")
    return df

def filter_promo(df, df_promo_product):
    df_merge = df.merge(df_promo_product[["product_id", "ind_promo"]], on=["product_id"], how="left")
    df_filter = df_merge[df_merge["ind_promo"]!=1]
    return df_filter

def read_all_product():
    df = pd.read_excel(os.path.join(config.ref_dir, "dim_product_with_txn.xlsx"), engine="openpyxl")
    return df

def _agg_data(df, grp_keys, df_promo_product):
    df_agg1 = df.groupby(grp_keys, as_index=False)[["raw_sales", "sales"]].sum()
    df_agg2 = df.groupby(grp_keys)[["txn_id"]].nunique().rename(columns={"txn_id": "count_txns"})
    
    df_filter = filter_promo(df, df_promo_product)
    df_qty = df_filter.groupby(grp_keys)[["qty"]].sum()

    df_out = df_agg1.merge(df_agg2, on=grp_keys, how="left").merge(df_qty, on=grp_keys, how="left")
    return df_out

def promo_agg(df, df_product, df_promo_product):

    df_promo_product["ind_promo"] = 1
    merge_keys = ["product_id", "brand", "category", "Range", "Segment"]
    df_merge = df.merge(df_product[merge_keys], on=["product_id"], how="left")
    print("| -- product dim merged...")

    grp_keys1 = ["store_id", "txn_date", "brand", "category", "Range"]
    df_out1 = _agg_data(df_merge, grp_keys1, df_promo_product)
    grp_keys2 = ["store_id", "txn_date", "category"]
    df_out2 = _agg_data(df_merge, grp_keys2, df_promo_product)

    grp_keys3 = ["store_id", "txn_date"]
    df_out3 = _agg_data(df_merge, grp_keys3, df_promo_product)
    print("| -- sales and txn id aggregated...")


    return df_out1, df_out2, df_out3

def run_agg(year):
    year = str(year)
    out_dir = os.path.join(config.temp_dir, "xingyu", "promo_20210315_new_cm")
    sel_cols = ["txn_date", "store_id", "product_id", "raw_sales", "sales", "txn_id", "qty"]

    df_product = read_all_product()
    df_promo_product = read_promo_product()

    #df = pd.read_parquet("/data/raw_processed/txn_{}_clean.parquet".format(year))[sel_cols]
    df = pd.read_parquet("/data/raw_processed/txn_{}_clean_new_cm.parquet".format(year))[sel_cols]
    df_agg1, df_agg2, df_agg3 = promo_agg(df, df_product, df_promo_product)
    df_agg1.to_csv(out_dir + "/sum_sales_by_store_brand_cate_range_{}.csv".format(year), index=False)
    
    df_agg2.to_csv(out_dir + "/sum_sales_by_store_cate_{}.csv".format(year), index=False)
    df_agg3.to_csv(out_dir + "/sum_sales_by_store_{}.csv".format(year), index=False)

if __name__=='__main__':
    run_agg(sys.argv[1])
   
 

