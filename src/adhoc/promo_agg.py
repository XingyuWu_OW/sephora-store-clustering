import os
import sys
import datetime
import numpy as np
import pandas as pd

from src import config
from src.utils import io

def read_promo_product():
    df = pd.read_excel(os.path.join(config.ref_dir, "promo_dim_product.xlsx"), engine="openpyxl")
    return df

def read_all_product():
    df = pd.read_excel(os.path.join(config.ref_dir, "dim_product_with_txn.xlsx"), engine="openpyxl")
    return df

def get_store_type():
    df = pd.read_excel(os.path.join(config.ref_dir, "dim_stores_offline_v5.xlsx"), engine="openpyxl")
    df_sel = df[["store_id", "region"]].drop_duplicates()
    df_sel["store_type"] = 1  # offline

    eb_stores = [3, 254, 999, 1017, 1019, 1087, 1211]
    df_eb = pd.DataFrame({"store_id": eb_stores, "region": ["EB"]*len(eb_stores), "store_type": [0]*len(eb_stores)})
    df_out = pd.concat([df_sel, df_eb])
    return df_out

def filter_promo_txn(df_raw, df_promo_product, df_store_type):
    df = df_raw.merge(df_store_type, on=["store_id"])
    df_dedup = df[["product_id", "txn_id"]].drop_duplicates()
    df_promo_txn = df_dedup.merge(df_promo_product[["product_id"]], on=["product_id"])
    df_promo_txn_unique = df_promo_txn[["txn_id"]].drop_duplicates()
    df_filter = df.merge(df_promo_txn_unique, on=["txn_id"])
    return df_filter, df_promo_txn

def _agg_sales(df, grp_keys):
    
    df_agg1 = df.groupby(grp_keys, as_index=False)[["raw_sales", "sales"]].sum()
    df_agg2 = df.groupby(grp_keys)[["txn_id"]].nunique().rename(columns={"txn_id": "count_txns"})

    df_out = df_agg1.merge(df_agg2, on=grp_keys, how="left")
    return df_out

def promo_agg(df, df_product, df_promo_product):

    merge_keys = ["product_id", "brand", "category", "Range", "Segment"]
    df_merge = df.merge(df_product[merge_keys], on=["product_id"], how="left")
    print("| -- product dim merged...")

    grp_keys1 = ["txn_id", "txn_date", "category", "store_type", "region"]
    df_out1 = _agg_sales(df_merge, grp_keys1)
    
    grp_keys2 = ["txn_id", "txn_date", "store_type", "region"]
    df_out2 = _agg_sales(df_merge, grp_keys2)

    print("| -- sales and txn id aggregated...")

    df_promo_product["ind_promo"] = 1
    # remove promo skus to calculate quantities
    df_filter = df.merge(df_promo_product[["product_id", "ind_promo"]], on=["product_id"], how="left")
    df_filter = df_filter[df_filter["ind_promo"]!=1]
    df_filter_merge = df_filter.merge(df_product[merge_keys], on=["product_id"], how="left")

    df_filter_agg1 = df_filter_merge.groupby(grp_keys1, as_index=False)[["qty"]].sum()
    df_filter_agg2 = df_filter_merge.groupby(grp_keys2, as_index=False)[["qty"]].sum()

    df_out_by_cat = df_out1.merge(
        df_filter_agg1,
        on=grp_keys1, how="left"
    )

    df_out_total = df_out2.merge(
        df_filter_agg2,
        on=grp_keys2, how="left"
    )

    return df_out_by_cat, df_out_total

def map_txn_sku(df_sku_txn, df_txn_agg):
    df_merge = df_sku_txn.merge(df_txn_agg, on=["txn_id"], how="left")
    if "category" in df_txn_agg.columns:
        keys = ["txn_date", "product_id", "region", "category", "store_type"]
    else:
        keys = ["txn_date", "product_id", "region", "store_type"]
    df_agg = df_merge.groupby(
        keys, as_index=False
    )[["raw_sales", "sales", "count_txns", "qty"]].sum()
    return df_agg

def agg_by_brand(df_store_type):
    dim_store = df_store_type
    dim_store["channel"] = np.where(
        dim_store["store_type"] == 1,
        "offline",
        "online"
    )

    all_df = []
    for yr in [2018, 2019, 2020]:
        yr = str(yr)
        df = pd.read_csv(os.path.join(config.temp_dir, "xingyu", "promo", "sum_sales_by_store_brand_cate_range_{}.csv".format(yr)))
        all_df.append(df)
    df = pd.concat(all_df)
    df_merge = df.merge(dim_store[["store_id", "channel", "region"]], on=['store_id'])
    df_agg = df_merge.groupby(["txn_date", "region", "brand", "category", "Range", "channel"],as_index=False)[
        ["raw_sales", "sales", "count_txns", "qty"]
    ].sum()
    return df_agg

def run_agg(year):
    year = str(year)
    out_dir = os.path.join(config.temp_dir, "xingyu", "promo_20210315")
    sel_cols = ["txn_date", "store_id", "product_id", "raw_sales", "sales", "txn_id", "qty"]

    df_product = read_all_product()
    df_promo_product = read_promo_product()
    df_store_type = get_store_type()

    df_brand_agg = agg_by_brand(df_store_type)
    df_brand_agg.to_csv(out_dir + "/daily_sales_by_region_by_brand_cat_range.csv".format(year), index=False)
    
    #df = pd.read_parquet("/data/raw_processed/txn_{}_clean.parquet".format(year))[sel_cols]

    # df = pd.read_parquet("/data/raw_processed/txn_{}_clean_new_cm.parquet".format(year))[sel_cols]
    # df_filter, df_promo_txn = filter_promo_txn(df, df_promo_product, df_store_type)
    # df_txn_agg_by_cat, df_txn_agg = promo_agg(df_filter, df_product, df_promo_product)
    # df_out = map_txn_sku(df_promo_txn, df_txn_agg)
    # df_out.to_csv(out_dir + "/promo_associated_sales_total_{}.csv".format(year), index=False)
    
    # df_out2 = map_txn_sku(df_promo_txn, df_txn_agg_by_cat)
    # df_out2.to_csv(out_dir + "/promo_associated_sales_by_cat_{}.csv".format(year), index=False)


if __name__=='__main__':
    run_agg(sys.argv[1])
    

