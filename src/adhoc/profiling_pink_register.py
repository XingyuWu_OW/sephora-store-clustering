import os
import sys
import numpy as np
import pandas as pd
import warnings
warnings.filterwarnings("ignore")

from src import config

def check_pink_register(year, df_store, df_register):
    year = str(year)
    df = pd.read_parquet(os.path.join(config.raw_processed, "txn_{}_clean.parquet".format(year)))
    df = df[df["customer_id"]!=0]
    df.loc[:, "txn_date"] = pd.to_datetime(df["txn_date"], format="%Y-%m-%d")
    df["period"] = df["txn_date"].dt.strftime("%Y%m")

    df_merge = df.merge(
        df_store[["store_id", "store_code"]],
        on=["store_id"]
    )

    df_agg = df_merge.groupby(["store_code", "period"])[["customer_id"]].nunique().reset_index()
    df_agg2 = df_merge.groupby(["store_code", "period"], as_index=False)[["sales"]].sum()

    for df in [df_agg, df_agg2, df_register]:
        for col in ["store_code", "period"]:
            df[col] = df[col].astype("str")

    df_out = df_agg.merge(
        df_agg2, on=["store_code", "period"], how="left"
    ).merge(df_register, on=["store_code", "period"], how="left")

    return df_out

if __name__ == "__main__":
    df_store = pd.read_excel(os.path.join(config.ref_dir, "dim_stores_offline_v5.xlsx"), engine="openpyxl")
    df_register = pd.read_csv(os.path.join(config.raw_dir, "crm", "register.csv"))

    df_out = check_pink_register("2020", df_store, df_register)
    df_out.to_csv(os.path.join(config.temp_dir, "xingyu", "store_register_cm_v2.csv"), index=False)