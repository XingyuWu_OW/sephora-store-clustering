import os
import sys
import numpy as np
import pandas as pd
import warnings
warnings.filterwarnings("ignore")

from src import config

def convert_str(x):
    try:
        return str(int(x))
    except:
        return x

def get_dim_product():
    df_product = pd.read_excel(os.path.join(config.ref_dir, "dim_product_with_txn.xlsx"), engine="openpyxl")
    df_product = df_product[["product_id", "sku_code", "category", "Range", "Segment"]].rename(
        columns={"Range": "Range_old", "Segment": "Segment_old"}
    )

    df_sk_func = pd.read_excel(
        os.path.join(config.raw_dir, "SKU_Classification", "sk_classification.xlsx"), 
        engine="openpyxl", sheet_name="SKU")
    raw_cols = df_sk_func.columns.values
    df_sk_func.columns = [col.replace("\n", "").strip() for col in raw_cols]
    sel_cols = [col for col in df_sk_func.columns if "Unnamed" not in col]
    df_sk_func = df_sk_func[sel_cols].rename(columns={"Mat." : "sku_code"})

    df_sk_func["sku_code"] = df_sk_func["sku_code"].apply(lambda x: convert_str(x))

    df_sk_dim_product = df_product.merge(df_sk_func, on=["sku_code"], how="left")
    print(df_sk_dim_product.head(10))
    return df_sk_dim_product


def run_function_agg():
    df_sk_dim_product = get_dim_product()
    df_store = pd.read_excel(os.path.join(config.ref_dir, "dim_stores_offline_v5.xlsx"), engine="openpyxl")

    df_txn = pd.read_parquet(os.path.join(config.sdl_dir, "txn_store_sku_monthly_v2.parquet"))
    df_filter = df_txn.merge(df_store[["store_id", "store_code"]], on=["store_id"])

    df_merge = df_filter.merge(df_sk_dim_product, on=["product_id"], how="left")
    df_sk = df_merge[df_merge["category"]=="SKINCARE"]

    key_cols = ["category", "Range_old", "Segment_old", "Range", "Segment", "Sub Segment", "First Function"]

    for col in key_cols:
        if col in df_sk.columns:
            df_sk[col].fillna("_NaN", inplace=True)
    
    join_cols = ["store_code", "year"] + key_cols
    df_agg1 = df_sk.groupby(join_cols)[["month"]].nunique().reset_index()
    df_agg2 = df_sk.groupby(join_cols, as_index=False)[["qty", "sales"]].sum()

    df_agg = df_agg1.merge(df_agg2, on=join_cols)
    
    return df_agg

if __name__ == "__main__":
    df_agg = run_function_agg()
    df_agg.to_csv("/data/temp/xingyu/sk_function_agg.csv", index=False)