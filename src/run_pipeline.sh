#!/bin/bash -e

source ~/.bashrc

echo "---- data cleaning ----"
python3 src/input/dim_date.py
python3 src/input/clean_shelf_product.py
python3 src/input/clean_space_meter.py
python3 src/input/clean_cn_mini.py
python3 src/input/get_store_attributes.py

bash src/input/split_txn.sh
python3 src/input/standardize_txn.py
python3 src/input/filter_txn.py  # optional step

echo "---- gen sdl ----"
python3 src/sdl/txn_sales_sdl.py
python3 src/sdl/txn_sales_agg.py
python3 src/sdl/filter_dim_product.py
python3 src/sdl/shelf_sdl.py
python3 src/sdl/crm_sdl.py

echo "---- run assortment ----"
python3 src/model/assortment_engine.py assortment_config.json v1

latest_assortment=`ls -lth /data/model/assortment/|head -2|tail -1|awk -F" " '{print $NF}'`
python3 src/model/post_assortment.py ${latest_assortment}