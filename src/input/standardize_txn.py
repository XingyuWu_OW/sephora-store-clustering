import os
import sys
import datetime
import numpy as np
import pandas as pd

from src import config

def standardize_txn(filepath):
    """
    Standardize transaction table
    """
    df = pd.read_csv(filepath, header=None)
    print("finished reading {}".format(filepath))
    df.columns = [
        "txn_ts", "txn_id", "product_id", "store_id", "customer_id", "qty", "sales_at_fullprice", "sales", 
        "promo_code", "promo_type", "coupon_code", "coupon_type", "sample_issued", "markdown", "margin"
    ]
    df["txn_date"] = df["txn_ts"].astype("str").str[:10]
    df["raw_sales"] = df["sales_at_fullprice"] * df["qty"]
    df["discount"] = df["raw_sales"] - df["sales"]

    df["discount_pct"] = np.where(
        df["raw_sales"] != 0,
        df["discount"] / df["raw_sales"],
        0
    )
    out_cols = [
        "txn_ts", "txn_date", "txn_id", "product_id", "store_id", "customer_id", "qty", "sales_at_fullprice", "sales", 
        "raw_sales", "promo_code", "promo_type", "margin", "discount", "discount_pct"
    ]
    print("finished standardizing {}".format(filepath))
    return df[out_cols]

if __name__=='__main__':
    src_dir = config.raw_dir
    out_dir = config.raw_processed

    txn_2018 = os.path.join(src_dir, "txn_2018.csv.gz")
    txn_2019 = os.path.join(src_dir, "txn_2019.csv.gz")
    txn_2020 = os.path.join(src_dir, "txn_2020.csv.gz")
    txn_20201231 = os.path.join(src_dir, "Transcation_20201231.csv")

    df_txn_2018 = standardize_txn(txn_2018)
    df_txn_2018.to_parquet(os.path.join(out_dir, "txn_2018_clean.parquet"), index=False)
    
    df_txn_2019 = standardize_txn(txn_2019)
    df_txn_2019.to_parquet(os.path.join(out_dir, "txn_2019_clean.parquet"), index=False)
    
    df_txn_2020 = standardize_txn(txn_2020)
    df_txn_20201231 = standardize_txn(txn_20201231)
    df_txn_2020_all = pd.concat([df_txn_2020, df_txn_20201231])

    df_txn_2020_all.to_parquet(os.path.join(out_dir, "txn_2020_clean.parquet"), index=False)