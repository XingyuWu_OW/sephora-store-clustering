import os
import datetime
import numpy as np
import pandas as pd

from src import config

def _get_weekstart(dt):
    return dt - datetime.timedelta(days = dt.weekday())

def gen_dim_date(start_date="2018-01-01", end_date="2021-12-31"):
    """
    Generate date dimension table

    Args:
        start_date (str): start date of the date dimension table, default is 2018-01-01
        end_date (str): end date of the date dimension table, default is 2021-12-31

    Returns:
        df: date dimension table in Pandas DataFrame
    """    
    range = pd.date_range(start=start_date, end=end_date)

    df = pd.DataFrame({"date": range})
    df["weekstartdate"] = df["date"].apply(lambda x: _get_weekstart(x))

    df["month"] = df["date"].astype("str").str[:7]
    df["month_no"] = df["date"].apply(lambda x: x.month)
    df["quarter"] = np.where(
        df["month_no"] <=3,
        "Q1",
        np.where(
            df["month_no"] <=6,
            "Q2",
            np.where(
                df["month_no"] <=9,
                "Q3",
                "Q4"
            )
        )
    )

    df["year"] = df["month"].str[:4]
    df["dow"] = df["date"].apply(lambda x: x.weekday()+1)
    df["quarter_year"] = df["year"] + df["quarter"]
    df["weekend_ind"] = np.where(
        df["dow"].isin([6, 7]),
        1,
        0
    )
    df["week_type"] = df["year"] + "_"  + df["weekend_ind"].astype("str")

    return df

if __name__=="__main__":
    df_date = gen_dim_date()
    df_date.to_csv(os.path.join(config.ref_dir, "dim_date.csv"), index=False, encoding="utf-8")
