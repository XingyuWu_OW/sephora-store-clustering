import os
import datetime
import pandas as pd
import warnings
from dateutil.parser import parse
warnings.filterwarnings("ignore")

from src import config
from src.utils.common import convert_int

def read_one_file(filepath):
    df = pd.read_excel(filepath, engine="openpyxl", skiprows=[0])
    return df

def get_rename_dict():
    rename_dict = {
    "opening_time": "opening_date"
    }
    return rename_dict

def convert_excel_date_to_int(x):
    x = str(x)
    if any(x.startswith(yr) for yr in ["1900-", "1901-", "1902-", "1903-", "1904-", "1905-"]):
        base_dt = datetime.datetime(1900, 1, 1, 0, 0)
        dt = parse(x)
        out = str((dt - base_dt).days)
    else:
        out = x
    return out

def read_all():
    """
    Read all Linear Meter tables, extract store attributes and combine
    """
    src = os.path.join(config.planogram_dir, "Linear_Meter")
    rename_dict = get_rename_dict()
    all_stores = []
    for yr in ["2018", "2019", "2020"]:
        src_dir = os.path.join(src, yr)
        print(src_dir)
        for filename in os.listdir(src_dir):
            if filename.endswith(".xlsx") and all(x not in filename for x in ["V2 2018.10.11-Q4", "V1 2019.1.10-Q1 V2", "V1 2019.07.04-Q3"]):
                file_path = os.path.join(src_dir, filename)
                print(file_path)
                df = read_one_file(file_path)
                new_col_names = []
                for col in df.columns:
                    new_col_names.append(col.lower().replace("\n", "").replace("(", "").replace(")", "").strip().replace(" ", "_"))
                df.columns = new_col_names
                df = df.rename(columns=rename_dict)
                sel_cols = ["store_code", "store_name_cn", "region", "district", "city", "store_type", "opening_date", "total_area", "sales_area", "tier"]
                exist_cols = []
                for col in sel_cols:
                    if col in df.columns:
                        exist_cols.append(col)
                stores = df[exist_cols]
                for col in sel_cols:
                    if col not in df.columns:
                        stores[col] = ""        
                stores = stores[sel_cols] 
                stores["year"] = yr
                q_index = filename.index("Q")
                stores["quarter"] = filename[q_index:q_index+2]
                all_stores.append(stores)
    
    df_out = pd.concat(all_stores)
    df_out = df_out[~pd.isnull(df_out["store_code"])]
    df_out["store_code"] =  df_out["store_code"].astype("int").astype("str")
    df_out["opening_date"] = df_out["opening_date"].astype("str").str[:10]
    df_out["store_type"] = df_out["store_type"].str.strip()
    for col in ["city", "region", "district"]:    
        df_out[col] = df_out[col].str.lower()
    for col in ["total_area", "sales_area"]:
        df_out[col] = df_out[col].apply(lambda x: convert_excel_date_to_int(x))
    
    df_out["open_date"] = df_out["opening_date"].apply(lambda x: clean_opening_date(x))
    return df_out

def clean_opening_date(dt):
    base = datetime.date(1900, 1, 1)
    try:
        dt_float = float(dt)
        dt_correct = base + datetime.timedelta(days=dt_float)
        dt_out = dt_correct.strftime("%Y-%m-%d")
        return dt_out
    except:
        return dt
    
def adhoc_fix(df_raw):
    """
    Apply adhoc fix
    ** Please check / review the necessity of this step in production **
    """
    df = df_raw.copy()
    
    # manually add close_date for store_code
    df.loc[df["store_code"]==6139, "close_date"] = "2020-02-06"
    df.loc[df["store_code"]==6032, "close_date"] = "2020-06-17"
    df.loc[df["store_code"]==6055, "close_date"] = "2020-02-10"
    df.loc[df["store_code"]==6290, "close_date"] = "2020-09-13"
    df.loc[df["store_code"]==6157, "close_date"] = "2020-11-02"
    df.loc[df["store_code"]==6219, "close_date"] = "2020-11-08"

    # manually fix guiyang stores from south to west
    df.loc[df["city"]=="guiyang", "region"] = "West region"

    # add store type
    df.loc[df["store_code"]==6292, "store_type"] = "Suburban Hub"
    df.loc[df["store_code"]==6299, "store_type"] = "Suburban Proximity"
    df.loc[df["store_code"]==6395, "store_type"] = "Suburban Hub"
    
    return df


def gen_dim_store(df_store_attr):
    """
    Drive store dimension table from weekly Linear Meter tables and DimStore.csv
    ** Please check / review the necessity of manual corrections in production **
    """
    dim_store = pd.read_csv(os.path.join(config.raw_dir, "DimStore.csv"), low_memory=False)
    dim_store = dim_store[["store_id", "store_code", "region", "city", "close_date"]]
    dim_store["city"] = dim_store["city"].str.lower()
    
    for col in ["close_date"]:
        dim_store[col] = pd.to_datetime(dim_store[col], format="%d-%m-%Y").dt.strftime("%Y-%m-%d")

    dim_city = pd.read_csv(os.path.join(config.ref_dir, "dim_city.csv"))

    df_store_attr = df_store_attr.sort_values(by=["store_code", "year", "quarter"], ascending=False)
    df_store_attr_latest = df_store_attr.groupby(["store_code"], as_index=False)[
        ["store_name_cn", "store_type", "total_area", "sales_area", "open_date"]
    ].first()
    df_store_attr_latest["store_code"] = df_store_attr_latest["store_code"].astype("int")

    df_offline = df_store_attr_latest.merge(
        dim_store, on=["store_code"], how="left"
    ).merge(dim_city, on=["city"], how="left")

    df_offline = df_offline.rename(columns={"store_name_cn": "store_name"})
    df_out = adhoc_fix(df_offline)

    out_cols = ["store_id", "store_code", "store_name", "store_type", "total_area", "sales_area", "open_date", "close_date", 
    "region", "city", "province", "region_gov", "region_client", "city_tier", "city_rank"]

    return df_out[out_cols]
    

if __name__=="__main__":
    out_dir = config.raw_processed
    df_out = read_all()
    df_out.to_csv(os.path.join(out_dir, "store_attributes_quarterly.csv"), encoding="utf_8_sig", index=False)
    df_offline = gen_dim_store(df_out)
    df_offline.to_excel(os.path.join(config.ref_dir, "dim_stores_offline_v5.xlsx"), encoding="utf_8_sig", index=False)
