import os
from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient

# need to run `source ~/.env` at command line first before running this code
connect_str = os.getenv('AZURE_STORAGE_CONNECTION_STRING')
data_dir = os.getenv('DATA_DIR')
print("check data dir -- ", data_dir)

blob_service_client = BlobServiceClient.from_connection_string(connect_str)

for c in blob_service_client.list_containers():
    container_path = os.path.join(data_dir, c.name)
    if not os.path.exists(container_path):
        os.mkdir(container_path)
    container_client = blob_service_client.get_container_client(c)
    existing_files = os.listdir(container_path)
    for b in container_client.list_blobs():
        b_name = b.name.replace(" ", "_")
        out_path = os.path.join(container_path, b_name)
        if not os.path.exists(out_path):
            print("downloading blob {} in container {} to {}".format(b.name, c.name, out_path))
        
            cmd = "az storage blob download -c \'{}\' -n \'{}\' -f {} --connection-string \'{}\'".format(c.name, b.name, out_path, connect_str)
            os.system(cmd)
        

