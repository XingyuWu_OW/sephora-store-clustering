import os
import re
import sys
import numpy as np
import pandas as pd

from src import config
from src.utils import io

def clean_shelf_product(src_dir, out_dir):
    """
    Combine and clean weekly shelf product tables
    """
    all_df = []
    for filename in os.listdir(src_dir):
        filepath = os.path.join(src_dir, filename)
        if filepath.endswith(".TXT"):
            df = io.read_txt(filepath)
            week = filename.split(".")[0].split("_")[3]
            weekstartdate = pd.to_datetime(week, format="%Y%m%d").strftime("%Y-%m-%d")
            df["weekstartdate"] = weekstartdate
            all_df.append(df)

    df_out = pd.concat(all_df)
    df_out = df_out[df_out["country"]=="China"]
    df_out.to_csv(os.path.join(out_dir, "shelf_product.csv"), index=False)
    return df_out

if __name__ == '__main__':
    src_dir = os.path.join(config.planogram_dir, "shelf_product")
    out_dir = config.raw_processed
    clean_shelf_product(src_dir, out_dir)
    
