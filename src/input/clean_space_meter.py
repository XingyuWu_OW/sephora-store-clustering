import os
import re
import sys
import numpy as np
import pandas as pd

from src import config
from src.utils import io

def clean_space_meter(src_dir, out_dir):
    """
    Combine and clean weekly space meter and space shelf meter tables
    """
    meter_df = []
    shelf_df = []
    for filename in os.listdir(src_dir):
        filepath = os.path.join(src_dir, filename)
        if filepath.endswith(".TXT"):
            df = io.read_txt(filepath)
            df = df[df["country"]=="China"]
            if "_SHELF_" in filename:
                week = filename.split(".")[0].split("_")[4]
                weekstartdate = pd.to_datetime(week, format="%Y%m%d").strftime("%Y-%m-%d")
                df["weekstartdate"] = weekstartdate
                shelf_df.append(df)
            else:
                week = filename.split(".")[0].split("_")[3]
                weekstartdate = pd.to_datetime(week, format="%Y%m%d").strftime("%Y-%m-%d")
                df["weekstartdate"] = weekstartdate
                meter_df.append(df)             

    df_shelf = pd.concat(shelf_df)
    df_meter = pd.concat(meter_df)
    
    df_shelf.to_csv(os.path.join(out_dir, "space_shelf_meter.csv"), index=False)
    df_meter.to_csv(os.path.join(out_dir, "space_meter.csv"), index=False)
    return df_shelf, df_meter


if __name__ == '__main__':
    src_dir = os.path.join(config.planogram_dir, "space_meter")
    out_dir = config.raw_processed
    df1, df2 = clean_space_meter(src_dir, out_dir)
    
