import os
import sys
import datetime
import numpy as np
import pandas as pd

from src import config
from src.utils import io, common

def _filter_eb(df):
    """
    Filter EB transactions
    """
    eb_stores = [3, 254, 999, 1017, 1019, 1087, 1211]
    df_filter = df[df["store_id"].isin(eb_stores)]
    print("| -- eb stores filtered...")
    return df_filter

def _filter_offline(df):
    """
    Filter offline transactions
    """
    dim_store = io.read_excel(os.path.join(config.ref_dir, "dim_stores_offline_v5.xlsx"))
    offline_stores = dim_store["store_id"].unique()
    df_filter = df[df["store_id"].isin(offline_stores)]
    print("| -- offline stores filtered...")
    return df_filter

def get_txn_offline_eb():
    sel_cols = ["margin", ""]
    df1 = pd.read_parquet(os.path.join(config.raw_processed, "txn_2018_clean.parquet"))
    df1 = common.reduce_size(df1)
    df2 = pd.read_parquet(os.path.join(config.raw_processed, "txn_2019_clean.parquet"))
    df2 = common.reduce_size(df2)
    df3 = pd.read_parquet(os.path.join(config.raw_processed, "txn_2020_clean.parquet"))
    df3 = common.reduce_size(df3)

    df1_filter = _filter_offline(df1)
    df2_filter = _filter_offline(df2)
    df3_filter = _filter_offline(df3)

    df = pd.concat([df1_filter, df2_filter, df3_filter])
    print("| -- offline store table combined...")
    df.to_csv(os.path.join(config.raw_processed, "txn_offline_clean_v2.csv"), chunksize=100000, index=False)
    print("| -- offline store filter finished...")
    df = pd.DataFrame()

    df1_eb = _filter_eb(df1)
    df2_eb = _filter_eb(df2)
    df3_eb = _filter_eb(df3)

    df_eb = pd.concat([df1_eb, df2_eb, df3_eb])
    df_eb.to_parquet(os.path.join(config.raw_processed, "txn_eb_clean_v2.parquet"), index=False)
    print("| -- eb store filter finished...")

if __name__=='__main__':
    get_txn_offline_eb()