#!/bin/bash -e

# break transaction raw data into 3 files (temporary, due to limited resource on server)
for yr in 2018 2019 2020;
    do
        cat /data/storeclustringdata/Transcation.csv|awk -F"-" '$1==yr{print $0}' yr=${yr}|gzip > /data/storeclustringdata/txn_${yr}.csv.gz
    done