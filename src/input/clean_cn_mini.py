import os
import re
import sys
import numpy as np
import pandas as pd

from src import config
from src.utils import io

def combine_mini(src_dir, out_dir):
    """
    Combine and clean weekly mini data
    """
    all_df = []
    for filename in os.listdir(src_dir):
        filepath = os.path.join(src_dir, filename)
        if filepath.endswith(".TXT"):
            df = io.read_txt(filepath)
            week = re.findall(r'\d+', filename)[0]
            weekstartdate = pd.to_datetime(week, format="%Y%m%d").strftime("%Y-%m-%d")
            df["weekstartdate"] = weekstartdate
            all_df.append(df)

    df_out = pd.concat(all_df)
    raw_cols = df_out.columns.values.copy()
    df_out.columns = [col.lower() for col in raw_cols]
    out_cols = ["store", "sapcode", "mini", "weekstartdate"]
    df_out[out_cols].to_parquet(os.path.join(out_dir, "cn_mini_weekly.parquet"), index=False)
    return df_out


if __name__ == '__main__':
    src_dir = os.path.join(config.planogram_dir, "CN_MINI_OUTPUT")
    out_dir = config.raw_processed
    combine_mini(src_dir, out_dir)
    
