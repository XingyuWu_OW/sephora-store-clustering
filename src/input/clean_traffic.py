import os
import re
import sys
import numpy as np
import pandas as pd

from src import config
from src.utils import io
from src.utils.common import convert_int

def clean_dim_store(src_dir, out_dir):
    df = pd.read_csv(os.path.join(src_dir, "DimStore.csv"))
    df["store_id"] = df["store_id"].apply(lambda x: convert_int(x))
    df.to_csv(os.path.join(out_dir, "dim_store_clean.csv"), index=False, encoding="utf_8_sig")

    return df

def clean_traffic(src_dir, out_dir):
    dim_store = clean_dim_store(src_dir, out_dir)
    df_traffic = pd.read_csv(os.path.join(src_dir, "traffic.csv"), header=None, names=["date", "store_code", "traffic_in", "traffic_out"])
    df_traffic["store_code"] = df_traffic["store_code"].astype("int")
    df_traffic = df_traffic[df_traffic["store_code"].isin(dim_store["store_code"].unique())]
    
    df_traffic["date"] = pd.to_datetime(df_traffic["date"], format="%Y%m%d").dt.strftime("%Y-%m-%d")
    df_traffic = df_traffic.sort_values(by=["date"])
    df_traffic.to_csv(os.path.join(out_dir, "traffic_clean.csv"), index=False)
    return df_traffic

if __name__ == '__main__':
    src_dir = config.raw_dir
    out_dir = config.raw_processed
    ref_dir = config.ref_dir
    clean_dim_store(src_dir, ref_dir)
    clean_traffic(src_dir, out_dir)
    
