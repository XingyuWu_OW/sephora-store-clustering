import numpy as np
import pandas as pd

from src import config

def get_total_sales():
    """
    Calculate store level total monthly average sales
    """
    df = pd.read_csv(os.path.join(config.sdl_dir, "txn_store_category_yearly_v2.csv"))
    df["avg_monthly_sales"] = df["sum_sales"] / df["cnt_months_w_sales"]
    df = df[df["year"]>=2019]
    df_agg = df.groupby(["year", "store_code"], as_index=False)[["avg_monthly_sales"]].sum()
    df_agg2 = df_agg.groupby(["store_code"], as_index=False)[["avg_monthly_sales"]].mean().rename(
        columns={"avg_monthly_sales": "store_total_sales"})
    return df_agg2

def derive_cumsum(df):
    """
    Derive cumulative sum
    """
    df["cumsum_share"] = df.groupby(["store_code"])["share"].cumsum()
    df["cumsum_shelf"] = df.groupby(["store_code"])["shelf"].cumsum()
    return df

def form_add_remove(df_store):
    """
    split add and remove as separate tables from assortment table
    """
    total = df_store["avg_monthly_sales"].sum()
    df_store['share'] = df_store["avg_monthly_sales"]/total
    
    add_sel_cols = ["store_code", "brand", "category", "assortment", "require_shelf", "other_store_weighted_avg_share"]
    rm_sel_cols = ["store_code", "brand", "category", "assortment", "shelf", "share"]
    df_add = df_store[df_store["assortment"]=="ADD"][add_sel_cols].rename(
        columns={"require_shelf": "shelf", "other_store_weighted_avg_share": "share"}
    )
    df_add.fillna(0, inplace=True)
    df_add = df_add.sort_values(by=["share"]).reset_index(drop=True)
    df_remove = df_store[df_store["assortment"]=="SUGGEST REMOVE"][rm_sel_cols]
    df_remove = df_remove.sort_values(by=["share"], ascending=False).reset_index(drop=True)
    df_add = derive_cumsum(df_add)
    df_remove = derive_cumsum(df_remove)
    return df_add, df_remove

def optimize_add_remove(df_add_raw, df_remove_raw, shelf_diff_thres=1):
    """
    Adjust add and remove based on sales addition proxy vs. sales removal
    """
    df_remove = df_remove_raw.copy()
    df_add = df_add_raw.copy()
    while len(df_remove) > 0 and len(df_add) > 0 and (df_remove["share"].max() > df_add["share"].min()):
        remove_cumsum_shelf = df_remove.head(1)["cumsum_shelf"].values[0]
        df_add["diff"] = df_add["cumsum_shelf"] - remove_cumsum_shelf
        df_add["diff_tag"] = np.where(
            ((df_add["diff"] >=0) & (df_add["diff"]<=shelf_diff_thres)) | 
            ((df_add["diff"]<0) & (df_add["diff"]>=shelf_diff_thres*(-1))),
            1,
            0
        )
        df_add_filter = df_add[df_add["diff_tag"]==1]
        if len(df_add_filter) > 0:
            add_cumsum_shelf = df_add_filter.head(1)["cumsum_shelf"].values[0]
            add_cumsum_share = df_add_filter.head(1)["cumsum_share"].values[0]
            remove_cum_share = df_remove[df_remove["cumsum_shelf"]==remove_cumsum_shelf]["cumsum_share"].values[0]
            if remove_cum_share > add_cumsum_share:
                df_remove = df_remove[df_remove["cumsum_shelf"]!=remove_cumsum_shelf]
                df_add = df_add[df_add["cumsum_shelf"]>add_cumsum_shelf]
                df_add = derive_cumsum(df_add)
                df_remove = derive_cumsum(df_remove)
            else:
                break
        else:
            break
        
    return df_add, df_remove

def join_back(df_store, df_add, df_remove):
    """
    Join back the adjusted assortment
    """
    out_cols = list(df_store.columns.values)
    key_cols = ["brand", "category"]
    df_add.rename(columns={"assortment": "assortment_add"}, inplace=True)
    df_remove.rename(columns={"assortment": "assortment_remove"}, inplace=True)
    
    df_merge = df_store.merge(
        df_add[key_cols + ["assortment_add"]], on=key_cols, how="left"
    ).merge(
        df_remove[key_cols + ["assortment_remove"]], on=key_cols, how="left"
    )
    
    df_merge["assortment_adjust"] = np.where(
        df_merge["assortment_add"] == "ADD",
        "ADD",
        np.where(
            df_merge["assortment_remove"] == "SUGGEST REMOVE",
            "SUGGEST REMOVE",
            np.where(
                df_merge["assortment"] == "SUGGEST REMOVE",
                "KEEP",
                np.where(
                    df_merge["assortment"] == "ADD",
                    "NOT ADD",
                    df_merge["assortment"]
                )
            )
        )
    )
    
    return df_merge[out_cols + ["assortment_adjust"]]

def run_adjust(df_all_assortment, thres=3):
    df = df_all_assortment
    adjust = []
    still_need_fix = []
    for store in df["store_code"].unique():
        df_store = df[df["store_code"]==store].reset_index(drop=True)
        df_add, df_remove = form_add_remove(df_store)
        df_add_adjust, df_remove_adjust = optimize_add_remove(df_add, df_remove, thres)
        if df_remove_adjust["share"].max() > df_add_adjust["share"].min():
            still_need_fix.append(store)
        df_store_adjust = join_back(df_store, df_add_adjust, df_remove_adjust)
        adjust.append(df_store_adjust)
    
    df_adjust = pd.concat(adjust)
    return df_adjust