import os
import sys
import numpy as np
import pandas as pd

import warnings
warnings.filterwarnings("ignore")

from src import config

def read_data(filepath):
    df = pd.read_csv(filepath)
    return df

def merge_data(df1,df2):
    df_merge = pd.merge(df1,df2,on='store_code', how='left')
    #df_merge.to_csv(outpath, index=False, encoding='utf_8_sig')
    return df_merge

def add_sales(assort,share,sales):
    if assort == "ADD":
        real_share = share * sales
    elif assort == "REMOVE":
        real_share = 0
        #real_share = 0 - share
    else:
        real_share = 0
    return real_share

def add_sales_sku(sku_opportunity, assort, share, sales):
    if (sku_opportunity == 1) | (assort == "KEEP"):
        real_share = share * sales
    else:
        real_share = 0
    return real_share

def add_sales_max(assort,share,sales):
    if (assort == "ADD") | (assort == "NOT ADD"):
        real_share = share * sales
    elif assort == "REMOVE":
        real_share = 0
        #real_share = 0 - share
    else:
        real_share = 0
    return real_share

def remove_sales(assort,sales):
    if assort == "SUGGEST REMOVE":
        real_share = 0 - sales
    else:
        real_share = 0
    return real_share

if __name__ == '__main__':
    filename = sys.argv[2]
    input_store_cluster = sys.argv[1]
    outpath = "/data/model/assortment/measurement/measure_{}.csv".format(filename)
    df_store_cluster = pd.read_csv(os.path.join(config.model_dir, "cluster", input_store_cluster))
    df_store_cluster = df_store_cluster[["store_code","cluster"]]
    df_store_cluster_list = df_store_cluster[["cluster"]]
    df_store_cluster_list = df_store_cluster_list.drop_duplicates()
    #df_store_cluster_list = ["ALL"]

    #store sales, choose 19-20, aggregate to store sales
    input_store_sales = "/data/sdl/txn_store_category_yearly_v2.csv"
    df_store_sales = read_data(input_store_sales)
    df_store_sales['average_month_sales'] = df_store_sales.apply(
        lambda x: x['sum_sales'] / x['cnt_months_w_sales'], axis=1)
    df_store_sales = df_store_sales[["year","store_code","average_month_sales"]]
    df_store_sales = df_store_sales[(df_store_sales["year"]== 2019) | (df_store_sales["year"]== 2020)]
    df_store_sales = df_store_sales.groupby(["year","store_code"],as_index = False).aggregate(np.sum)
    df_store_sales = df_store_sales.groupby("store_code",as_index = False).aggregate(np.average)
    df_store_sales = df_store_sales[["store_code","average_month_sales"]]

    df_raw = read_data("/data/model/assortment/{}/cluster_assortment_sku.csv".format(filename))

    df_input_cluster_result_sku = df_raw[
        ["store_code", "brand", "category", "assortment", "other_store_weighted_avg_share", 
        "avg_monthly_sales"]]
    df_input_cluster_result_sku.rename(columns={"avg_monthly_sales": "monthly_avg_sales"},
                                    inplace=True)

    sel_cols = ["store_code", "brand", "category", "assortment", "other_store_weighted_avg_share", "monthly_avg_sales"]
    df_input_cluster_result = df_input_cluster_result_sku[sel_cols].drop_duplicates()

    sel_cols2 = ["store_code", "brand", "category", "assortment", "other_store_weighted_avg_share", "monthly_avg_sales"]
    df_input_cluster_result = df_input_cluster_result[sel_cols2]

    df_input_cluster_result = df_input_cluster_result.groupby(["store_code", "assortment", "brand", "category"], as_index = False).aggregate(np.sum)
    df_input_cluster_result = df_input_cluster_result.merge(df_store_sales, on=["store_code"])


    df_input_cluster_result["sales_add"] = df_input_cluster_result.apply(
        lambda x: add_sales(x["assortment"], x["other_store_weighted_avg_share"], x["average_month_sales"]), axis=1)

    df_input_cluster_result["sales_add_max"] = df_input_cluster_result.apply(
        lambda x: add_sales_max(x["assortment"], x["other_store_weighted_avg_share"], x["average_month_sales"]), axis=1)

    df_input_cluster_result["sales_remove"] = df_input_cluster_result.apply(
        lambda x: remove_sales(x["assortment"], x["monthly_avg_sales"]), axis=1)

    df_agg1 = df_input_cluster_result.groupby(
        ["store_code"],as_index = False).aggregate(np.sum)
    df_agg2 = df_input_cluster_result[df_input_cluster_result["assortment"]=="ADD"].groupby(
        ["store_code"],as_index = False)[["sales_add"]].min().rename(
        columns={"sales_add": "sales_add_min"})[["store_code", "sales_add_min"]]
    df_agg3 = df_input_cluster_result[df_input_cluster_result["assortment"]=="SUGGEST REMOVE"].groupby(["store_code"],as_index = False)[["sales_remove"]].min().rename(
        columns={"sales_remove": "sales_remove_max"})[["store_code", "sales_remove_max"]]

    #df_input_cluster_result = df_input_cluster_result[["store_code", "sales_add", "sales_remove", "sales_add_max"]]
    df_add_remove_sales = df_agg1[["store_code", "sales_add", "sales_remove", "sales_add_max"]].merge(
        df_agg2, on=["store_code"], how="left").merge(df_agg3, on=["store_code"], how="left")

    total_add = df_add_remove_sales["sales_add"].sum() * 12
    total_remove = df_add_remove_sales["sales_remove"].sum() * 12
    net_opp = total_add + total_remove

    print("| -- total add is {:,}".format(total_add))
    print("| -- total remove is {:,}".format(total_remove))
    print("| -- net opportunity is {:,}".format(net_opp))
