import os
import sys
import datetime
import numpy as np
import pandas as pd

import warnings
warnings.filterwarnings("ignore")

from src import config
from src.utils.common import convert_int, get_rank

def read_dim_product(filepath):
    """
    Load product dimension table
    """
    if filepath.endswith("xlsx"):
        df = pd.read_excel(filepath, engine="openpyxl")
    else:
        df = pd.read_csv(
            filepath, 
            low_memory=False, 
            header=None, 
            names=["product_id", "brand_type", "brand", "category", "sku_code", "target", "product_name", "product_name_en", 
            "Range", "Segment", "skincare_function", "brand_code"]
        )
    for col in ["Segment", "Range"]:
        df[col].fillna("_NaN", inplace=True)
    
    return df

def correct_brand_category(df_raw, df_product, df_category_correct):
    """
    Adjust categories of certain brands as per POG arrangement
    """
    df = df_raw.copy()
    df = df.merge(df_category_correct, on=["brand", "category"], how="left")
    df["category"] = df["category_new"].combine_first(df["category"])
    df.drop("category_new", axis=1) 

    # Break CHLOE FR to 2 brands
    df_chloe = df_product[
        (df_product["brand"]=="CHLOE") & 
        (df_product["category"]=="FRAGRANCE")
    ]
    df_chloe["ind_adf"] = df_chloe["product_name_en"].apply(lambda x: 1 if "ATELIER DES FLEURS" in x else 0)
    chloe_adf = df_chloe[df_chloe["ind_adf"]==1]["sku_code"].unique()

    df["brand"] = np.where(
        df["sku_code"].isin(chloe_adf),
        "CHLOE_ADF",
        df["brand"]
    )

    # Break MEN's into one category
    men_brands = [
        "BIOTHERM",
        "CLARINS",
        "CLINIQUE",
        "DIOR",
        "DTRT",
        "JACK BLACK",
        "LAB SERIES",
        "SEPHORA",
        "SHISEIDO",
        "VS",
        "SEBASTIAN"
    ]
    men_skus = df_product[
        (df_product["brand"].isin(men_brands)) & 
        (df_product["category"] == "SKINCARE") &
        (df_product["target"] == "MEN")  
    ]["sku_code"].unique()
    men_skus = list(men_skus) + [436376, 465427]

    df["category"] = np.where(
        df["sku_code"].isin(men_skus),
        "MEN_SC",
        df["category"]
    )

    return df

def derive_brand_cat_share(df, city_col="city", province_col=None):
    """
    Derive sales share of a brand within category
    """
    if province_col is not None:
        geo_cols = [province_col, city_col]
    else:
        geo_cols = [city_col]
    df_agg1 = df.groupby(geo_cols+["month"], as_index=False)[["sales"]].sum()
    df_agg1_city = df_agg1.groupby(geo_cols, as_index=False)[["sales"]].mean().rename(columns={"sales": "city_level_sales"})

    df_agg2 = df.groupby(geo_cols+["brand", "category", "month"], as_index=False)[["sales"]].sum()
    df_agg2_brand_cat = df_agg2.groupby(geo_cols+["brand", "category"], as_index=False)[["sales"]].mean()

    df_out = df_agg2_brand_cat.merge(df_agg1_city, on=geo_cols)
    df_out["sales_share"] = df_out["sales"] / df_out["city_level_sales"]
    df_quantile = df_out.groupby(geo_cols+["category"], as_index=False)[["sales"]].quantile(.5).rename(columns={"sales": "sales_median"})
    df_out = df_out.merge(df_quantile, on=geo_cols+["category"])

    return df_out

def agg_eb_to_city(df_product, df_category_correct, df_city_en, out_dir):
    """
    Aggregate EB sales to city level
    """

    def _clean_city_name(x):
        city_exclude = ["亳州",
            "儋州",
            "兰州",
            "台州",
            "宿州",
            "常州",
            "广州",
            "徐州",
            "德州",
            "忻州",
            "惠州",
            "扬州",
            "抚州",
            "朔州",
            "杭州",
            "柳州",
            "梅州",
            "梧州",
            "永州",
            "池州",
            "沧州",
            "泉州",
            "泰州",
            "泸州",
            "温州",
            "湖州",
            "滁州",
            "滨州",
            "漳州",
            "潮州",
            "福州",
            "苏州",
            "荆州",
            "衢州",
            "贺州",
            "赣州",
            "达州",
            "郑州",
            "郴州",
            "鄂州",
            "钦州",
            "锦州",
            "随州"]

        x = str(x)
        for c in ["州", "市", "地区"]:
            if x.endswith(c) and x not in city_exclude:
                x = x.replace(c, "")
        return x
                
    df = pd.read_csv(
        os.path.join(config.raw_dir, "oms_sales_order_sku_level_df.csv"), 
        header=None, 
        names=["store_code", "province_cn", "city_cn", "date", "sku_code", "sku_name", "qty", "brand_raw", "sales"]
    ).drop("sku_name", axis=1)
    df["month"] = df["date"].str[:7]
    df["year"] = df["date"].str[:4]
    df_filter = df[df["year"].isin(["2019", "2020"])]
    df_filter.loc[:, "city_cn"] = np.where(
        df_filter["province_cn"].isin(["上海", "北京", "天津", "重庆"]),
        df_filter["province_cn"],
        df_filter["city_cn"]
    )

    df_filter.loc[:, "city_cn"] = df_filter["city_cn"].apply(lambda x: _clean_city_name(x))

    df_product = df_product[["sku_code", "brand", "category", "Range", "Segment", "product_name_en", "target"]].drop_duplicates()
    for df in [df_filter, df_product]:
        df["sku_code"] = df["sku_code"].apply(lambda x: convert_int(x))
    df_merge = df_filter.merge(df_product, on=["sku_code"], how="left")
    df_merge = df_merge[~pd.isnull(df_merge["brand"])]

    df_merge = correct_brand_category(df_merge, df_product, df_category_correct)
    df_agg = derive_brand_cat_share(df_merge, "city_cn", "province_cn")
    #df_agg.to_excel(os.path.join(out_dir, "eb_sales_by_city_raw.xlsx"), index=False, encoding="utf_8_sig")
    df_agg = df_agg.merge(df_city_en[["city_cn", "province_cn", "city", "province"]].drop_duplicates(), on=["city_cn", "province_cn"])

    df_agg["city"] = np.where(
        df_agg["city_cn"] == "杭州",
        "hangzhou",
        np.where(
            df_agg["city_cn"] == "广州",
            "guangzhou",
            df_agg["city"]
        )
    )

    col_mapping = {}
    for col in ["sales", "city_level_sales", "sales_share"]:
        col_mapping[col] = col + "_eb"
    
    df_agg.rename(columns=col_mapping, inplace=True)
    return df_agg

def agg_shelf(df_shelf, df_store):
    """
    Aggregate shelf to city level
    """
    city_mapping = {
        "nantongshi": "nantong",
        "shuhai": "zhuhai"
    }
    df_store["city"] = df_store["city"].apply(lambda x: city_mapping.get(x, x))
    df_store = df_store[
        pd.isnull(df_store["close_date"]) &
        ~(df_store["store_code"].isin([6126, 6395, 6292, 6299]))  # exclude flagship and outlet stores
    ]
    
    df_stores_per_city = df_store.groupby(["province", "city"], as_index=False)[["store_code"]].count().rename(
        columns={"store_code": "total_stores"}
    )

    df_shelf_merge = df_shelf.merge(df_store[["store_code", "city", "province"]], on=["store_code"])
    df_no_shelf = df_shelf_merge[pd.isnull(df_shelf_merge["shelf"])][
        ["province", "city", "store_code", "brand", "category"]].drop_duplicates()
    df_shelf_merge = df_shelf_merge[~pd.isnull(df_shelf_merge["shelf"])]

    df_shelf_agg = df_shelf_merge.groupby(["province", "city", "brand", "category"])[["store_code"]].nunique().reset_index().rename(
        columns={"store_code": "stores_with_shelf"}
    )

    return df_shelf_agg, df_stores_per_city, df_no_shelf

def tag_eb_opportunity(df_agg_eb, df_shelf_agg, df_stores_per_city, df_offline_agg, df_no_shelf):
    """
    Tag opportunity for EB
    """

    key_cols = ["province", "city", "brand", "category"]
    df_out = df_agg_eb.merge(df_stores_per_city, on=["province", "city"], how="left").merge(
        df_shelf_agg, on=key_cols, how="left"
    ).merge(
        df_offline_agg, on=key_cols, how="left"
    )
    df_out["stores_with_shelf"] = np.where(
        ~pd.isnull(df_out["total_stores"]) & pd.isnull(df_out["stores_with_shelf"]),
        0,
        df_out["stores_with_shelf"]
    )

    df_out["gap_stores"] = df_out["total_stores"] - df_out["stores_with_shelf"]

    core_categories = ["SKINCARE", "MAKE UP", "FRAGRANCE"]
    full_store_brands = ["DIOR", "CLARINS", "GUERLAIN", "CLINIQUE", "BIOTHERM"]
    df_out["ind_eb_opportunity_temp"] = np.where(
        (df_out["gap_stores"] > 0) & (df_out["sales_eb"] > df_out["sales_median"]) & \
        (df_out["category"].isin(core_categories)) & ~(df_out["brand"].isin(full_store_brands)),
        1,
        0
    )
    df_out["ind_eb_opportunity"] = np.where(
        pd.isnull(df_out["ind_offline_good"]),
        df_out["ind_eb_opportunity_temp"],
        np.where(
            df_out["ind_offline_good"] == 1,
            df_out["ind_eb_opportunity_temp"],
            0
        )
    )

    df_eb = df_out[df_out["ind_eb_opportunity"]==1].reset_index(drop=True)
    df_eb = df_eb.merge(df_no_shelf, on=["province", "city", "brand", "category"], how="left")

    return df_eb

def agg_offline_by_city(df_offline, df_store):
    """
    Aggregate offline sales to city level
    """
    geo_cols = ["province", "city"]
    df_offline = df_offline.merge(df_store[["store_code"]+geo_cols], on=["store_code"])
    df_offline_agg = df_offline.groupby(geo_cols+["brand", "category"], as_index=False)[["avg_monthly_sales"]].sum().rename(
        columns={"avg_monthly_sales": "sales_offline"})
    
    df_offline_median = df_offline_agg.groupby(
        geo_cols+["category"], as_index=False)[["sales_offline"]].quantile(.5).rename(
            columns={"sales_offline": "sales_offline_median"})
        
    df_offline_agg_out = df_offline_agg.merge(df_offline_median, on=geo_cols+["category"], how="left")
    df_offline_agg_out["ind_offline_good"] = np.where(
        df_offline_agg_out["sales_offline"] > df_offline_agg_out["sales_offline_median"],
        1,
        0
    )

    return df_offline_agg_out

def run_eb_opportunity(out_dir, df_shelf, df_store, df_offline):
    df_product_all = read_dim_product(os.path.join(config.raw_dir, "DimProduct_20210223_V2.csv"))
    df_city_en = pd.read_excel(os.path.join(config.input_dir, "dim_city_cn_en.xlsx"), engine="openpyxl")
    df_category_correct = pd.read_csv(os.path.join(config.input_dir, "dim_brand_category_correction.csv"))
    df_store_name = pd.read_excel(os.path.join(config.ref_dir, "dim_stores_offline_v5.xlsx"), engine="openpyxl")[["store_code", "store_name"]].rename(columns={"store_name": "store_name_raw"})
    df_store_name["store_name"] = df_store_name["store_code"].astype("str") + " " + df_store_name["store_name_raw"]

    df_agg_eb = agg_eb_to_city(df_product_all, df_category_correct, df_city_en, out_dir)
    df_shelf_agg, df_stores_per_city, df_no_shelf = agg_shelf(df_shelf, df_store)

    df_offline_agg = agg_offline_by_city(df_offline, df_store)
    df_out = tag_eb_opportunity(df_agg_eb, df_shelf_agg, df_stores_per_city, df_offline_agg, df_no_shelf)
    df_out.rename(
        columns={
            "sales_eb": "avg_monthly_sales_eb", 
            "sales_share_eb": "avg_monthly_sales_share_eb",
            "sales_offline": "avg_monthly_sales_offline"
        }, inplace=True
    )

    df_out = df_out.merge(df_store_name, on=["store_code"], how="left")

    out_cols = [
        "province_cn", "city_cn", "province", "city", "brand", "category", "avg_monthly_sales_eb", "avg_monthly_sales_offline", 
        "ind_offline_good", "total_stores", "stores_with_shelf", "gap_stores", "ind_eb_opportunity", "store_name"
    ]
    df_out[out_cols].to_excel(os.path.join(out_dir, "eb_opportunity.xlsx"), index=False, encoding="utf_8_sig")