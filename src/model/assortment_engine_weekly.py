import os
import sys
import datetime
import numpy as np
import pandas as pd

import warnings
warnings.filterwarnings("ignore")

from src import config

class StoreAssortment:
    def __init__(self, assort_config, cluster, stores, brand_type_dict, df_sales, df_shelf=None):
        self.config = assort_config
        self.brand_profile_threshold = self.config["brand_profile_threshold"]
        self.remove_threshold = self.config["remove_threshold"]
        self.cluster = cluster
        self.stores = stores
        self.df_sales = df_sales
        self.df_shelf = df_shelf
        self.key_cols = ["brand", "category"]
        self.core_categories = ["SKINCARE", "MAKE UP", "FRAGRANCE"]
        self.brand_type_dict = brand_type_dict
        self.must_keep = {
            "SKINCARE": ["LAUDER"],
            "FRAGRANCE": ["DIOR"],
            "MAKE UP": ["LANCOME", "ARMANI", "YVES ST LAURENT"]
        }
        self.cannot_add = {
            "SKINCARE": ["PLACEHOLDER"]
        }

        self.large_brands = {
            "SKINCARE": ["LANCOME", "LAUDER", "SKII", "GUERLAIN", "DIOR", "SHISEIDO"],
            "MAKE UP": ["LANCOME", "LAUDER", "DIOR"]
        }
    
    def filter_sales(self, df):
        df_sales_filter = df[df["store_code"].isin(self.stores)]
        df_agg_pivot = df_sales_filter.pivot_table(
            index=self.key_cols, 
            values=["avg_weekly_sales"], 
            columns=["store_code"]
        ).reset_index()
        store_lst = list(df_sales_filter["store_code"].unique())
        store_lst.sort()
        df_agg_pivot.columns = self.key_cols + ["avg_weekly_sales_{}".format(store) for store in store_lst]
        return df_agg_pivot, df_sales_filter
    
    def decide_add_within_cluster(self, brand, category, other_store_good_brands_dict):
        print("|| ---- decide within cluster")
        whether_add = "NOT ADD"
        if category in self.core_categories:
            all_brands = other_store_good_brands_dict[category]
            
            if brand in all_brands.keys():
                whether_add =  "ADD"
        return whether_add

    def get_other_store_good_brands(self, store, df_agg, q):
        df_shelf = self.df_shelf.copy()
        other_store_good_brands_dict = {}
        left_cols = [col for col in df_agg.columns if str(store) not in col]
        for category in self.core_categories:
            df_left = df_agg[left_cols]
            df_sel = df_left[df_left["category"]==category]
            all_brands = {}
            for other_store in self.stores:
                sales_col = "avg_weekly_sales_{}".format(other_store)
                if other_store != store:
                    # remove large all-store brands when calculating cutoff
                    large_brands = self.large_brands.get(category, ["_DEFAULT"])
                    brands_with_shelf = df_shelf[
                        (df_shelf["store_code"]==other_store) &
                        (df_shelf["category"]==category)
                    ]["brand"].unique()

                    df_sel_new = df_sel[
                        ~(df_sel["brand"].isin(large_brands)) & 
                        (df_sel["brand"].isin(brands_with_shelf))  # with shelf
                    ]
                    cutoff = df_sel_new[sales_col].quantile(q)
                    brands = list(df_sel[df_sel[sales_col]>cutoff]["brand"].unique())
                    for b in brands:
                        all_brands[b] = all_brands.get(b, 0) + 1
            
            other_store_good_brands_dict.update({category: all_brands})
        
        return other_store_good_brands_dict

    def add_item(self, df_cluster_brand_rank, q=0.5):
        df_agg, df_sales_filter = self.filter_sales(self.df_sales)
        df_out = df_agg[self.key_cols]

        for store in self.stores:
            print("| -- decide whether to add items for {}".format(store))
            other_store_good_brands_dict = self.get_other_store_good_brands(store, df_agg, q)

            sales_col = "avg_weekly_sales_{}".format(store)
            df_sel = df_agg[self.key_cols + [sales_col]]
            assort_col = "assortment_{}".format(store)
            df_sel.loc[:, assort_col] = np.array(["KEEP"] * len(df_sel))
            not_sell_items = df_sel[pd.isnull(df_sel[sales_col])][self.key_cols].values
            
            for item in not_sell_items:
                brand = item[0]
                category = item[1]

                if category in self.core_categories:
                    brand_type = self.brand_type_dict[category].get(brand, "_others")
                    sel_df = df_cluster_brand_rank[
                        (df_cluster_brand_rank["category"]==category) &
                        (df_cluster_brand_rank["brand_type"]==brand_type) &
                        (df_cluster_brand_rank["cluster"]==self.cluster)
                    ]

                    if df_cluster_brand_rank["cluster"].nunique() > 1:
                        if len(sel_df) > 0:
                            brand_profile_cluster_rank = sel_df["brand_type_share_rank"].values[0]
                        else:
                            brand_profile_cluster_rank = 9999

                        if brand_type != "_others":
                            threshold = self.brand_profile_threshold[category].get(brand_type, 3)
                        else:
                            threshold = 0
                    else:
                        brand_profile_cluster_rank = 9999
                        threshold = 0

                    # firstly add based on brand profile, otherwise look at opportunities in the cluster
                    if brand_profile_cluster_rank <= threshold:
                        whether_add = "ADD"
                    else:
                        whether_add = self.decide_add_within_cluster(brand, category, other_store_good_brands_dict)
                else:
                    whether_add = "NOT ADD"
                df_sel.loc[(df_sel["brand"]==brand) & (df_sel["category"]==category), assort_col] = whether_add

            df_out = df_out.merge(
                df_agg[self.key_cols + [sales_col]], on=self.key_cols
            ).merge(df_sel[self.key_cols + [assort_col]], on=self.key_cols)

        return df_out

    def decide_remove(self, df_sales, sales_col, add_items_dict, df_shelf):
        df = df_sales[df_sales["category"].isin(self.core_categories)]
        df = df.merge(df_shelf, on=self.key_cols)

        total_add = 0
        for k,v in add_items_dict.items():
            total_add = total_add + len(v)

        df_sort = df.sort_values(by=[sales_col])
        df_remove = df_sort.head(total_add)[self.key_cols]
        df_remove["remove_ind"] = 1

        return df_remove

    def remove_item(self, df_add, new_stores):
        df_out = df_add[self.key_cols]
        all_cat = df_out["category"].unique()

        for store in self.stores:
            df_shelf_store = self.df_shelf[self.df_shelf["store_code"]==store][self.key_cols + ["shelf"]]
            print("| -- decide whether to remove items for {}".format(store))
            sales_col = "avg_weekly_sales_{}".format(store)
            assort_col = "assortment_{}".format(store)
            sel_cols = self.key_cols + [sales_col, assort_col]
            df_sel = df_add[sel_cols]

            if store not in new_stores:
                add_items_dict = {}
                df_tmp = df_sel[df_sel[assort_col]=="ADD"]
                for cat in df_tmp["category"].unique():
                    add_items_dict.update({cat: list(df_tmp[df_tmp["category"]==cat]["brand"].unique())})

                df_remove = self.decide_remove(df_sel, sales_col, add_items_dict, df_shelf_store)
                df_sel = df_sel.merge(df_remove, on=self.key_cols, how="left")
                df_sel[assort_col] = np.where(
                    df_sel["remove_ind"] == 1,
                    "SUGGEST REMOVE",
                    df_sel[assort_col]
                )

            shelf_col = "shelf_{}".format(store)
            df_out = df_out.merge(
                df_sel[sel_cols], on=self.key_cols, how="left"
            ).merge(
                df_shelf_store[self.key_cols + ["shelf"]].rename(columns={"shelf": shelf_col}),
                on=self.key_cols, how="left"
            )

            # final adjust
            df_out[assort_col] = np.where(
                ~pd.isnull(df_out[sales_col]) & pd.isnull(df_out[shelf_col]) & (df_out["category"].isin(self.core_categories)),
                "NO SHELF",
                df_out[assort_col]
            )

        return df_out

    def map_sku_assortment(self, dim_product, df_assort, df_sales_agg, df_sku_agg):

        by_cat = self.key_cols
        by_range = by_cat + ["Range"]
        by_seg = by_range + ["Segment"]

        df_agg1 = df_sales_agg.groupby(
            by_range, as_index=False
        )[["avg_weekly_sales"]].mean()

        df_agg1 = df_agg1.groupby(by_cat, as_index=False).apply(_get_rank, "range_rank").drop("avg_weekly_sales", axis=1)

        df_agg2 = df_sales_agg.groupby(
            by_seg, as_index=False
        )[["avg_weekly_sales"]].mean()

        df_agg2 = df_agg2.groupby(by_range, as_index=False).apply(_get_rank, "segment_rank").drop("avg_weekly_sales", axis=1)

        df_rank = df_agg2.merge(df_agg1, on=by_range, how="left")
        #df_rank_sku = df_rank.merge(dim_product[by_seg + ["sku_code"]], on=by_seg, how="left")

        df_sku_agg_all = df_sku_agg.groupby(by_seg + ["sku_code"], as_index=False)[["avg_weekly_sales"]].mean()
        df_sku_agg_all = df_sku_agg_all.groupby(
            by_seg, as_index=False).apply(_get_rank, "sku_rank").drop("avg_weekly_sales", axis=1)

        df_rank_sku = df_rank.merge(df_sku_agg_all[by_seg + ["sku_code", "sku_rank"]], on=by_seg, how="left")

        all_sku = []
        for store_code in self.stores:
            print("| -- map range, seg, sku for {}".format(store_code))
            col = "assortment_" + str(store_code)
            sales_col = "avg_weekly_sales_" + str(store_code)
            df_tmp = df_assort[by_cat + [col, sales_col]]
            df_tmp_sku = df_tmp.merge(df_rank_sku, on=by_cat, how="left").rename(columns={
                col: "assortment", sales_col: "avg_weekly_sales"})
            out_cols = list(df_tmp_sku.columns.values).copy()
            df_tmp_sku["store_code"] = store_code

            df_sku_agg_tmp = df_sku_agg[df_sku_agg["store_code"]==store_code][["sku_code"]].drop_duplicates()
            df_sku_agg_tmp["ind_existing_sku"] = 1

            df_tmp_sku = df_tmp_sku.merge(df_sku_agg_tmp, on=["sku_code"], how="left")
            df_tmp_sku["ind_existing_sku"].fillna(0, inplace=True)
        
            all_sku.append(df_tmp_sku[["store_code"] + out_cols + ["ind_existing_sku"]])
        
        df_all_sku = pd.concat(all_sku)
        return df_all_sku
    
    def calc_share(self, df):
        share_keycols = self.key_cols
        df["share_x_sales"] = df["share"] * df["total_avg_weekly_sales"]
        df_agg = df.groupby(share_keycols, as_index=False).agg({
            "share": ["mean", "max"],
            "share_x_sales": "sum",
            "total_avg_weekly_sales": "sum",
        })

        df_agg.columns = share_keycols + ["other_store_avg_share", "other_store_max_share", "share_x_sales", "total_avg_weekly_sales"]
        df_agg["other_store_weighted_avg_share"] = df_agg["share_x_sales"] / df_agg["total_avg_weekly_sales"]
        share_outcols = ["other_store_avg_share", "other_store_max_share", "other_store_weighted_avg_share"]
        
        return df_agg[share_keycols + share_outcols]

    def attach_other_store_share(self, df_assort_sku, df_agg_cat_share):
        global_share = self.calc_share(df_agg_cat_share)
        share_outcols = ["other_store_avg_share", "other_store_max_share", "other_store_weighted_avg_share"]
        rename_map = {}
        for col in share_outcols:
            rename_map[col] = col.replace("other_store_", "global_")
        global_share = global_share.rename(columns=rename_map)

        df_share = df_agg_cat_share[df_agg_cat_share["store_code"].isin(self.stores)]

        list_df = []
        for store in self.stores:
            df_filter = df_assort_sku[df_assort_sku["store_code"]==store]
            df_share_filter = df_share[df_share["store_code"]!=store]
            share_keycols = self.key_cols

            df_share_filter_agg = self.calc_share(df_share_filter)

            df_out = df_filter.merge(df_share_filter_agg, on=share_keycols, how="left")
            list_df.append(df_out)
        
        df_all = pd.concat(list_df)
        df_all_cols = df_all.columns.values.copy()

        df_all_fill = df_all.merge(global_share, on=self.key_cols, how="left")
        for c in share_outcols:
            new_c = c.replace("other_store_", "global_")
            df_all_fill[c] = df_all_fill[c].combine_first(df_all_fill[new_c])

        return df_all[df_all_cols]

def _get_rank(df, rank_col, value_col="avg_weekly_sales"):
    df = df.sort_values(by=[value_col], ascending=False).reset_index(drop=True)
    df[rank_col] = df.index + 1
    return df

def _sales_agg(df, grp_keys):
    df_agg1 = df.groupby(grp_keys)[["weekstartdate"]].nunique().reset_index()
    df_agg2 = df.groupby(grp_keys, as_index=False)[["sales"]].sum()

    df_agg = df_agg1.merge(df_agg2, on=grp_keys, how="left")
    df_agg["avg_weekly_sales"] = df_agg["sales"] / df_agg["weekstartdate"]
    return df_agg 

def load_agg_sales():
    out_dir = config.adl_dir
    df_agg_cat = pd.read_csv(os.path.join(out_dir, "df_agg_cat.csv"))
    df_agg_cat_share = pd.read_csv(os.path.join(out_dir, "df_agg_cat_share.csv"))
    df_agg_cat_range_seg = pd.read_csv(os.path.join(out_dir, "df_agg_cat_range_seg.csv"))
    df_sku_agg = pd.read_csv(os.path.join(out_dir, "df_sku_agg.csv"))
    df_agg_cat_all = pd.read_csv(os.path.join(out_dir, "df_agg_cat_all.csv"))

    return df_agg_cat, df_agg_cat_share, df_agg_cat_range_seg, df_sku_agg, df_agg_cat_all

def save_agg_sales(df_agg_cat, df_agg_cat_share, df_agg_cat_range_seg, df_sku_agg, df_agg_cat_all):
    out_dir = config.adl_dir
    df_agg_cat.to_csv(os.path.join(out_dir, "df_agg_cat.csv"), index=False)
    df_agg_cat_share.to_csv(os.path.join(out_dir, "df_agg_cat_share.csv"), index=False)
    df_agg_cat_range_seg.to_csv(os.path.join(out_dir, "df_agg_cat_range_seg.csv"), index=False)
    df_sku_agg.to_csv(os.path.join(out_dir, "df_sku_agg.csv"), index=False)
    df_agg_cat_all.to_csv(os.path.join(out_dir, "df_agg_cat_all.csv"), index=False)


def run_agg_sales(df_product, df_store, df_shelf_agg, rerun=False):
    if not rerun:
        df_agg_cat, df_agg_cat_share, df_agg_cat_range_seg, df_sku_agg, df_agg_cat_all = load_agg_sales()
        print("| -- weekly sales agg loaded")
    else:
        for col in ["Range", "Segment"]:
            df_product[col].fillna("_NaN", inplace=True)
        df = pd.read_parquet(os.path.join(config.sdl_dir, "txn_store_sku_weekly_v2.parquet"))
        df["weekstartdate"] = pd.to_datetime(df["weekstartdate"], format="%Y-%m-%d")
        df["year"] = df["weekstartdate"].dt.year
        df["weekstartdate"] = df["weekstartdate"].dt.strftime("%Y-%m-%d")

        df_merge = df.merge(df_product[["product_id", "brand", "category", "Range", "Segment", "sku_code"]], on=["product_id"]).merge(
            df_store[["store_id", "store_code"]], on=["store_id"])

        exclude_weeks = ["2020-01-27", "2020-02-03", "2020-02-10", "2020-02-17", "2020-02-24", "2020-03-02"]

        df_prep = df_merge[
            (df_merge["year"].isin([2019, 2020])) & 
            ~(df_merge["weekstartdate"].isin(exclude_weeks))
        ]
        df_prep = df_prep[df_prep["sales"]>1]

        grp_keys1 = ["store_code", "brand", "category"]
        df_agg_cat_all = _sales_agg(df_prep, grp_keys1)

        # filter no shelf
        df_filter = df_prep.merge(df_shelf_agg, on=["store_code", "brand", "category"])
        df_agg_cat = _sales_agg(df_filter, grp_keys1)

        df_agg_cat_sum = df_agg_cat.groupby(["store_code"], as_index=False)[["avg_weekly_sales"]].sum().rename(
            columns={"avg_weekly_sales": "total_avg_weekly_sales"})
        df_agg_cat_share = df_agg_cat.merge(df_agg_cat_sum, on=["store_code"], how="left")
        df_agg_cat_share["share"] = df_agg_cat_share["avg_weekly_sales"] / df_agg_cat_share["total_avg_weekly_sales"]

        grp_keys2 = ["store_code", "brand", "category", "Range", "Segment"]
        df_agg_cat_range_seg = _sales_agg(df_filter, grp_keys2)

        by_sku = ["store_code", "brand", "category", "Range", "Segment", "sku_code"]
        df_sku_agg = _sales_agg(df_filter, by_sku)

        save_agg_sales(df_agg_cat, df_agg_cat_share, df_agg_cat_range_seg, df_sku_agg, df_agg_cat_all)
        print("| -- weekly sales aggregated")
    return df_agg_cat, df_agg_cat_share, df_agg_cat_range_seg, df_sku_agg, df_agg_cat_all

def cluster_brand_profile(df, df_cluster, df_brand_type):
    df_merge = df.merge(df_cluster, on=["store_code"]).merge(df_brand_type, on=["brand", "category"])

    by_cat = ["cluster", "store_code", "category"]
    df_agg1 = df_merge.groupby(by_cat, as_index=False)[["avg_weekly_sales"]].sum().rename(
        columns={"avg_weekly_sales": "avg_weekly_sales_by_cat"}
    )

    df_agg2 = df_merge.groupby(by_cat + ["brand_type"], as_index=False)[["avg_weekly_sales"]].sum()

    df_rank = df_agg2.merge(df_agg1, on=by_cat)
    df_rank["brand_type_share"] = df_rank["avg_weekly_sales"] / df_rank["avg_weekly_sales_by_cat"]

    df_rank_agg = df_rank.groupby(["cluster", "category", "brand_type"], as_index=False)[["brand_type_share"]].mean()

    df_rank_agg = df_rank_agg.groupby(["category", "brand_type"], as_index=False).apply(
        _get_rank, "brand_type_share_rank", "brand_type_share")

    df_rank_agg.to_csv(os.path.join(config.model_dir, "cluster", "cluster_brand_type_share_rank_weekly.csv"), index=False)
    return df_rank_agg

def get_shelf():
    df_raw = pd.read_csv(os.path.join(config.raw_processed, "space_shelf_meter.csv")).rename(columns={
        "category": "category_from_space"
    })

    min_weekstartdate = df_raw["weekstartdate"].min()
    df_mapping = pd.read_csv(os.path.join(config.ref_dir, "shelf_pog_brand_category_mapping.csv"))

    df_shelf = df_raw.merge(
        df_mapping,
        on=["brand", "pog_name"],
        how="left"
    )

    df_shelf["brand"] = df_shelf["brand"].str.upper()

    # add SC shelf for MU brands (on the same POG)
    add_sc = []
    for brand in ["SHU", "BOBBI BROWN", "MAC", "MAKE UP FOR EVER"]:
        df_shelf_brand = df_shelf[df_shelf["brand"]==brand]
        df_shelf_brand["category"] = "SKINCARE"
        df_shelf_brand["shelf"] = 1
        add_sc.append(df_shelf_brand)

    # add CHALING FR
    df_chaling = df_shelf[df_shelf["brand"]=="CHALING"]
    df_chaling["category"] = "FRAGRANCE"
    df_chaling["shelf"] = 1
    
    df_shelf = pd.concat([df_shelf, df_chaling] + add_sc)
    
    df_shelf = df_shelf[
        (df_shelf["weekstartdate"]==min_weekstartdate) & 
        (df_shelf["category"].isin(["SKINCARE", "MAKE UP", "FRAGRANCE"]))
    ]
    df_shelf_agg = df_shelf.groupby(["store_code", "brand", "category"], as_index=False)[["shelf"]].sum()
    return df_shelf_agg

def read_brand_type():
    df_brand_type = pd.read_csv(os.path.join(config.ref_dir, "dim_brand_type_revised.csv"))
    brand_type_dict = {"SKINCARE": {}, "MAKE UP": {}, "FRAGRANCE": {}}
    for i in df_brand_type.values:
        brand_type_dict[i[1]].update({i[0]: i[2]})

    return df_brand_type, brand_type_dict

def run_assortment(cluster_file, rerun_sales_agg):
    print("| -- load config for assortment")
    conf = config.assortment_config

    print("| -- read input data")
    df_store = pd.read_excel(os.path.join(config.ref_dir, "dim_stores_offline_v5.xlsx"), engine="openpyxl")

    new_stores = df_store[df_store["open_date"] >= "2020-10-01"]["store_code"].unique()
    print("new stores: ", new_stores)

    df_product = pd.read_excel(os.path.join(config.ref_dir, "dim_product_with_txn.xlsx"), engine="openpyxl")
    df_brand_type, brand_type_dict = read_brand_type()
    df_shelf_agg = get_shelf()
    df_store_cluster = pd.read_csv(os.path.join(config.model_dir, "cluster", cluster_file))

    print("| -- calculate avg weekly sales")
    df_agg_cat, df_agg_cat_share, df_agg_cat_range_seg, df_sku_agg, df_agg_cat_all = run_agg_sales(
        df_product, df_store, df_shelf_agg, rerun=rerun_sales_agg)
    df_cluster_brand_rank = cluster_brand_profile(df_agg_cat_share, df_store_cluster, df_brand_type)
    
    base_dir = os.path.join(config.model_dir, "assortment")
    ts = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    out_dir = os.path.join(base_dir, ts + "_weekly")
    os.mkdir(out_dir)

    for cluster in df_store_cluster["cluster"].unique():
        print("| -- running assortment for {} cluster".format(cluster))
        stores = list(df_store_cluster[df_store_cluster["cluster"]==cluster]["store_code"].unique())
        cluster_assort = StoreAssortment(conf, cluster, stores, brand_type_dict, df_agg_cat_all, df_shelf_agg)
        df_assort_add = cluster_assort.add_item(df_cluster_brand_rank)

        df_assort_add_remove = cluster_assort.remove_item(df_assort_add, new_stores)
        df_assort_add_remove.to_csv(os.path.join(out_dir, "{}_assortment_add_remove.csv".format(cluster)), index=False)

        df_sku_assort = cluster_assort.map_sku_assortment(df_product, df_assort_add_remove, df_agg_cat_range_seg, df_sku_agg)
        df_sku_assort_out = cluster_assort.attach_other_store_share(df_sku_assort, df_agg_cat_share)
        df_sku_assort_out.to_csv(
            os.path.join(out_dir, "{}_assortment_sku.csv".format(cluster)), 
            index=False
        )

if __name__ == "__main__":
    if sys.argv[2] == "rerun_agg":
        rerun_sales_agg = True
    else:
        rerun_sales_agg = False
    run_assortment(sys.argv[1], rerun_sales_agg)