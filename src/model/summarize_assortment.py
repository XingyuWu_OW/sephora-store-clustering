import os
import sys
import datetime
import numpy as np
import pandas as pd

import warnings
warnings.filterwarnings("ignore")

from src import config

def summarize_assortment(dir_name):
    level = dir_name.split("_")[1]

    sel_cols = ["store_code", "brand", "category", "assortment", "shelf", "add_logic_flag", "avg_{}_sales".format(level), 
        "other_store_avg_share", "other_store_weighted_avg_share"]

    src_dir = os.path.join(config.model_dir, "assortment", dir_name)

    df_list = []
    for f in os.listdir(src_dir):
        if f.endswith("_sku.csv"):
            df = pd.read_csv(os.path.join(src_dir, f))
            if "add_logic_flag" not in df.columns and "add_logic_flag" in sel_cols:
                sel_cols.remove("add_logic_flag")
            df = df[sel_cols]
            df_dedup = df.drop_duplicates()
            df_list.append(df_dedup)
    
    df_out = pd.concat(df_list)

    df_out.to_csv(os.path.join(config.model_dir, "assortment", "summary", "summary_{}.csv".format(dir_name)), index=False)
    return df_out   

def compare_assortment(dir_name1, dir_name2):

    df1 = summarize_assortment(dir_name1)
    df2 = summarize_assortment(dir_name2)

    df_compare = df1.merge(df2, on=["store_code", "brand", "category"])
    df_compare.to_csv(os.path.join(config.model_dir, "assortment", "summary", "compare_{}_{}.csv".format(dir_name1, dir_name2)), index=False)

if __name__ == "__main__":
    if len(sys.argv) == 2:
        summarize_assortment(sys.argv[1])
    else:
        compare_assortment(sys.argv[1], sys.argv[2])