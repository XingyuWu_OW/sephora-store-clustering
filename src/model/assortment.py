import os
import sys
import datetime
import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler

from src import config
from src.utils.helper_assortment import *

# class Assortment:
#     def __init__

# def get_cluster_stores(df, cluster):
#     stores = list(df[df["cluster"]==cluster]["store_id"].unique())
#     print("| -- read stores for cluster {}".format(cluster))
#     return stores

def get_cluster_cm(df_store_cm, stores, cluster):
    df = df_store_cm[df_store_cm["store_id"].isin(stores)]
    if cluster != "lux" and len(stores) > 15:
        df_agg_store = df.groupby(["store_id"], as_index=False)[["sales"]].sum().sort_values(by=["sales"], ascending=False)
        sel_stores = df_agg_store.head(15)["store_id"].unique()
        df = df[df["store_id"].isin(sel_stores)]

    df_agg = df.groupby(["customer_id"], as_index=False)[["qty", "sales", "txn_id"]].sum()
    print("| ---- check 1", df_agg.shape)

    # filter customers with higher sales
    sales_cutoff = df_agg["sales"].quantile(.75)
    df_agg = df_agg[df_agg["sales"] > sales_cutoff]
    #df_agg = df_agg.sample(frac=0.3)
    print("| ---- check 2", df_agg.shape)

    scaler = MinMaxScaler(feature_range=(1,2))
    df_agg["sales_weight"] = scaler.fit_transform(df_agg[["sales"]].values)
    print("| -- get customers for stores in cluster {}".format(cluster))
    return df_agg

def gen_assortment(df_cm, cluster, df_product, svd_score_file):
    df_reader = pd.read_csv(
        "/data/temp/xingyu/svd/{}".format(svd_score_file), 
        header=None,
        names=["customer_id", "product_key", "score"],
        chunksize=5000000)

    all_out = []
    for chunk in df_reader:
        out = chunk.merge(df_cm, on=["customer_id"])
        if len(out) > 0:
            #print("| -- cm filtered for one chunk")
            all_out.append(out)
    
    df_cm = pd.concat(all_out)
    print("| -- get propensity scores for customers in stores in cluster {}".format(cluster), df_cm.shape)
    df_cm["weighted_score"] = df_cm["score"] * df_cm["sales_weight"]
    df_agg = df_cm.groupby(["product_key"], as_index=False).agg({
        "weighted_score": ["sum", "mean", "median"],
        "sales_weight": "sum"
    })
    df_agg.columns = ["product_key", "sum_weighted_score", "mean_weighted_score", "median_weighted_score", "sum_weights"]
    df_agg["weighted_avg_score"] = df_agg["sum_weighted_score"] / df_agg["sum_weights"]

    df_agg = df_agg.merge(df_product, on=["product_key"])
    print("| -- gen assortment for cluster {}".format(cluster))
    return df_agg

def run_assortment(cluster, year, svd_score_file):
    year = str(year)
    df_cluster = pd.read_csv(os.path.join(config.model_dir, "store_cluster_v1.csv"))
    stores = get_cluster_stores(df_cluster, cluster)

    df_store_cm = pd.read_parquet(os.path.join(config.sdl_dir, "store_cm_{}.parquet".format(year)))
    df_cm = get_cluster_cm(df_store_cm, stores, cluster)

    df_product = pd.read_csv("/data/temp/xingyu/svd/product_list_v2.csv")
    df_assort = gen_assortment(df_cm, cluster, df_product, svd_score_file)

    ts = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    df_assort.to_csv(os.path.join(config.model_dir, "cluster_{}_assortment_{}.csv".format(cluster, ts)), index=False)
    return df_assort

if __name__ == "__main__":
    run_assortment(sys.argv[1], sys.argv[2], sys.argv[3])



