import os
import sys
import datetime
import numpy as np
import pandas as pd

import warnings
warnings.filterwarnings("ignore")

from src import config

class StoreAssortment:
    def __init__(self, assort_config, cluster, stores, brand_type_dict, df_sales, df_space=None):
        self.config = assort_config
        self.brand_profile_threshold = self.config["brand_profile_threshold"]
        self.remove_threshold = self.config["remove_threshold"]
        self.cluster = cluster
        self.stores = stores
        self.df_sales = df_sales
        self.df_space = df_space
        self.key_cols = ["brand", "category"]
        self.core_categories = ["SKINCARE", "MAKE UP", "FRAGRANCE"]
        self.brand_type_dict = brand_type_dict
        self.must_keep = {
            "SKINCARE": ["LAUDER"],
            "FRAGRANCE": ["DIOR"],
            "MAKE UP": ["LANCOME", "ARMANI", "YVES ST LAURENT"]
        }
        self.cannot_add = {
            "SKINCARE": ["PLACEHOLDER"]
        }

        self.large_brands = {
            "SKINCARE": ["LANCOME", "LAUDER", "SKII", "GUERLAIN", "DIOR", "SHISEIDO"]
        }
    
    def filter_sales(self):
        df_sales_filter = self.df_sales[self.df_sales["store_code"].isin(self.stores)]
        df_agg_pivot = df_sales_filter.pivot_table(
            index=self.key_cols, 
            values=["monthly_avg_sales"], 
            columns=["store_code"]
        ).reset_index()
        store_lst = list(df_sales_filter["store_code"].unique())
        store_lst.sort()
        df_agg_pivot.columns = self.key_cols + ["monthly_avg_sales_{}".format(store) for store in store_lst]
        return df_agg_pivot, df_sales_filter
    
    def decide_add_within_cluster(self, brand, category, other_store_good_brands_dict):
        print("|| ---- decide within cluster")
        whether_add = "NOT ADD"
        if category in self.core_categories:
            all_brands = other_store_good_brands_dict[category]
            
            if brand in all_brands.keys():
                whether_add =  "ADD"
        return whether_add

    def get_other_store_good_brands(self, store, df_agg, q):
        other_store_good_brands_dict = {}
        left_cols = [col for col in df_agg.columns if str(store) not in col]
        for category in self.core_categories:
            df_left = df_agg[left_cols]
            df_sel = df_left[df_left["category"]==category]
            all_brands = {}
            for other_store in self.stores:
                sales_col = "monthly_avg_sales_{}".format(other_store)
                if other_store != store:
                    # remove large all-store brands when calculating cutoff
                    large_brands = self.large_brands.get(category, ["_DEFAULT"])
                    df_sel_new = df_sel[~df_sel["brand"].isin(large_brands)]
                    cutoff = df_sel_new[sales_col].quantile(q)
                    brands = list(df_sel[df_sel[sales_col]>cutoff]["brand"].unique())
                    for b in brands:
                        all_brands[b] = all_brands.get(b, 0) + 1
            
            other_store_good_brands_dict.update({category: all_brands})
        
        return other_store_good_brands_dict

    def add_item(self, df_cluster_brand_rank, q=0.5):
        df_agg, df_sales_filter = self.filter_sales()
        df_out = df_agg[self.key_cols]

        for store in self.stores:
            print("| -- decide whether to add items for {}".format(store))
            other_store_good_brands_dict = self.get_other_store_good_brands(store, df_agg, q)

            sales_col = "monthly_avg_sales_{}".format(store)
            df_sel = df_agg[self.key_cols + [sales_col]]
            assort_col = "assortment_{}".format(store)
            df_sel.loc[:, assort_col] = np.array(["KEEP"] * len(df_sel))
            not_sell_items = df_sel[pd.isnull(df_sel[sales_col])][self.key_cols].values
            
            for item in not_sell_items:
                brand = item[0]
                category = item[1]

                if category in self.core_categories:
                    brand_type = self.brand_type_dict[category].get(brand, "_others")
                    sel_df = df_cluster_brand_rank[
                        (df_cluster_brand_rank["category"]==category) &
                        (df_cluster_brand_rank["brand_type"]==brand_type) &
                        (df_cluster_brand_rank["cluster"]==self.cluster)
                    ]

                    if df_cluster_brand_rank["cluster"].nunique() > 1:
                        if len(sel_df) > 0:
                            brand_profile_cluster_rank = sel_df["brand_type_share_rank"].values[0]
                        else:
                            brand_profile_cluster_rank = 9999

                        if brand_type != "_others":
                            threshold = self.brand_profile_threshold[category].get(brand_type, 3)
                        else:
                            threshold = 0
                    else:
                        brand_profile_cluster_rank = 9999
                        threshold = 0

                    # firstly add based on brand profile, otherwise look at opportunities in the cluster
                    if brand_profile_cluster_rank <= threshold:
                        whether_add = "ADD"
                    else:
                        whether_add = self.decide_add_within_cluster(brand, category, other_store_good_brands_dict)
                else:
                    whether_add = "NOT ADD"
                df_sel.loc[(df_sel["brand"]==brand) & (df_sel["category"]==category), assort_col] = whether_add

            df_out = df_out.merge(
                df_agg[self.key_cols + [sales_col]], on=self.key_cols
            ).merge(df_sel[self.key_cols + [assort_col]], on=self.key_cols)

        return df_out

    def decide_remove(self, store, df_sales, sales_col, add_items_dict):
        df_space_agg = self.df_space.groupby(["store_code", "brand", "category"], as_index=False)[["shelf"]].sum()
        remove_items_dict = {}
        adjust_items_dict = {}
        for k,v in add_items_dict.items():
            num_brands = self.remove_threshold.get(k, 0)
            df_sel = df_sales[df_sales["category"]==k].sort_values(by=[sales_col])
            remove_brands = [i for i in df_sel.head(num_brands)["brand"].unique() if i not in self.must_keep[k] and i not in v]
            remove_items_dict.update({k: remove_brands})

            # if k == "FRAGRANCE":
            #     df_sel = df_sales[df_sales["category"]==k].sort_values(by=[sales_col])
            #     remove_brands = [i for i in df_sel.head(num_brands)["brand"].unique() if i not in self.must_keep[k]]
            #     remove_items_dict.update({k: remove_brands})
            # else:
            #     other_stores = [s for s in self.stores if s != store]
            #     df_space_agg_sel = df_space_agg[df_space_agg["store_code"]==store][["brand", "category", "shelf"]]
            
            #     df_merge = df_sales.merge(df_space_agg_sel, on=["brand", "category"], how="left")
            #     df_cat = df_merge[(df_merge["category"]==k) & ~pd.isnull(df_merge[sales_col])].sort_values(by=[sales_col])
            #     df_cat = df_cat[~pd.isnull(df_cat["shelf"])].reset_index(drop=True)
            #     df_cat["shelf_cumsum"] = df_cat.groupby(["category"])["shelf"].cumsum()

            #     df_space_sel = self.df_space[
            #         (self.df_space["store_code"].isin(other_stores)) & 
            #         (self.df_space["category"]==k) & 
            #         (self.df_space["brand"].isin(v))
            #     ].groupby(["category", "brand"], as_index=False)[["shelf"]].min()

            #     space_needed = df_space_sel["shelf"].sum()

            #     min_index = df_cat[df_cat["shelf_cumsum"]>=space_needed].index.min()
            #     if (df_cat.loc[min_index, "shelf_cumsum"] - space_needed) / space_needed > 0.5:
            #         min_index = min_index - 1

            #     remove_brands = list(df_cat[df_cat.index<=min_index]["brand"].unique())
            #     remove_brands = [i for i in remove_brands if i not in self.must_keep[k]]
                
            #     remove_items_dict.update({k: remove_brands})
        
        return remove_items_dict

    def remove_item(self, df_add):
        df_out = df_add[self.key_cols]

        for store in self.stores:
            print("| -- decide whether to remove items for {}".format(store))
            sales_col = "monthly_avg_sales_{}".format(store)
            assort_col = "assortment_{}".format(store)
            df_sel = df_add[self.key_cols + [sales_col, assort_col]]

            add_items_dict = {}
            df_tmp = df_sel[df_sel[assort_col]=="ADD"]
            for cat in df_tmp["category"].unique():
                add_items_dict.update({cat: list(df_tmp[df_tmp["category"]==cat]["brand"].unique())})

            remove_items_dict = self.decide_remove(store, df_sel, sales_col, add_items_dict)
            for k,v in remove_items_dict.items():
                df_sel.loc[(df_sel["brand"].isin(v)) & (df_sel["category"]==k), assort_col] = "REMOVE"

            df_out = df_out.merge(
                df_sel[self.key_cols + [sales_col, assort_col]], on=self.key_cols
            )

        return df_out

    def map_sku_assortment(self, dim_product, df_assort, df_sales_agg, df_sku_agg):

        by_cat = self.key_cols
        by_range = by_cat + ["Range"]
        by_seg = by_range + ["Segment"]

        df_agg1 = df_sales_agg.groupby(
            by_range, as_index=False
        )[["monthly_avg_sales"]].mean()

        df_agg1 = df_agg1.groupby(by_cat, as_index=False).apply(_get_rank, "range_rank").drop("monthly_avg_sales", axis=1)

        df_agg2 = df_sales_agg.groupby(
            by_seg, as_index=False
        )[["monthly_avg_sales"]].mean()

        df_agg2 = df_agg2.groupby(by_range, as_index=False).apply(_get_rank, "segment_rank").drop("monthly_avg_sales", axis=1)

        df_rank = df_agg2.merge(df_agg1, on=by_range, how="left")
        #df_rank_sku = df_rank.merge(dim_product[by_seg + ["sku_code"]], on=by_seg, how="left")

        df_sku_agg_all = df_sku_agg.groupby(by_seg + ["sku_code"], as_index=False)[["monthly_avg_sales"]].mean()
        df_sku_agg_all = df_sku_agg_all.groupby(
            by_seg, as_index=False).apply(_get_rank, "sku_rank").drop("monthly_avg_sales", axis=1)

        df_rank_sku = df_rank.merge(df_sku_agg_all[by_seg + ["sku_code", "sku_rank"]], on=by_seg, how="left")

        all_sku = []
        for store_code in self.stores:
            print("| -- map range, seg, sku for {}".format(store_code))
            col = "assortment_" + str(store_code)
            sales_col = "monthly_avg_sales_" + str(store_code)
            df_tmp = df_assort[by_cat + [col, sales_col]]
            df_tmp_sku = df_tmp.merge(df_rank_sku, on=by_cat, how="left").rename(columns={
                col: "assortment", sales_col: "monthly_avg_sales"})
            out_cols = list(df_tmp_sku.columns.values).copy()
            df_tmp_sku["store_code"] = store_code

            df_sku_agg_tmp = df_sku_agg[df_sku_agg["store_code"]==store_code][["sku_code"]].drop_duplicates()
            df_sku_agg_tmp["ind_existing_sku"] = 1

            df_tmp_sku = df_tmp_sku.merge(df_sku_agg_tmp, on=["sku_code"], how="left")
            df_tmp_sku["ind_existing_sku"].fillna(0, inplace=True)
        
            all_sku.append(df_tmp_sku[["store_code"] + out_cols + ["ind_existing_sku"]])
        
        df_all_sku = pd.concat(all_sku)
        return df_all_sku
    
    def attach_other_store_share(self, df_assort_sku, df_agg_cat_share):
        df_share = df_agg_cat_share[df_agg_cat_share["store_code"].isin(self.stores)]
        df_share["share_x_sales"] = df_share["share"] * df_share["sales"]

        list_df = []
        for store in self.stores:
            df_filter = df_assort_sku[df_assort_sku["store_code"]==store]
            df_share_filter = df_share[df_share["store_code"]!=store]
            share_keycols = self.key_cols
            df_share_filter_agg = df_share_filter.groupby(share_keycols, as_index=False).agg({
                "share": ["mean", "max"],
                "sales": "sum",
                "total_sales": "sum",
            })

            df_share_filter_agg.columns = share_keycols + [
                "other_store_avg_share", "other_store_max_share", "other_store_sales", "other_store_total_sales"]
            df_share_filter_agg["other_store_weighted_avg_share"] = df_share_filter_agg["other_store_sales"] / df_share_filter_agg["other_store_total_sales"]

            df_out = df_filter.merge(df_share_filter_agg, on=share_keycols, how="left")
            list_df.append(df_out)
        
        df_all = pd.concat(list_df)
        return df_all

def _get_rank(df, rank_col, value_col="monthly_avg_sales"):
    df = df.sort_values(by=[value_col], ascending=False).reset_index(drop=True)
    df[rank_col] = df.index + 1
    return df

def _sales_agg(df, grp_keys):
    df_agg1 = df.groupby(grp_keys)[["month"]].nunique().reset_index()
    df_agg2 = df.groupby(grp_keys, as_index=False)[["sales"]].sum()

    df_agg = df_agg1.merge(df_agg2, on=grp_keys, how="left")
    df_agg["monthly_avg_sales"] = df_agg["sales"] / df_agg["month"]
    return df_agg 

def run_agg_sales(df_product, df_store):
    for col in ["Range", "Segment"]:
        df_product[col].fillna("_NaN", inplace=True)
    df = pd.read_parquet(os.path.join(config.sdl_dir, "txn_store_sku_monthly_v2.parquet"))
    df_merge = df.merge(df_product[["product_id", "brand", "category", "Range", "Segment", "sku_code"]], on=["product_id"]).merge(
        df_store[["store_id", "store_code"]], on=["store_id"])

    df_filter = df_merge[(df_merge["year"].isin([2019, 2020])) & (df_merge["month"]!="2020-02")]
    df_filter = df_filter[df_filter["sales"]>1]

    grp_keys1 = ["store_code", "brand", "category"]
    df_agg_cat = _sales_agg(df_filter, grp_keys1)

    df_agg_cat_all = _sales_agg(df_merge, grp_keys1)
    df_agg_cat_all_sum = df_agg_cat_all.groupby(["store_code"], as_index=False)[["sales"]].sum().rename(columns={"sales": "total_sales"})
    df_agg_cat_share = df_agg_cat_all.merge(df_agg_cat_all_sum, on=["store_code"], how="left")
    df_agg_cat_share["share"] = df_agg_cat_share["sales"] / df_agg_cat_share["total_sales"]

    grp_keys2 = ["store_code", "brand", "category", "Range", "Segment"]
    df_agg_cat_range_seg = _sales_agg(df_filter, grp_keys2)

    by_sku = ["store_code", "brand", "category", "Range", "Segment", "sku_code"]
    df_sku_agg = _sales_agg(df_filter, by_sku)

    print("| -- monthly sales aggregated")
    return df_agg_cat, df_agg_cat_share, df_agg_cat_range_seg, df_sku_agg

def cluster_brand_profile(df_cluster, df_brand_type):
    df = pd.read_csv(os.path.join(config.sdl_dir, "txn_store_category_yearly_v2.csv")).drop("brand_type", axis=1)
    df["avg_monthly_sales"] = df["sum_sales"] / df["cnt_months_w_sales"]
    df_merge = df.merge(df_cluster, on=["store_code"]).merge(df_brand_type, on=["brand", "category"])

    by_cat = ["cluster", "store_code", "category"]
    df_agg1 = df_merge.groupby(by_cat, as_index=False)[["avg_monthly_sales"]].sum().rename(
        columns={"avg_monthly_sales": "avg_monthly_sales_by_cat"}
    )

    df_agg2 = df_merge.groupby(by_cat + ["brand_type"], as_index=False)[["avg_monthly_sales"]].sum()

    df_rank = df_agg2.merge(df_agg1, on=by_cat)
    df_rank["brand_type_share"] = df_rank["avg_monthly_sales"] / df_rank["avg_monthly_sales_by_cat"]

    df_rank_agg = df_rank.groupby(["cluster", "category", "brand_type"], as_index=False)[["brand_type_share"]].mean()

    df_rank_agg = df_rank_agg.groupby(["category", "brand_type"], as_index=False).apply(
        _get_rank, "brand_type_share_rank", "brand_type_share")

    df_rank_agg.to_csv(os.path.join(config.model_dir, "cluster", "cluster_brand_type_share_rank.csv"), index=False)
    return df_rank_agg

def get_space():
    df_raw = pd.read_csv(os.path.join(config.raw_processed, "space_shelf_meter.csv")).rename(columns={
        "category": "category_from_space"
    })
    min_weekstartdate = df_raw["weekstartdate"].min()
    df_mapping = pd.read_csv(os.path.join(config.ref_dir, "shelf_pog_brand_category_mapping.csv"))

    df_space = df_raw.merge(
        df_mapping,
        on=["brand", "pog_name"],
        how="left"
    )
    
    df_space = df_space[df_space["weekstartdate"]==min_weekstartdate]
    return df_space

def read_brand_type():
    df_brand_type = pd.read_csv(os.path.join(config.ref_dir, "dim_brand_type_revised.csv"))
    brand_type_dict = {"SKINCARE": {}, "MAKE UP": {}, "FRAGRANCE": {}}
    for i in df_brand_type.values:
        brand_type_dict[i[1]].update({i[0]: i[2]})

    return df_brand_type, brand_type_dict

def run_assortment(cluster_file):
    print("| -- load config for assortment")
    conf = config.assortment_config

    print("| -- read input data")
    df_store = pd.read_excel(os.path.join(config.ref_dir, "dim_stores_offline_v5.xlsx"), engine="openpyxl")
    df_product = pd.read_excel(os.path.join(config.ref_dir, "dim_product_with_txn.xlsx"), engine="openpyxl")
    df_brand_type, brand_type_dict = read_brand_type()

    df_store_cluster = pd.read_csv(os.path.join(config.model_dir, "cluster", cluster_file))

    df_cluster_brand_rank = cluster_brand_profile(df_store_cluster, df_brand_type)

    df_agg_cat, df_agg_cat_share, df_agg_cat_range_seg, df_sku_agg = run_agg_sales(df_product, df_store)
    df_space = get_space()

    base_dir = os.path.join(config.model_dir, "assortment")
    ts = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    out_dir = os.path.join(base_dir, ts)
    os.mkdir(out_dir)

    for cluster in df_store_cluster["cluster"].unique():
        print("| -- running assortment for {} cluster".format(cluster))
        stores = list(df_store_cluster[df_store_cluster["cluster"]==cluster]["store_code"].unique())
        cluster_assort = StoreAssortment(conf, cluster, stores, brand_type_dict, df_agg_cat, df_space)
        df_assort_add = cluster_assort.add_item(df_cluster_brand_rank)

        df_assort_add_remove = cluster_assort.remove_item(df_assort_add)
        df_assort_add_remove.to_csv(os.path.join(out_dir, "{}_assortment_add_remove.csv".format(cluster)), index=False)

        df_sku_assort = cluster_assort.map_sku_assortment(df_product, df_assort_add_remove, df_agg_cat_range_seg, df_sku_agg)
        df_sku_assort_out = cluster_assort.attach_other_store_share(df_sku_assort, df_agg_cat_share)
        df_sku_assort_out.to_csv(
            os.path.join(out_dir, "{}_assortment_sku.csv".format(cluster)), 
            index=False
        )

if __name__ == "__main__":
    run_assortment(sys.argv[1])