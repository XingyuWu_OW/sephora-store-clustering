import os
import sys
import numpy as np
import pandas as pd

import warnings
warnings.filterwarnings("ignore")

from src import config

def read_data(filepath):
    df = pd.read_csv(filepath)
    return df

def merge_data(df1,df2):
    df_merge = pd.merge(df1,df2,on='store_code', how='left')
    #df_merge.to_csv(outpath, index=False, encoding='utf_8_sig')
    return df_merge

def add_sales(assort,share,sales):
    if assort == "ADD":
        real_share = share * sales
    elif assort == "REMOVE":
        real_share = 0
        #real_share = 0 - share
    else:
        real_share = 0
    return real_share

def add_sales_sku(sku_opportunity, assort, share, sales):
    if (sku_opportunity == 1) | (assort == "KEEP"):
        real_share = share * sales
    else:
        real_share = 0
    return real_share

def add_sales_max(assort,share,sales):
    if (assort == "ADD") | (assort == "NOT ADD"):
        real_share = share * sales
    elif assort == "REMOVE":
        real_share = 0
        #real_share = 0 - share
    else:
        real_share = 0
    return real_share

def remove_sales(assort,sales):
    if assort == "SUGGEST REMOVE":
        real_share = 0 - sales
    else:
        real_share = 0
    return real_share

def get_total_sales():
    df = pd.read_csv(os.path.join(config.sdl_dir, "txn_store_category_yearly_v2.csv"))
    df["avg_monthly_sales"] = df["sum_sales"] / df["cnt_months_w_sales"]
    df = df[df["year"]>=2019]
    df_agg = df.groupby(["year", "store_code"], as_index=False)[["avg_monthly_sales"]].sum()
    df_agg2 = df_agg.groupby(["store_code"], as_index=False)[["avg_monthly_sales"]].mean().rename(
        columns={"avg_monthly_sales": "average_month_sales"})
    return df_agg2[["store_code", "average_month_sales"]]

if __name__ == '__main__':
    filepath = sys.argv[2]
    input_store_cluster = sys.argv[1]
    assortment_col = sys.argv[3]
    #outpath = "/data/model/assortment/measurement/measure_{}.csv".format(filename)
    df_store_cluster = pd.read_csv(os.path.join(config.model_dir, "cluster", input_store_cluster))
    df_store_cluster = df_store_cluster[["store_code","cluster"]]
    df_store_cluster_list = df_store_cluster[["cluster"]]
    df_store_cluster_list = df_store_cluster_list.drop_duplicates()

    df_store_sales = get_total_sales()

    df_add_remove_sales_sku = pd.DataFrame(columns=["store_code", "sales_add_sku"])
    df_add_remove_sales = pd.DataFrame(columns=["store_code", "sales_add", "sales_remove", "sales_add_max"])

    if "_adjust" in assortment_col:
        assortment_file = "cluster_assortment_sku_adjust.csv"
        assortment_col = "assortment"
    else:
        assortment_file = "cluster_assortment_sku.csv"

    df_raw = read_data("/data/model/assortment/{}/{}".format(filepath, assortment_file))

    df_input_cluster_result_sku = df_raw[
        ["store_code", "brand", "category", assortment_col, "other_store_weighted_avg_share", 
        "avg_monthly_sales", "sku_opportunity", "cluster_avg_sku_share"]]
    df_input_cluster_result_sku.rename(columns={"avg_monthly_sales": "monthly_avg_sales"},
                                    inplace=True)

    sel_cols = ["store_code", "brand", "category", assortment_col, "other_store_weighted_avg_share", "monthly_avg_sales"]
    df_input_cluster_result = df_input_cluster_result_sku[sel_cols].drop_duplicates()

    sel_cols1 = ["store_code", assortment_col, "sku_opportunity", "cluster_avg_sku_share"]
    sel_cols2 = ["store_code", assortment_col, "other_store_weighted_avg_share", "monthly_avg_sales"]
    df_input_cluster_result = df_input_cluster_result[sel_cols2]
    df_input_cluster_result_sku = df_input_cluster_result_sku[sel_cols1]

    df_input_cluster_result = df_input_cluster_result.groupby(["store_code", assortment_col], as_index = False).aggregate(np.sum)
    df_input_cluster_result = df_input_cluster_result.merge(df_store_sales, on=["store_code"])

    df_input_cluster_result_sku = df_input_cluster_result_sku[
        (df_input_cluster_result_sku[assortment_col] == "KEEP") & 
        (df_input_cluster_result_sku["sku_opportunity"] == 1)
    ]
    df_input_cluster_result_sku = df_input_cluster_result_sku.groupby(["store_code", assortment_col], as_index = False).aggregate(np.sum)
    df_input_cluster_result_sku = df_input_cluster_result_sku.merge(df_store_sales, on=["store_code"])        

    df_input_cluster_result["sales_add"] = df_input_cluster_result.apply(
        lambda x: add_sales(x[assortment_col], x["other_store_weighted_avg_share"], x["average_month_sales"]), axis=1)

    df_input_cluster_result["sales_add_max"] = df_input_cluster_result.apply(
        lambda x: add_sales_max(x[assortment_col], x["other_store_weighted_avg_share"], x["average_month_sales"]), axis=1)

    df_input_cluster_result["sales_remove"] = df_input_cluster_result.apply(
        lambda x: remove_sales(x[assortment_col], x["monthly_avg_sales"]), axis=1)

    df_input_cluster_result_sku["sales_add_sku"] = df_input_cluster_result_sku.apply(
        lambda x: add_sales_sku(x["sku_opportunity"], x[assortment_col], x["cluster_avg_sku_share"], x["average_month_sales"]), axis=1)

    #df_input_cluster_result = df_input_cluster_result[["store_code", "sales_add", "sales_add_sku", "sales_remove", "sales_add_max"]]
    df_input_cluster_result = df_input_cluster_result[["store_code", "sales_add", "sales_remove", "sales_add_max"]]
    df_add_remove_sales = df_input_cluster_result.groupby(["store_code"], as_index = False).aggregate(np.sum)

    df_input_cluster_result_sku = df_input_cluster_result_sku[["store_code", "sales_add_sku"]]
    df_add_remove_sales_sku = df_input_cluster_result_sku.groupby(["store_code"], as_index = False).aggregate(np.sum)

    df_add_remove_sales = df_add_remove_sales.merge(df_store_cluster, on=["store_code"])
    df_add_remove_sales_sku = df_add_remove_sales_sku.merge(df_store_cluster, on=["store_code"])

    total_add = df_add_remove_sales["sales_add"].sum() * 12
    total_add_sku = df_add_remove_sales_sku["sales_add_sku"].sum() * 12
    total_remove = df_add_remove_sales["sales_remove"].sum() * 12
    net_opp = total_add + total_add_sku + total_remove
    net_opp_no_sku = total_add + total_remove

    print("| -- total add is {:,}".format(total_add))
    print("| -- total add from SKU is {:,}".format(total_add_sku))
    print("| -- total remove is {:,}".format(total_remove))
    print("| -- net opportunity excl. SKU is {:,}".format(net_opp_no_sku))
    print("| -- net opportunity is {:,}".format(net_opp))
