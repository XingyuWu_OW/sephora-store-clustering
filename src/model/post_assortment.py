import os
import sys
import numpy as np
import pandas as pd

import warnings
warnings.filterwarnings("ignore")

from src import config
from src.utils.io import load_config

def read_data(filepath):
    df = pd.read_csv(filepath)
    return df

def merge_data(df1, df2):
    df_merge = pd.merge(df1, df2, on="store_code", how="left")
    return df_merge

def add_sales(assort, share, sales):
    if assort == "ADD":
        real_share = share * sales
    elif assort == "SUGGEST REMOVE":
        real_share = 0
    else:
        real_share = 0
    return real_share


def remove_sales(assort, sales):
    if assort == "SUGGEST REMOVE":
        real_share = 0 - sales
    else:
        real_share = 0
    return real_share

def add_index(assort):
    if assort == "ADD":
        index = 1
    elif assort == "NOT ADD":
        index = 2
    elif assort == "KEEP":
        index = 3
    elif assort == "SUGGEST REMOVE":
        index = 4
    else:
        index = 5
    return index

def num_add(assort):
    if assort == "ADD":
        num = 1
    else:
        num = 0
    return num

def num_remove(assort):
    if assort == "SUGGEST REMOVE":
        num = 1
    else:
        num = 0
    return num

def num_keep(assort):
    if assort == "KEEP":
        num = 1
    else:
        num = 0
    return num


def load_store_sales(df_store_cluster, assortment_config, dim_store_info):
    """
    Load sales from SDL
    """
    input_store_sales = os.path.join(config.sdl_dir, "txn_store_category_yearly_v2.csv")
    df_store_sales = read_data(input_store_sales)
    df_store_sales["average_month_sales"] = df_store_sales["sum_sales"] / df_store_sales["cnt_months_w_sales"]
    df_store_sales = df_store_sales[
        df_store_sales["year"] >= assortment_config["txn_start_year"]
    ][["year", "store_code", "average_month_sales"]]

    df_agg_store_year = df_store_sales.groupby(["year", "store_code"], as_index = False).agg(np.sum)
    df_agg_store = df_agg_store_year.groupby("store_code", as_index = False).agg(np.average)
    df_sales_out = merge_data(df_agg_store[["store_code", "average_month_sales"]], df_store_cluster)
    df_sales_out = df_sales_out.merge(dim_store_info, on=["store_code"], how="left")
    return df_sales_out

def load_dim_store():
    """
    Load store dimension table
    """
    df_store_info = pd.read_excel(os.path.join(config.ref_dir, "dim_stores_offline_v5.xlsx"), engine="openpyxl")
    df_store_info["store_name_code"] = df_store_info.apply(lambda x: str(x["store_code"]) + " " + x["store_name"], axis=1)
    df_store_info = df_store_info[["store_code", "store_name_code", "region","city"]].rename(
        columns={"store_name_code": "store_name"})
    return df_store_info

def prep_for_dashboard(df_raw, df_store_cluster, df_sku_name, df_store_sales, df_brand_type, outpath, outpath_summary, outpath_summary_brand, outpath_summary_sku, outpath_summary_cluster):
    df_store_cluster = df_store_cluster[["store_code", "cluster"]]
    df_store_cluster_list = df_store_cluster[["cluster"]]
    df_store_cluster_list = df_store_cluster_list.drop_duplicates()

    df_output_data = pd.DataFrame(columns=["store_code", "store_name", "region", "city",
                                                          "cluster", "brand", "category", "monthly_avg_sales", "monthly_avg_sales_all", "sales_after_add",
                                                          "sales_add", "pct_sales_add", "sales_remove", "pct_sales_remove", "pct_store_sales",
                                                          "other_store_weighted_avg_share", "assortment_index", "assortment", "shelf_new", "shelf_add", "shelf_remove"])

    df_output_data_summary = pd.DataFrame(columns=["cluster", "category", "average_month_sales",
                                           "sales_add", "sales_add_annual", "sales_remove",
                                           "sales_remove_annual", "pct_sales_add", "pct_sales_remove", "shelf_add", "shelf_remove"])

    df_output_data_summary_brand = pd.DataFrame(columns=["brand", "category", "average_month_sales",
                                                   "sales_add", "sales_add_annual", "sales_remove",
                                                   "sales_remove_annual", "pct_sales_add", "pct_sales_remove",
                                                   "shelf_add", "shelf_remove"])
    df_output_data_summary_sku = pd.DataFrame(columns=["brand", "category", "product_name", "product_name_en",
                                                   "sales_add_sku", "sales_add_annual_sku",
                                                    "pct_sales_add_sku", "mini"
                                                   ])

    df_output_data_summary_cluster = pd.DataFrame(columns=["cluster", "brand", "category", "average_month_sales",
                        "sales_add", "sales_remove", "shelf_add", "shelf_remove", "num_add", "num_keep", "num_remove"
                                                   ])
    for i in df_raw["cluster"].unique():
        df_input_cluster_result = df_raw[df_raw["cluster"]==i].drop("cluster", axis=1)

        #sku calculation
        df_input_cluster_result_sku_level = df_input_cluster_result[["store_code", "brand", "category", "sku_code", "cluster_avg_sku_share", "sku_opportunity"]]
        df_input_sku_mini = df_input_cluster_result[["store_code", "sku_code", "mini"]]
        df_input_sku_mini = df_input_sku_mini[df_input_sku_mini["mini"]==1]

        df_input_sku_mini["sku_code"] = df_input_sku_mini["sku_code"].astype(
            int).astype(str)
        df_input_cluster_result_sku_level = df_input_cluster_result_sku_level[
            df_input_cluster_result_sku_level["sku_opportunity"] == 1]
        df_input_cluster_result_sku_level["sku_code"] = df_input_cluster_result_sku_level["sku_code"].astype(int).astype(str)
        df_input_cluster_result_sku_level = merge_data(df_input_cluster_result_sku_level,df_store_sales)

        df_input_cluster_result_sku_level = pd.merge(df_input_cluster_result_sku_level, df_sku_name,on="sku_code",how="left")


        df_input_cluster_result_sku_level["sales_add_sku"] = df_input_cluster_result_sku_level.apply(
            lambda x: x["sku_opportunity"] * x["cluster_avg_sku_share"] * x["average_month_sales"], axis=1)
        df_input_cluster_result_sku_level["pct_sales_add_sku"] = df_input_cluster_result_sku_level.apply(
            lambda x: x["sku_opportunity"] * x["cluster_avg_sku_share"], axis=1)
        df_input_cluster_result_sku_level["sales_add_annual_sku"] = df_input_cluster_result_sku_level.apply(
            lambda x: x["sales_add_sku"] * 12, axis=1)


        df_input_cluster_result_sku_level = df_input_cluster_result_sku_level[["store_code", "brand", "category", "sku_code", "product_name", "product_name_en", "sales_add_sku", "sales_add_annual_sku", "pct_sales_add_sku", "sku_opportunity"]]
        df_input_cluster_result_sku_level = pd.merge(df_input_cluster_result_sku_level, df_input_sku_mini,on=["store_code", "sku_code"],how="left")
        df_output_data_summary_sku = pd.concat([df_input_cluster_result_sku_level, df_output_data_summary_sku])


        # calculate brand level
        df_input_cluster_result = df_input_cluster_result[["store_code", "brand", "category", "assortment",
                                                            "other_store_weighted_avg_share", "avg_monthly_sales",
                                                            "shelf", "require_shelf"]]
        df_input_cluster_result.rename(columns={"avg_monthly_sales": "monthly_avg_sales"}, inplace=True)

        df_input_cluster_result = df_input_cluster_result.drop_duplicates()
        df_input_cluster_result = merge_data(df_input_cluster_result, df_store_sales)
        df_input_cluster_result = df_input_cluster_result[(df_input_cluster_result["category"] == "SKINCARE") |
                                                            (df_input_cluster_result["category"] == "MAKE UP") |
                                                            (df_input_cluster_result["category"] == "FRAGRANCE")]
        # calculate column needed
        df_input_cluster_result["sales_add"] = df_input_cluster_result.apply(
            lambda x: add_sales(x["assortment"], x["other_store_weighted_avg_share"], x["average_month_sales"]), axis=1)
        df_input_cluster_result["pct_sales_add"] = df_input_cluster_result.apply(
            lambda x: x["sales_add"] / x["average_month_sales"], axis=1)
        df_input_cluster_result["sales_remove"] = df_input_cluster_result.apply(
            lambda x: remove_sales(x["assortment"], x["monthly_avg_sales"]), axis=1)
        df_input_cluster_result["pct_sales_remove"] = df_input_cluster_result.apply(
            lambda x: x["sales_remove"] / x["average_month_sales"], axis=1)
        df_input_cluster_result["sales_after_add"] = df_input_cluster_result.apply(
            lambda x: (float(x["monthly_avg_sales"]) + float(x["sales_add"])), axis=1)
        df_input_cluster_result["pct_store_sales"] = df_input_cluster_result.apply(
            lambda x: x["monthly_avg_sales"] / x["average_month_sales"], axis=1)
        df_input_cluster_result["num_add"] = df_input_cluster_result.apply(
            lambda x: num_add(x["assortment"]), axis=1)
        df_input_cluster_result["num_keep"] = df_input_cluster_result.apply(
            lambda x: num_keep(x["assortment"]), axis = 1)
        df_input_cluster_result["num_remove"] = df_input_cluster_result.apply(
            lambda x: num_remove(x["assortment"]), axis=1)

        df_input_cluster_result["monthly_avg_sales"] = df_input_cluster_result["monthly_avg_sales"].fillna(0)
        df_input_cluster_result["monthly_avg_sales_all"] = df_input_cluster_result["monthly_avg_sales"] + \
                                                            df_input_cluster_result["sales_add"]
        df_input_cluster_result["assortment_index"] = df_input_cluster_result.apply(
            lambda x: add_index(x["assortment"]), axis=1)

        df_input_cluster_result["shelf_add"] = np.where(
            df_input_cluster_result["assortment"] == "ADD",
            df_input_cluster_result["require_shelf"],
            0
        )

        df_input_cluster_result["shelf_remove"] = np.where(
            df_input_cluster_result["assortment"] == "SUGGEST REMOVE",
            df_input_cluster_result["shelf"] * (-1),
            0
        )

        df_input_cluster_result["shelf_new"] = df_input_cluster_result["shelf"].combine_first(df_input_cluster_result["shelf_add"])

        df_input_cluster_result_summary = df_input_cluster_result[[
            "store_code", "cluster", "category", "average_month_sales",
            "sales_add", "sales_remove",
            "shelf_add", "shelf_remove"]]

        df_input_cluster_result_summary_brand = df_input_cluster_result[[
            "store_code", "brand", "category", "average_month_sales",
            "sales_add", "sales_remove",
            "shelf_add", "shelf_remove", "num_add", "num_keep", "num_remove"]]

        df_input_cluster_result_summary_cluster = df_input_cluster_result[[
            "cluster", "brand", "category", "average_month_sales",
            "sales_add", "sales_remove",
            "shelf_add", "shelf_remove", "num_add", "num_keep", "num_remove"]]

        df_input_cluster_result = df_input_cluster_result[["store_code", "store_name", "region", "city",
                                                          "cluster", "brand", "category", "monthly_avg_sales", "monthly_avg_sales_all", "sales_after_add",
                                                          "sales_add", "pct_sales_add", "sales_remove", "pct_sales_remove", "pct_store_sales",
                                                          "other_store_weighted_avg_share", "assortment_index", "assortment", "shelf_new", "shelf_add", "shelf_remove"]]

        df_output_data = pd.concat([df_input_cluster_result, df_output_data])

        df_input_cluster_result_summary = df_input_cluster_result_summary.groupby(["store_code", "cluster", "category", "average_month_sales"], as_index=False).agg(
            np.sum)

        df_input_cluster_result_summary = df_input_cluster_result_summary[["cluster", "category", "average_month_sales",
                                                            "sales_add", "sales_remove", "shelf_add", "shelf_remove"
                                                            ]]

        df_input_cluster_result_summary = df_input_cluster_result_summary.groupby(["cluster", "category"], as_index=False).agg(
            np.sum)

        df_input_cluster_result_summary["pct_sales_add"] = df_input_cluster_result_summary.apply(
            lambda x: x["sales_add"] / x["average_month_sales"], axis=1)
        df_input_cluster_result_summary["pct_sales_remove"] = df_input_cluster_result_summary.apply(
            lambda x: x["sales_remove"] / x["average_month_sales"], axis=1)

        df_input_cluster_result_summary["sales_add_annual"] = df_input_cluster_result_summary["sales_add"] * 12
        df_input_cluster_result_summary["sales_remove_annual"] = df_input_cluster_result_summary["sales_remove"] * 12

        df_input_cluster_result_summary = df_input_cluster_result_summary[["cluster", "category", "average_month_sales",
                                                            "sales_add", "sales_add_annual", "sales_remove",
                                                            "sales_remove_annual", "pct_sales_add", "pct_sales_remove", "shelf_add", "shelf_remove"]]

        df_output_data_summary = pd.concat([df_input_cluster_result_summary, df_output_data_summary])


        # calculate summary for cluster
        df_input_cluster_result_summary_cluster = df_input_cluster_result_summary_cluster.groupby (["cluster", "brand", "category"],as_index=False).agg(np.sum)
        df_output_data_summary_cluster = pd.concat([df_input_cluster_result_summary_cluster, df_output_data_summary_cluster])

        # calculate summary data for brand
        df_input_cluster_result_summary_brand = df_input_cluster_result_summary_brand.groupby(
            ["store_code", "brand", "category", "average_month_sales"], as_index=False).agg(
            np.sum)

        df_input_cluster_result_summary_brand = df_input_cluster_result_summary_brand[["brand", "category", "num_add", "num_keep", "num_remove", "average_month_sales",
                                                                            "sales_add", "sales_remove", "shelf_add",
                                                                            "shelf_remove"
                                                                            ]]


        df_output_data_summary_brand = pd.concat([df_input_cluster_result_summary_brand, df_output_data_summary_brand])

    df_output_data = df_output_data.merge(df_brand_type[["brand", "category", "brand_type"]], on=["brand", "category"], how="left")
    df_output_data.to_excel(outpath, index=False, encoding="utf_8_sig")
    df_output_data_summary.to_csv(outpath_summary, index=False, encoding="utf-8")
    df_output_data_summary_cluster.to_csv(outpath_summary_cluster, index=False, encoding="utf-8")

    # aggregate to total
    total_sales = df_store_sales[~pd.isnull(df_store_sales["cluster"])]["average_month_sales"].sum()

    df_output_data_summary_brand["pct_sales_add"] = df_output_data_summary_brand.apply(
        lambda x: x["sales_add"] / total_sales, axis=1)
    df_output_data_summary_brand["pct_sales_remove"] = df_output_data_summary_brand.apply(
        lambda x: x["sales_remove"] / total_sales, axis=1)

    df_output_data_summary_brand["sales_add_annual"] = df_output_data_summary_brand["sales_add"] * 12
    df_output_data_summary_brand["sales_remove_annual"] = df_output_data_summary_brand[
                                                                       "sales_remove"] * 12

    df_output_data_summary_brand =  df_output_data_summary_brand.groupby(["brand", "category"],as_index=False).agg(
        np.sum)

    df_output_data_summary_brand = df_output_data_summary_brand[
        ["brand", "category", "num_add", "num_keep", "num_remove",
         "sales_add", "sales_add_annual",
         "sales_remove",
         "sales_remove_annual", "pct_sales_add",
         "pct_sales_remove", "shelf_add",
         "shelf_remove"]]


    df_output_data_summary_brand.to_csv(outpath_summary_brand, index=False, encoding="utf-8")

    df_output_data_summary_store_sku = df_output_data_summary_sku[
        ["store_code", "brand", "category", "sku_code", "product_name", "product_name_en",
         "sales_add_sku", "sales_add_annual_sku", "pct_sales_add_sku"]
    ]
    df_output_data_summary_store_sku = df_output_data_summary_store_sku.merge(
        df_output_data[["store_code", "store_name", "region", "cluster"]].drop_duplicates(),
        on=["store_code"]
    )

    df_output_data_summary_sku = df_output_data_summary_sku[["brand", "category", "sku_code", "product_name", "product_name_en",
                                                   "sales_add_sku", "sales_add_annual_sku",
                                                    "pct_sales_add_sku", "sku_opportunity", "mini"]]
    df_output_data_summary_sku = df_output_data_summary_sku.groupby(
        ["brand", "category", "sku_code", "product_name", "product_name_en"], as_index=False).agg(np.sum)
    df_output_data_summary_sku = df_output_data_summary_sku.sort_values(by="sales_add_annual_sku",ascending=False)

    out_cols = ["store_code", "store_name", "region", "cluster", "brand", "category", "sku_code", "product_name", "product_name_en",
         "sales_add_sku", "sales_add_annual_sku", "pct_sales_add_sku"]
    df_output_data_summary_store_sku[out_cols].to_excel(outpath_summary_sku, index=False, encoding="utf_8_sig")

    return None

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Must specify assortment result dir name. Usage: python3 src/model/post_assortment.py <assortment dir name>")
    else:
        assort_dir = sys.argv[1]
        config_path = os.path.join(config.model_dir, "assortment", assort_dir, "assortment_config.json")
        assortment_config = load_config(config_path)

        input_store_cluster = os.path.join(config.model_dir, "cluster", assortment_config["cluster_file"])
        df_store_cluster = read_data(input_store_cluster)

        # read sku name
        input_sku_name = os.path.join(config.ref_dir, "dim_product_with_txn.xlsx")
        df_sku_name = pd.read_excel(input_sku_name, engine="openpyxl")[["sku_code", "product_name", "product_name_en"]]
        df_sku_name["sku_code"] = df_sku_name["sku_code"].astype(str)

        # load store info
        dim_store_info = load_dim_store()

        # load sales
        df_store_sales = load_store_sales(df_store_cluster, assortment_config, dim_store_info)

        brand_type_file = assortment_config["brand_type_file"]
        df_brand_type = pd.read_csv(os.path.join(config.input_dir, brand_type_file))

        for assort_file in ["cluster_assortment_sku.csv", "cluster_assortment_sku_adjust.csv"]:
            if "_adjust" in assort_file:
                out_dir = os.path.join(config.model_dir, "assortment", assort_dir, "dashboard_adjust")
            else:
                out_dir = os.path.join(config.model_dir, "assortment", assort_dir, "dashboard")

            if not os.path.exists(out_dir):
                os.mkdir(out_dir)

            input_assortment_result = os.path.join(config.model_dir, "assortment", assort_dir, assort_file)
            df_raw = read_data(input_assortment_result)

            # store_brand_category tab (excel)
            outpath = os.path.join(out_dir, "store_brand_category.xlsx")

            # summary_cluster_category tab
            outpath_summary = os.path.join(out_dir, "summary_cluster_category.csv")

            # summary_brand_category tab
            outpath_summary_brand = os.path.join(out_dir, "summary_brand_category.csv")

            # sku tab
            outpath_summary_sku = os.path.join(out_dir, "store_sku.xlsx")

            # cluster view tab
            outpath_summary_cluster = os.path.join(out_dir, "summary_cluster_brand.csv")

            outpath_hair_men = os.path.join(out_dir, "hair_men_deepdive.csv")

            prep_for_dashboard(df_raw, df_store_cluster, df_sku_name, df_store_sales, df_brand_type, outpath, outpath_summary, outpath_summary_brand, outpath_summary_sku, outpath_summary_cluster)
            
            # copy EB
            os.system("cp {}/../eb_opportunity.xlsx {}/eb_opportunity.xlsx".format(out_dir, out_dir))

            # copy cluster brand type rank
            cluster_brandtype_rank_path = os.path.join(config.model_dir, "cluster", "cluster_brand_type_share_rank_monthly.csv")
            os.system("cp {} {}/cluster_brand_type_rank.csv".format(cluster_brandtype_rank_path, out_dir))
        
        # for HAIR and MEN deepdive
        outpath_hair_men = os.path.join(out_dir, "hair_men_deepdive.csv")
        df_summary = pd.read_csv(os.path.join(config.model_dir, "assortment", assort_dir, "cluster_assortment_summary_adjust.csv"))
        df_hair_men = df_summary[
            df_summary["category"].isin(["HAIR", "MEN_SC"]) & ~pd.isnull(df_summary["share"])
        ]
        hair_men_out_cols = ["store_code", "brand", "category", "avg_monthly_sales", "cluster", "share"]
        df_hair_men[hair_men_out_cols].to_csv(outpath_hair_men, index=False, encoding="utf-8")
