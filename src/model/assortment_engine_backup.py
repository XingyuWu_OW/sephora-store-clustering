import os
import sys
import json
import datetime
import numpy as np
import pandas as pd

import warnings
warnings.filterwarnings("ignore")

from src import config
from src.utils.io import load_config
from src.utils.common import get_rank
from eb_opportunity import run_eb_opportunity
from assortment_adjust import run_adjust

class StoreAssortment:
    def __init__(self, level, assort_config, cluster, stores, df_cluster_brand_rank, df_sales, df_shelf, df_brand_first, 
    available_shelf_dict, outlet_stores=None, new_stores=None, premium_stores=None, premium_product=None, apply_cannot_add=False):
        self.level = level
        self.outlet_stores = outlet_stores
        self.new_stores = new_stores
        self.premium_stores = premium_stores
        self.cluster = cluster
        self.stores = stores
        self.df_sales = df_sales
        self.df_shelf = df_shelf
        self.cluster_brand_rank = df_cluster_brand_rank
        self.df_brand_first = df_brand_first
        self.df_product_premium = premium_product
        self.available_shelf_dict = available_shelf_dict
        self.key_cols = ["brand", "category"]

        # initialize with configurations
        self.config = assort_config
        self.brand_profile_threshold = self.config["brand_profile_threshold"]
        self.core_categories = self.config["core_categories"]
        if apply_cannot_add:
            self.cannot_add = self.config["cannot_add"]
        else:
            self.cannot_add = {}
        self.large_brands = self.config["large_brands"]
        self.add_metric = self.config["add_metric"]
        self.remove_by_shelf = self.config["remove_by_shelf"]
        self.use_logic1 = self.config["use_logic1"]
        self.sku_rank_top = self.config["sku_rank_top"]
        self.apply_premium_tag = self.config["apply_premium_tag"]
        self.premium_brands = self.config["premium_brands"]
        self.top_brand_type_brands = self.get_top_brand_type_brands()
    
    def filter_sales(self, df):
        df_sales_filter = df[df["store_code"].isin(self.stores)]
        
        df_agg_pivot = df.pivot_table(
            index=self.key_cols, 
            values=["avg_{}_sales".format(self.level)], 
            columns=["store_code"]
        ).reset_index()
        store_lst = list(df["store_code"].unique())
        store_lst.sort()
        df_agg_pivot.columns = self.key_cols + ["avg_{}_sales_{}".format(self.level, store) for store in store_lst]

        sel_cols = self.key_cols + ["avg_{}_sales_{}".format(self.level, store) for store in self.stores]
        df_agg_pivot = df_agg_pivot[sel_cols]

        return df_agg_pivot, df_sales_filter
    
    def decide_add_within_cluster(self, store, brand, category, other_store_good_brands_dict, with_shelf, good_brands_thres):
        whether_add = "NOT ADD"
        if category in self.core_categories:
            all_brands_add = []
            all_brands = other_store_good_brands_dict[category]
            for b,v in all_brands.items():
                if v > 0 and v >= good_brands_thres:
                    all_brands_add.append(b)

            if brand in all_brands_add and with_shelf and brand not in self.cannot_add.get(category, ["_DEFAULT"]):
                if not self.apply_premium_tag:
                    whether_add = "ADD"
                else:
                    if store in self.premium_stores:
                        whether_add = "ADD"
                    else: 
                        if brand not in self.premium_brands.get(category, ["_DEFAULT"]):
                            whether_add = "ADD"
                        else:
                            whether_add = "NOT ADD"
        return whether_add

    def get_other_store_good_brands(self, store, df_agg, q):
        df_shelf = self.df_shelf.copy()
        other_store_good_brands_dict = {}
        left_cols = [col for col in df_agg.columns if str(store) not in col]
        for category in self.core_categories:
            df_left = df_agg[left_cols]
            df_sel = df_left[df_left["category"]==category]
            all_brands = {}
            all_brands_total = {}
            for other_store in self.stores:
                sales_col = "avg_{}_sales_{}".format(self.level, other_store)
                if self.add_metric == "productivity":
                    metric_col = "avg_{}_sales_per_shelf_{}".format(self.level, other_store)
                else:
                    metric_col = sales_col
                if other_store != store and other_store not in self.outlet_stores:
                    # remove large all-store brands and cannot-add brands when calculating cutoff
                    large_brands = set(self.large_brands.get(category, ["_DEFAULT"]) + self.cannot_add.get(category, ["_DEFAULT"]))

                    df_shelf_other_store = df_shelf[
                        (df_shelf["store_code"]==other_store) &
                        (df_shelf["category"]==category) & 
                        ~pd.isnull(df_shelf["shelf"])
                    ]
                    brands_with_shelf = df_shelf_other_store["brand"].unique()

                    for b in brands_with_shelf:
                        all_brands_total[b] = all_brands_total.get(b, 0) + 1

                    df_sel_new = df_sel[
                        ~(df_sel["brand"].isin(large_brands)) & 
                        (df_sel["brand"].isin(brands_with_shelf))  # with shelf
                    ].merge(df_shelf_other_store[["brand", "shelf"]], on=["brand"])

                    if self.add_metric == "productivity":
                        df_sel_new[metric_col] = df_sel_new[sales_col] / df_sel_new["shelf"]

                    cutoff = df_sel_new[metric_col].quantile(q)
                    brands = list(df_sel_new[df_sel_new[metric_col] > cutoff]["brand"].unique())
                    for b in brands:
                        all_brands[b] = all_brands.get(b, 0) + 1
            
            for k,v in all_brands.items():
                all_brands[k] = all_brands[k] / all_brands_total[k]

            other_store_good_brands_dict.update({category: all_brands})
        
        return other_store_good_brands_dict

    def add_item(self):
        df_cluster_brand_rank = self.cluster_brand_rank.copy()
        q = self.config["top_quantile"]
        good_brands_thres = self.config["good_brands_thres"]

        df_agg, df_sales_filter = self.filter_sales(self.df_sales)
        df_out = df_agg[self.key_cols]

        for store in self.stores:
            df_shelf_store = self.df_shelf[
                (self.df_shelf["store_code"]==store) & 
                (self.df_shelf["ind_opportunity"]==1)
            ][self.key_cols + ["shelf", "ind_opportunity"]]

            sales_col = "avg_{}_sales_{}".format(self.level, store)
            df_sel = df_agg[self.key_cols + [sales_col]]
            assort_col = "assortment_{}".format(store)
            add_logic_col = "add_logic_{}".format(store)
            df_sel.loc[:, assort_col] = np.array(["KEEP"] * len(df_sel))
            df_sel.loc[:, add_logic_col] = np.array([0] * len(df_sel))
            df_sel_shelf = df_sel.merge(df_shelf_store, on=self.key_cols, how="left")

            if store not in self.outlet_stores:
                print("| | -- decide ADD for {}".format(store))
                other_store_good_brands_dict = self.get_other_store_good_brands(store, df_agg, q)
                
                # no sales
                df_no_sales = df_sel_shelf[
                    (pd.isnull(df_sel_shelf[sales_col])) & 
                    (pd.isnull(df_sel_shelf["shelf"]))
                ][self.key_cols]

                # with small sales, no shelf, with opportunity
                df_opp = df_sel_shelf[
                    ~pd.isnull(df_sel_shelf[sales_col]) & 
                    pd.isnull(df_sel_shelf["shelf"]) &
                    (df_sel_shelf["ind_opportunity"] == 1)
                ][self.key_cols]

                #not_sell_items = pd.concat([df_no_sales, df_opp]).drop_duplicates().values
                not_sell_items = df_no_sales.drop_duplicates().values
                
                for item in not_sell_items:
                    brand = item[0]
                    category = item[1]

                    with_shelf = False
                    df_check_shelf = df_shelf_store[(df_shelf_store["category"]==category) & (df_shelf_store["brand"]==brand)]
                    if len(df_check_shelf) > 0:
                        with_shelf = True
                    
                    add_logic_flag = 0
                    if category in self.core_categories:
                        if self.use_logic1:  # Logic 1 + 2
                            if brand in self.top_brand_type_brands[category] and with_shelf and brand not in self.cannot_add.get(category, ["_DEFAULT"]):
                                if not self.apply_premium_tag:
                                    whether_add = "ADD"
                                    add_logic_flag = 1
                                else:
                                    if store in self.premium_stores:
                                        whether_add = "ADD"
                                        add_logic_flag = 1
                                    else:
                                        if brand not in self.premium_brands.get(category, ["_DEFAULT"]):
                                            whether_add = "ADD"
                                            add_logic_flag = 1
                                        else:
                                            whether_add = "NOT ADD"
                            else:
                                whether_add = self.decide_add_within_cluster(store, brand, category, other_store_good_brands_dict, with_shelf, good_brands_thres)
                                if whether_add == "ADD":
                                    add_logic_flag = 2        
                        else:  # Only Logic 2
                            whether_add = self.decide_add_within_cluster(store, brand, category, other_store_good_brands_dict, with_shelf, good_brands_thres)
                            if whether_add == "ADD":
                                add_logic_flag = 2
                    else:
                        whether_add = "NOT ADD"

                    df_sel.loc[(df_sel["brand"]==brand) & (df_sel["category"]==category), assort_col] = whether_add
                    df_sel.loc[(df_sel["brand"]==brand) & (df_sel["category"]==category), add_logic_col] = add_logic_flag
            else:
                df_sel = df_sel_shelf.copy()
                df_sel[assort_col] = np.where(
                    ~pd.isnull(df_sel[sales_col]),
                    "KEEP",
                    np.where(
                        df_sel["ind_opportunity"]==1,
                        "NOT ADD",
                        "NO SHELF"
                    )
                )

                df_sel.loc[:, add_logic_col] = 0

            df_out = df_out.merge(
                df_agg[self.key_cols + [sales_col]], on=self.key_cols
            ).merge(df_sel[self.key_cols + [assort_col, add_logic_col]], on=self.key_cols)

        return df_out
    
    def get_top_brand_type_brands(self):
        df_brand_rank = self.cluster_brand_rank[
            self.cluster_brand_rank["cluster"] == self.cluster
        ]

        top_brand_type_brands = {}
        for cat in df_brand_rank["category"].unique():
            all_brands = []
            df_sel = df_brand_rank[df_brand_rank["category"]==cat]
            for brand_type in df_sel["brand_type"].unique():
                thres = self.brand_profile_threshold[cat].get(brand_type, 3)
                df_top = df_sel[(df_sel["brand_type_share_rank"] <= thres) & (df_sel["brand_type"] == brand_type)]
                if len(df_top) > 0:
                    all_brands = all_brands + list(df_top["brand"].unique())
            
            top_brand_type_brands[cat] = set(all_brands)
        
        return top_brand_type_brands

    def decide_remove_by_shelf(self, store, df_sales, sales_col, df_all_min_shelf, df_new_brand):
        df_shelf_filter = self.df_shelf[~pd.isnull(self.df_shelf["shelf"])]
        df_shelf_store = df_shelf_filter[df_shelf_filter["store_code"]==store][self.key_cols + ["shelf"]]

        all_shelf_needed = df_all_min_shelf["min_shelf"].sum()
        
        # add available shelf in store
        available_shelf = self.available_shelf_dict.get(store, 0)
        all_shelf_needed = max(0, all_shelf_needed - available_shelf)

        df = df_sales[df_sales["category"].isin(self.core_categories)]

        # brands in top brand type
        lst = []
        for cat in self.core_categories:
            tmp_df = pd.DataFrame({"brand": list(self.top_brand_type_brands[cat])})
            tmp_df["category"] = cat
            lst.append(tmp_df)
        
        df_top_brand_type = pd.concat(lst)
        df_top_brand_type["ind_add_brand_type"] = 1

        sales_col = "avg_{}_sales_{}".format(self.level, store)
        df = df.merge(df_shelf_store, on=self.key_cols).merge(df_new_brand, on=self.key_cols, how="left")
        if len(df_top_brand_type) > 0:
            df = df.merge(df_top_brand_type, on=self.key_cols, how="left")

        df = df[~pd.isnull(df[sales_col])]
        df = df[(df["ind_keep_brand"]!=1) & (df["ind_add_brand_type"]!=1)].reset_index(drop=True)

        # cannot remove new brands in a store
        df = df.sort_values(by=sales_col).reset_index(drop=True)
        df["shelf_cumsum"] = df["shelf"].cumsum()

        min_index = df[df["shelf_cumsum"]>=all_shelf_needed].index.min()
        if (df.loc[min_index, "shelf_cumsum"] - all_shelf_needed) / all_shelf_needed > 0.2:
            min_index = min_index - 1

        df_remove = df[df.index<=min_index][self.key_cols]
        df_remove["remove_ind"] = 1
        
        return df_remove

    def decide_remove(self, df_sales, sales_col, add_items_dict, df_shelf, df_new_brand):
        df_shelf_filter = df_shelf[~pd.isnull(df_shelf["shelf"])]
        df = df_sales[df_sales["category"].isin(self.core_categories)]
        df = df.merge(df_shelf_filter, on=self.key_cols).merge(df_new_brand, on=self.key_cols, how="left")
        df = df[df["ind_keep_brand"]!=1]

        total_add = 0
        for k,v in add_items_dict.items():
            total_add = total_add + len(v)

        df_sort = df.sort_values(by=[sales_col])
        df_remove = df_sort.head(total_add)[self.key_cols]
        df_remove["remove_ind"] = 1

        return df_remove

    def remove_item(self, df_add):
        df_out = df_add[self.key_cols]
        all_cat = df_out["category"].unique()

        for store in self.stores:
            df_shelf_store = self.df_shelf[
                (self.df_shelf["store_code"]==store) & 
                (self.df_shelf["ind_opportunity"]==1)
            ][self.key_cols + ["shelf", "ind_opportunity"]]

            print("| | -- suggest REMOVE for {}".format(store))
            sales_col = "avg_{}_sales_{}".format(self.level, store)
            assort_col = "assortment_{}".format(store)
            add_logic_col = "add_logic_{}".format(store)
            require_shelf_col = "require_shelf_{}".format(store)
            sel_cols = self.key_cols + [sales_col, assort_col, add_logic_col]
            df_sel = df_add[sel_cols]

            df_new_brand = self.df_brand_first[self.df_brand_first["store_code"]==store][self.key_cols + ["ind_keep_brand"]]

            # get required shelf
            df_store_add = df_sel[df_sel[assort_col]=="ADD"]
            df_shelf_filter = self.df_shelf[~pd.isnull(self.df_shelf["shelf"])]
            df_shelf_sel = df_shelf_filter.merge(df_store_add[self.key_cols], on=self.key_cols)
            df_all_min_shelf = df_shelf_sel.groupby(self.key_cols, as_index=False)[["shelf"]].min().rename(
                columns={"shelf": "min_shelf"}
            )

            if store not in self.new_stores:  
                if self.remove_by_shelf:
                    df_remove = self.decide_remove_by_shelf(store, df_sel, sales_col, df_all_min_shelf, df_new_brand)
                else:
                    df_remove = self.decide_remove(df_sel, sales_col, add_items_dict, df_shelf_store, df_new_brand)
                df_sel = df_sel.merge(df_remove, on=self.key_cols, how="left")
                df_sel[assort_col] = np.where(
                    df_sel["remove_ind"] == 1,
                    "SUGGEST REMOVE",
                    df_sel[assort_col]
                )

            shelf_col = "shelf_{}".format(store)
            opp_col = "ind_opportunity_{}".format(store)
            df_require_shelf = df_all_min_shelf.rename(columns={"min_shelf": require_shelf_col})

            df_out = df_out.merge(
                df_sel[sel_cols], on=self.key_cols, how="left"
            ).merge(
                df_require_shelf, 
                on=self.key_cols, how="left"
            ).merge(
                df_shelf_store[self.key_cols + ["shelf", "ind_opportunity"]].rename(
                    columns={"shelf": shelf_col, "ind_opportunity": opp_col}),
                on=self.key_cols, how="left"
            )

            df_out[assort_col] = np.where(
                pd.isnull(df_out[opp_col]) & (df_out["category"].isin(self.core_categories)),
                "NO SHELF",
                df_out[assort_col]
            )

        return df_out

    def get_sku_share(self, df, col):
        df_agg_store = df.groupby(["store_code"], as_index=False)[[col]].sum().rename(columns={col: col + "_total"})

        df_agg = df.merge(df_agg_store, on=["store_code"])
        df_agg["sku_share"] = df_agg[col] / df_agg[col + "_total"]

        df_share_avg = df_agg.groupby(["sku_code"], as_index=False)[["sku_share"]].mean().rename(
            columns={"sku_share": "cluster_avg_sku_share"})

        return df_share_avg

    def rank_sku(self, dim_product, df_assort, df_sales_agg_all, df_sku_agg, df_mini, df_pog):

        avg_col = "avg_{}_sales".format(self.level)
        by_cat = self.key_cols
        by_range = by_cat + ["Range"]
        by_seg = by_range + ["Segment"]
        by_sku = by_seg + ["sku_code"]

        max_week_mini = df_mini["weekstartdate"].max()
        df_mini_sel = df_mini[
            (df_mini["weekstartdate"]==max_week_mini) &
            (df_mini["store"] != 6126)
        ].rename(columns={"store": "store_code", "sapcode": "sku_code"})[["store_code", "sku_code", "mini"]]

        max_week_pog = df_pog["weekstartdate"].max()
        df_pog = df_pog[df_pog["weekstartdate"]==max_week_pog]
        
        sku_scope = df_mini_sel[
            (df_mini_sel["store_code"].isin(df_sales_agg_all["store_code"].unique())) & 
            (df_mini_sel["mini"] > 0)
        ]["sku_code"].unique()

        pog_sku = df_pog["sku_code"].unique()

        df_sku_agg_filter = df_sku_agg[
            (df_sku_agg["sku_code"].isin(sku_scope)) & 
            (df_sku_agg["sku_code"].isin(pog_sku))
        ]

        df_sales_agg = df_sku_agg_filter[
            df_sku_agg_filter["store_code"].isin(self.stores)
        ]

        df_agg1 = df_sales_agg.groupby(
            by_sku, as_index=False
        )[[avg_col]].mean()

        df_agg_global = df_sku_agg_filter.groupby(
            by_sku, as_index=False
        )[[avg_col]].mean()

        df_agg1.head()
        df_rank_sku = df_agg1.groupby(by_cat, as_index=False).apply(get_rank, "sku_rank", avg_col).drop(avg_col, axis=1)
        df_rank_sku_global = df_agg_global.groupby(by_cat, as_index=False).apply(
            get_rank, "sku_rank", avg_col
        ).drop(avg_col, axis=1).rename(columns={"sku_rank": "sku_rank_global"})

        df_sku_share = self.get_sku_share(df_sales_agg, avg_col)
        df_sku_share_global = self.get_sku_share(df_sku_agg_filter, avg_col).rename(
            columns={"cluster_avg_sku_share": "global_avg_sku_share"}
        )

        all_sku = []
        for store_code in self.stores:
            print("| | -- rank sku for {}".format(store_code))
            df_mini_store = df_mini_sel[df_mini_sel["store_code"]==store_code][["sku_code", "mini"]]
            col = "assortment_{}".format(store_code)
            add_logic_col = "add_logic_{}".format(store_code)
            sales_col = "avg_{}_sales_{}".format(self.level, store_code)
            shelf_col = "shelf_{}".format(store_code)
            require_shelf_col = "require_shelf_{}".format(store_code)

            df_tmp = df_assort[by_cat + [col, add_logic_col, sales_col, shelf_col, require_shelf_col]]
            for df in [df_rank_sku, df_sku_share, df_mini_store]:
                df["sku_code"] = df["sku_code"].astype("int")

            df_tmp_sku = df_tmp.merge(df_rank_sku_global, on=by_cat, how="left").merge(
                df_rank_sku, on=by_cat + ["Range", "Segment", "sku_code"], how="left")
            
            df_tmp_sku = df_tmp_sku.merge(
                df_sku_share, on=["sku_code"], how="left"
            ).merge(
                df_sku_share_global, on=["sku_code"], how="left"
            ).merge(df_mini_store, on=["sku_code"], how="left").rename(
                columns={
                    col: "assortment", 
                    add_logic_col: "add_logic_flag", 
                    sales_col: "avg_{}_sales".format(self.level),
                    shelf_col: "shelf",
                    require_shelf_col: "require_shelf"
            })
            df_tmp_sku["cluster_avg_sku_share"] = df_tmp_sku["cluster_avg_sku_share"].combine_first(df_tmp_sku["global_avg_sku_share"])
            out_cols = list(df_tmp_sku.columns.values).copy()
            df_tmp_sku["store_code"] = store_code

            df_sku_agg_tmp = df_sku_agg[df_sku_agg["store_code"]==store_code][["sku_code"]].drop_duplicates()
            df_sku_agg_tmp["ind_existing_sku"] = 1

            df_tmp_sku = df_tmp_sku.merge(df_sku_agg_tmp, on=["sku_code"], how="left")
            df_tmp_sku["ind_existing_sku"].fillna(0, inplace=True)
        
            all_sku.append(df_tmp_sku[["store_code"] + out_cols + ["ind_existing_sku"]])
        
        df_all_sku = pd.concat(all_sku)

        df_all_sku_out = self.tag_sku_opportunity(df_all_sku)

        df_brand_cat_rank = self.get_brand_cat_rank(df_all_sku_out)
        df_all_sku_out_add_rank = df_all_sku_out.merge(df_brand_cat_rank, on=self.key_cols, how="left")

        return df_all_sku_out_add_rank

    def get_brand_cat_rank(self, df):
        df_dedup = df[["store_code", "brand", "category", "avg_monthly_sales"]].drop_duplicates()
        df_agg = df_dedup.groupby(self.key_cols, as_index=False)[["avg_monthly_sales"]].mean()
        df_rank = get_rank(df_agg, "brand_cat_rank", "avg_monthly_sales")
        return df_rank[self.key_cols + ["brand_cat_rank"]]
    
    def tag_sku_opportunity(self, df):
        out_cols = list(df.columns.values).copy()
        grp_keys = ["store_code", "brand", "category"]
        df_rank_max = df.groupby(grp_keys, as_index=False)[["sku_rank"]].max().rename(
            columns={"sku_rank": "sku_rank_max"}
        )

        df = df.merge(df_rank_max, on=grp_keys)
        df["sku_rank_quantile"] = df["sku_rank"] / df["sku_rank_max"]

        sku_excl_range = ["ACCESSORY", "DEVICE"]
        sku_excl_seg = ["_foo"]
        sku_cons = (df["category"].isin(self.core_categories)) & ~(df["Range"].isin(sku_excl_range)) & ~(df["Segment"].isin(sku_excl_seg)) & \
        (df["assortment"]=="KEEP") & ~pd.isnull(df["shelf"]) & \
        (df["ind_existing_sku"]==0) & (df["sku_rank_quantile"] <= self.sku_rank_top)
        #~(df["store_code"].isin(outlet_stores))

        df["sku_opportunity"] = np.where(
            sku_cons,
            1,
            0
        )

        df["sku_opportunity"] = np.where(
            pd.isnull(df["mini"]),
            df["sku_opportunity"],
            0
        )

        out_cols.append("sku_opportunity")
        return df[out_cols]
    
    def calc_share(self, df):
        share_keycols = self.key_cols
        total_col = "total_avg_{}_sales".format(self.level)
        df["share_x_sales"] = df["share"] * df[total_col]
        df_agg = df.groupby(share_keycols, as_index=False).agg({
            "share": ["mean", "max"],
            "share_x_sales": "sum",
            total_col: "sum",
        })

        df_agg.columns = share_keycols + ["other_store_avg_share", "other_store_max_share", "share_x_sales", total_col]
        df_agg["other_store_weighted_avg_share"] = df_agg["share_x_sales"] / df_agg[total_col]
        share_outcols = ["other_store_avg_share", "other_store_max_share", "other_store_weighted_avg_share"]
        
        return df_agg[share_keycols + share_outcols]

    def attach_other_store_share(self, df_assort_sku, df_agg_cat_share):
        global_share = self.calc_share(df_agg_cat_share)
        share_outcols = ["other_store_avg_share", "other_store_max_share", "other_store_weighted_avg_share"]
        rename_map = {}
        for col in share_outcols:
            rename_map[col] = col.replace("other_store_", "global_")
        global_share = global_share.rename(columns=rename_map)

        df_share = df_agg_cat_share[df_agg_cat_share["store_code"].isin(self.stores)]

        list_df = []
        for store in self.stores:
            df_filter = df_assort_sku[df_assort_sku["store_code"]==store]
            df_share_filter = df_share[df_share["store_code"]!=store]
            share_keycols = self.key_cols

            df_share_filter_agg = self.calc_share(df_share_filter)

            df_out = df_filter.merge(df_share_filter_agg, on=share_keycols, how="left")
            list_df.append(df_out)
        
        df_all = pd.concat(list_df)
        df_all_cols = df_all.columns.values.copy()

        df_all_fill = df_all.merge(global_share, on=self.key_cols, how="left")
        for c in share_outcols:
            new_c = c.replace("other_store_", "global_")
            df_all_fill[c] = df_all_fill[c].combine_first(df_all_fill[new_c])

        return df_all_fill[df_all_cols]

    def sku_assortment_adjust(self, df, df_existing_premium):
        df_existing_premium["ind_existing_premium"] = 1
        if self.apply_premium_tag:
            print("| | -- final adjustment")

            premium_skus = self.df_product_premium["sku_code"].unique()

            df["premium_sku"] = np.where(
                df["sku_code"].isin(premium_skus),
                1,
                0
            )

            df["ind_premium_store"] = np.where(
                df["store_code"].isin(self.premium_stores),
                1,
                0
            )

            df = df.merge(df_existing_premium, on=["store_code", "brand", "category"], how="left")

            df_filter = df[
                (df["ind_premium_store"] == 1) |
                ((df["ind_premium_store"] == 0) & (df["premium_sku"] == 0)) |
                (df["ind_existing_premium"] == 1)
            ].reset_index(drop=True)

            df_filter.rename(columns={"sku_rank": "sku_rank_raw"}, inplace=True)
            df_filter.drop("ind_existing_premium", axis=1)

            # adjust rank
            df_out = df_filter.groupby(["store_code", "brand", "category"]).apply(get_rank, "sku_rank", "sku_rank_raw", True)
        else:
            df_out = df
        return df_out

def _sales_agg(df, grp_keys, level):
    if level == "monthly":
        time_col = "month"
    else:
        time_col = "weekstartdate"

    df_agg1 = df.groupby(grp_keys)[[time_col]].nunique().reset_index()
    df_agg2 = df.groupby(grp_keys, as_index=False)[["sales"]].sum()

    df_agg = df_agg1.merge(df_agg2, on=grp_keys, how="left")
    df_agg["avg_{}_sales".format(level)] = df_agg["sales"] / df_agg[time_col]
    return df_agg 

def load_agg_sales(level):
    out_dir = config.adl_dir
    df_agg_cat = pd.read_csv(os.path.join(out_dir, "df_agg_cat_{}.csv".format(level)))
    df_agg_cat_share = pd.read_csv(os.path.join(out_dir, "df_agg_cat_share_{}.csv".format(level)))
    df_agg_cat_range_seg = pd.read_csv(os.path.join(out_dir, "df_agg_cat_range_seg_{}.csv".format(level)))
    df_sku_agg = pd.read_csv(os.path.join(out_dir, "df_sku_agg_{}.csv".format(level)))
    df_agg_cat_all = pd.read_csv(os.path.join(out_dir, "df_agg_cat_all_{}.csv".format(level)))

    return df_agg_cat, df_agg_cat_share, df_agg_cat_range_seg, df_sku_agg, df_agg_cat_all

def save_agg_sales(df_agg_cat, df_agg_cat_share, df_agg_cat_range_seg, df_sku_agg, df_agg_cat_all, level):
    out_dir = config.adl_dir
    df_agg_cat.to_csv(os.path.join(out_dir, "df_agg_cat_{}.csv".format(level)), index=False)
    df_agg_cat_share.to_csv(os.path.join(out_dir, "df_agg_cat_share_{}.csv".format(level)), index=False)
    df_agg_cat_range_seg.to_csv(os.path.join(out_dir, "df_agg_cat_range_seg_{}.csv".format(level)), index=False)
    df_sku_agg.to_csv(os.path.join(out_dir, "df_sku_agg_{}.csv".format(level)), index=False)
    df_agg_cat_all.to_csv(os.path.join(out_dir, "df_agg_cat_all_{}.csv".format(level)), index=False)

def correct_brand_category(df_raw, df_product, df_shelf_raw, chloe_adf, df_brand_type):
    df = df_raw.copy()
    # change below brands' SC to MU
    sc_to_mu_brands = df_brand_type[
        (df_brand_type["category"]=="SKINCARE") & (df_brand_type["brand_type"]=="MU")
    ]["brand"].unique()

    df["category"] = np.where(
        (df["brand"].isin(sc_to_mu_brands)) & (df["category"] == "SKINCARE"),
        "MAKE UP",
        df["category"]
    )

    # CHALING FR to SC
    df["category"] = np.where(
        (df["brand"] == "CHALING") & (df["category"] == "FRAGRANCE"),
        "SKINCARE",
        df["category"]
    )

    # FRESH MU/FR to SC
    df["category"] = np.where(
        (df["brand"] == "FRESH SAS") & (df["category"].isin(["MAKE UP", "FRAGRANCE"])),
        "SKINCARE",
        df["category"]
    )

    # ALGENIST MU to SC
    df["category"] = np.where(
        (df["brand"] == "ALGENIST") & (df["category"].isin(["MAKE UP"])),
        "SKINCARE",
        df["category"]
    )    

    # Break CHLOE FR to 2 brands
    df["brand"] = np.where(
        df["sku_code"].isin(chloe_adf),
        "CHLOE_ADF",
        df["brand"]
    )

    # Break MEN's into one category
    men_brands = df_shelf_raw[df_shelf_raw["pog_name"]=="MEN SC"]["brand"].unique()
    men_skus = df_product[
        (df_product["brand"].isin(men_brands)) & 
        (df_product["category"] == "SKINCARE") &
        (df_product["target"] == "MEN")  
    ]["sku_code"].unique()
    men_skus = list(men_skus) + [436376, 465427]

    df["category"] = np.where(
        df["sku_code"].isin(men_skus),
        "MEN_SC",
        df["category"]
    )

    return df

def identify_new_brand(df_txn, df_product, df_store, level, df_promo, new_brand_cutoff):

    def _calc_diff_month(month_str, max_month):
        year_month = pd.to_datetime(month_str, format="%Y-%m")
        year_diff = max_month.year - year_month.year
        return max_month.month - year_month.month + year_diff * 12

    df = df_txn

    df_merge = df.merge(df_product[["product_id", "brand", "category", "Range", "Segment", "sku_code"]], on=["product_id"]).merge(
        df_store[["store_id", "store_code"]], on=["store_id"])
    
    df_merge = df_merge[~df_merge["product_id"].isin(df_promo["product_id"].unique())]
    df_prep = df_merge[df_merge["sales"] > 1]

    grp_keys = ["store_code", "brand", "category"]
    df_agg1 = df_prep.groupby(grp_keys)[["month"]].nunique().reset_index().rename(columns={"month": "count_month"})
    df_agg2 = df_prep.groupby(grp_keys, as_index=False)[["month"]].min().rename(columns={"month": "min_month"})
    df_agg = df_agg1.merge(df_agg2, on=grp_keys)

    max_date = pd.to_datetime(df["month"].max(), format="%Y-%m")

    df_agg["launch_months"] = df_agg["min_month"].apply(lambda x: _calc_diff_month(x, max_date))
    df_agg["ind_keep_brand"] = np.where(
        df_agg["launch_months"] <= int(new_brand_cutoff) - 1,
        1,
        np.where(
            (df_agg["count_month"] <= 2) & (df_agg["brand"].isin(["MAC"])),
            1,
            0
        )
    )

    df_agg.to_csv(os.path.join(config.adl_dir, "brand_first_purchase_month.csv"), index=False)

    return df_agg

def run_agg_sales(df_txn, df_product, df_store, df_shelf_agg, df_shelf_raw, chloe_adf, level, df_brand_type, rerun=False):
    if not rerun:
        df_agg_cat, df_agg_cat_share, df_agg_cat_range_seg, df_sku_agg, df_agg_cat_all = load_agg_sales(level)
        print("| -- {} sales agg loaded".format(level))
    else:
        df = df_txn

        df_merge = df.merge(df_product[["product_id", "brand", "category", "Range", "Segment", "sku_code"]], on=["product_id"]).merge(
            df_store[["store_id", "store_code"]], on=["store_id"])
        
        df_merge = correct_brand_category(df_merge, df_product, df_shelf_raw, chloe_adf, df_brand_type)

        if level == "monthly":
            df_prep = df_merge[(df_merge["year"]>=2019) & (df_merge["month"]!="2020-02")]
        else:
            df_merge["weekstartdate"] = pd.to_datetime(df_merge["weekstartdate"], format="%Y-%m-%d")
            df_merge["year"] = df_merge["weekstartdate"].dt.year
            df_merge["weekstartdate"] = df_merge["weekstartdate"].dt.strftime("%Y-%m-%d")

            exclude_weeks = ["2020-01-27", "2020-02-03", "2020-02-10", "2020-02-17", "2020-02-24", "2020-03-02"]

            df_prep = df_merge[
                (df_merge["year"]>=2019) & 
                ~(df_merge["weekstartdate"].isin(exclude_weeks))
            ]

        df_prep = df_prep[df_prep["sales"]>1]

        grp_keys1 = ["store_code", "brand", "category"]
        df_agg_cat_all = _sales_agg(df_prep, grp_keys1, level)

        # filter no shelf
        df_filter = df_prep.merge(df_shelf_agg[grp_keys1 + ["shelf"]], on=grp_keys1, how="left")
        df_filter = df_filter[~pd.isnull(df_filter["shelf"])]
        df_agg_cat = _sales_agg(df_filter, grp_keys1, level)

        # aggregation
        sales_col = "avg_{}_sales".format(level)

        df_agg_cat_sum = df_agg_cat.groupby(["store_code"], as_index=False)[[sales_col]].sum().rename(
            columns={sales_col: "total_" + sales_col})
        df_agg_cat_share = df_agg_cat.merge(df_agg_cat_sum, on=["store_code"], how="left")
        df_agg_cat_share["share"] = df_agg_cat_share[sales_col] / df_agg_cat_share["total_" + sales_col]

        grp_keys2 = ["store_code", "brand", "category", "Range", "Segment"]
        df_agg_cat_range_seg = _sales_agg(df_filter, grp_keys2, level)

        by_sku = ["store_code", "brand", "category", "Range", "Segment", "sku_code"]
        df_sku_agg = _sales_agg(df_filter, by_sku, level)

        save_agg_sales(df_agg_cat, df_agg_cat_share, df_agg_cat_range_seg, df_sku_agg, df_agg_cat_all, level)
        print("| -- {} sales aggregated".format(level))
    return df_agg_cat, df_agg_cat_share, df_agg_cat_range_seg, df_sku_agg, df_agg_cat_all

def cluster_brand_profile(df, df_cluster, df_brand_type, level):
    sales_col = "avg_{}_sales".format(level)
    cat_sales_col = "avg_{}_sales_by_cat".format(level)

    df_merge = df.merge(df_cluster, on=["store_code"]).merge(df_brand_type, on=["brand", "category"])

    by_cat = ["cluster", "store_code", "category"]
    df_agg1 = df_merge.groupby(by_cat, as_index=False)[[sales_col]].sum().rename(
        columns={sales_col: cat_sales_col}
    )

    df_agg2 = df_merge.groupby(by_cat + ["brand_type"], as_index=False)[[sales_col]].sum()

    df_rank = df_agg2.merge(df_agg1, on=by_cat)
    df_rank["brand_type_share"] = df_rank[sales_col] / df_rank[cat_sales_col]
    df_rank.to_csv("~/xingyu/df_rank.csv", index=False)

    df_rank_agg = df_rank.groupby(["cluster", "category", "brand_type"], as_index=False)[["brand_type_share"]].mean()

    df_rank_agg = df_rank_agg.groupby(["category", "brand_type"], as_index=False).apply(
        get_rank, "brand_type_share_rank", "brand_type_share")
    
    df_rank_agg_merge = df_rank_agg.merge(df_brand_type, on=["category", "brand_type"], how="left")

    df_rank_agg_merge.to_csv(os.path.join(config.model_dir, "cluster", "cluster_brand_type_share_rank_{}.csv".format(level)), index=False)

    return df_rank_agg_merge

def get_shelf(df_raw, df_product, df_mini, df_mapping, df_shelf_opp):
    max_week = df_mini["weekstartdate"].max()
    df_mini = df_mini[df_mini["weekstartdate"]==max_week]

    max_weekstartdate = df_raw["weekstartdate"].max()

    df_shelf = df_raw.merge(
        df_mapping,
        on=["brand", "pog_name"],
        how="left"
    )

    df_shelf["brand"] = df_shelf["brand"].str.upper()
    
    df_shelf_raw = df_shelf[
        (df_shelf["weekstartdate"]==max_weekstartdate) & 
        (df_shelf["category"].isin(["SKINCARE", "MAKE UP", "FRAGRANCE"]))
    ]

    df_shelf_filter = df_shelf_raw[
        (df_shelf_raw["pog_name"] != "MEN SC")
    ]

    grp_keys = ["store_code", "brand", "category"]
    df_shelf_agg = df_shelf_filter.groupby(grp_keys, as_index=False)[["shelf"]].sum()

    # ---- Break CHLOE ADF shelf ---- #
    df_chloe = df_product[
        (df_product["brand"]=="CHLOE") & 
        (df_product["category"]=="FRAGRANCE")
    ]
    df_chloe["ind_adf"] = df_chloe["product_name_en"].apply(lambda x: 1 if "ATELIER DES FLEURS" in x else 0)
    chloe_adf = df_chloe[df_chloe["ind_adf"]==1]["sku_code"].unique()
    stores_with_chloe_adf = df_mini[df_mini["sapcode"].isin(chloe_adf)]["store"].unique()

    df_shelf_agg_no_chloe = df_shelf_agg[
        (df_shelf_agg["brand"] != "CHLOE") |
        ((df_shelf_agg["brand"] == "CHLOE") & ~(df_shelf_agg["store_code"].isin(stores_with_chloe_adf)))
    ]

    df_chloe = df_shelf_agg[
        (df_shelf_agg["brand"] == "CHLOE") & 
        (df_shelf_agg["store_code"].isin(stores_with_chloe_adf))
    ].rename(columns={"shelf": "shelf_total"})
    df_chloe["shelf"] = df_chloe["shelf_total"] / 2

    df_chloe_adf = df_chloe.copy().rename(columns={"shelf": "shelf_mass"})
    df_chloe_adf.loc[:, "brand"] = "CHLOE_ADF"
    df_chloe_adf["shelf"] = df_chloe_adf["shelf_total"] - df_chloe_adf["shelf_mass"]

    need_cols = grp_keys + ["shelf"]
    df_shelf_agg = pd.concat([df_shelf_agg_no_chloe[need_cols], df_chloe[need_cols], df_chloe_adf[need_cols]]).reset_index(drop=True)
    # ---- Break CHLOE ADF shelf ---- #

    all_shelf_opp = []
    for store in df_shelf_agg["store_code"].unique():
        df_tmp = df_shelf_opp.copy()
        df_tmp["store_code"] = store
        all_shelf_opp.append(df_tmp)
    
    df_all_shelf_opp = pd.concat(all_shelf_opp)
    df_all_brand_cat = pd.concat([df_shelf_agg[grp_keys].drop_duplicates(), df_all_shelf_opp[grp_keys].drop_duplicates()]).drop_duplicates()

    df_out = df_all_brand_cat.merge(df_shelf_agg, on=grp_keys, how="left").merge(
        df_all_shelf_opp, on=grp_keys, how="left"
    )

    df_out["ind_opportunity"] = np.where(
        (~pd.isnull(df_out["shelf"]) & (df_out["tagging"]!="Already removed")) | (df_out["tagging"]=="Opportunity"),
        1,
        0
    )

    df_out["ind_already_remove"] = np.where(
        df_out["tagging"]=="Already removed",
        1,
        0
    )

    df_out.to_csv(os.path.join(config.sdl_dir, "space_shelf_meter_opportunity.csv"), index=False)
    return df_out, df_shelf_raw, chloe_adf

def get_available_shelf(buffer, df):
    available_shelf_dict = {}
    for i in df.values:
        available_shelf_dict[i[0]] = max(0, int(i[1]) - buffer)

    return available_shelf_dict

def get_premium_sku(df_product, premium_franchise):
    all_premium_sku = []
    for k,v in premium_franchise.items():
        for line_name in v:
            df_sel = df_product[
                (df_product["category"].isin(["SKINCARE", "MAKE UP"])) & 
                (df_product["brand"]==k) & 
                (df_product["product_name"].str.contains(line_name))
            ]
            all_premium_sku.append(df_sel)
    
    df_product_premium = pd.concat(all_premium_sku)[["sku_code"]].drop_duplicates()
    df_product_premium["sku_code"] = df_product_premium["sku_code"].astype("int").astype("str")
    df_product_premium["premium_sku"] = 1
    return df_product_premium

def run_assortment(cluster_version, brand_type_version, rerun_sales_agg, level, config_file, version=""):
    print("| -- load config for assortment")
    root_dir = os.getcwd()
    config_path = os.path.join(root_dir, "src", config_file)
    conf = load_config(config_path)
    apply_cannot_add = conf["apply_cannot_add"]
    premium_franchise = conf["premium_franchise"]

    print("| -- read cleaned tables")
    df_txn = pd.read_parquet(os.path.join(config.sdl_dir, "txn_store_sku_{}_v2.parquet".format(level)))
    df_store = pd.read_excel(os.path.join(config.ref_dir, "dim_stores_offline_v5.xlsx"), engine="openpyxl")
    df_promo = pd.read_excel(os.path.join(config.ref_dir, "promo_dim_product.xlsx"), engine="openpyxl")

    df_mini = pd.read_parquet(os.path.join(config.raw_processed, "cn_mini_weekly.parquet"))
    df_mini = df_mini[df_mini["store"].isin(df_store["store_code"].unique())]
    
    df_pog = pd.read_csv(os.path.join(config.raw_processed, "shelf_product.csv"))
    df_pog = df_pog[df_pog["on_pog_flag"] == "Y"]

    df_product = pd.read_excel(os.path.join(config.ref_dir, "dim_product_with_txn.xlsx"), engine="openpyxl")
    for col in ["Range", "Segment"]:
        df_product[col].fillna("_NaN", inplace=True)
    df_space_shelf_meter = pd.read_csv(os.path.join(config.raw_processed, "space_shelf_meter.csv")).rename(columns={
        "category": "category_from_space"
    })

    print("| -- read manual inputs")
    df_brand_type = pd.read_csv(os.path.join(config.input_dir, "dim_brand_type_revised_{}.csv".format(brand_type_version)))
    df_store_cluster = pd.read_csv(os.path.join(config.model_dir, "cluster", "store_cluster_{}.csv".format(cluster_version)))
    df_existing_premium = pd.read_csv(os.path.join(config.input_dir, "dim_existing_premium_stores.csv"))
    df_mapping = pd.read_csv(os.path.join(config.input_dir, "shelf_pog_brand_category_mapping.csv"))
    df_avail_shelf = pd.read_csv(os.path.join(config.input_dir, "available_shelf.csv"))
    df_shelf_opp = pd.read_csv(os.path.join(config.input_dir, "dim_shelf_opportunity.csv"))

    df_shelf_agg, df_shelf_raw, chloe_adf = get_shelf(df_space_shelf_meter, df_product, df_mini, df_mapping, df_shelf_opp)
    df_product_premuim = get_premium_sku(df_product, premium_franchise)    

    new_stores = df_store[df_store["open_date"] >= conf["new_store_cutoff"]]["store_code"].unique()
    outlet_stores = list(df_store_cluster[df_store_cluster["ind_outlet"]==1]["store_code"].unique())
    premium_stores = [99999] #list(df_store_cluster[df_store_cluster["ind_premium"]==1]["store_code"].unique())
    available_shelf_dict = get_available_shelf(conf["available_shelf_buffer"], df_avail_shelf)

    print("| -- calculate avg {} sales".format(level))
    df_agg_cat, df_agg_cat_share, df_agg_cat_range_seg, df_sku_agg, df_agg_cat_all = run_agg_sales(
        df_txn, df_product, df_store, df_shelf_agg, df_shelf_raw, chloe_adf, level, df_brand_type, rerun=rerun_sales_agg)
    df_cluster_brand_rank = cluster_brand_profile(df_agg_cat_share, df_store_cluster, df_brand_type, level)
    df_brand_first = identify_new_brand(df_txn, df_product, df_store, level, df_promo, conf["new_brand_period"])
    
    base_dir = os.path.join(config.model_dir, "assortment")
    ts = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    out_dir = os.path.join(base_dir, ts + "_{}_{}".format(level, version))
    os.mkdir(out_dir)

    # print("| -- run eb opportunity")
    # run_eb_opportunity(out_dir, df_shelf_agg, df_store)

    all_assort = []
    for i, cluster in enumerate(list(df_store_cluster["cluster"].unique())):
        print("| -- running assortment for {} cluster".format(cluster))
        stores = list(df_store_cluster[df_store_cluster["cluster"]==cluster]["store_code"].unique())
        cluster_assort = StoreAssortment(
            level, conf, cluster, stores, df_cluster_brand_rank, df_agg_cat_all, df_shelf_agg, df_brand_first, 
            available_shelf_dict, outlet_stores, new_stores, premium_stores, df_product_premuim, apply_cannot_add
        )
        df_assort_add = cluster_assort.add_item()

        df_assort_add_remove = cluster_assort.remove_item(df_assort_add)
        df_assort_add_remove.to_csv(os.path.join(out_dir, "{}_assortment_add_remove.csv".format(cluster)), index=False)

        df_sku_assort = cluster_assort.rank_sku(df_product, df_assort_add_remove, df_agg_cat_range_seg, df_sku_agg, df_mini, df_pog)
        df_sku_assort_out = cluster_assort.attach_other_store_share(df_sku_assort, df_agg_cat_share)
        df_sku_assort_out = cluster_assort.sku_assortment_adjust(df_sku_assort_out, df_existing_premium)
        df_sku_assort_out["cluster"] = cluster
        all_assort.append(df_sku_assort_out)

        if i == len(df_store_cluster["cluster"].unique()) - 1:
            with open(os.path.join(out_dir, "assortment_config.json"), "w") as f:
                json.dump(cluster_assort.config, f, indent=4)
    
    df_all_assort = pd.concat(all_assort)
    df_all_assort.to_csv(os.path.join(out_dir, "cluster_assortment_sku.csv"), index=False)
    excl_cols = [col for col in df_all_assort.columns if "sku" in col] + ["Range", "Segment", "mini"]
    need_cols = [col for col in df_all_assort.columns if col not in excl_cols]
    df_summary = df_all_assort[need_cols].drop_duplicates()
    df_summary.to_csv(os.path.join(out_dir, "cluster_assortment_summary.csv"), index=False)

    # adjust
    df_summary_adjust = run_adjust(df_summary, 3)
    df_summary_adjust.to_csv(os.path.join(out_dir, "cluster_assortment_summary_adjust.csv"), index=False)
    df_all_assort_adjust = df_all_assort.merge(
        df_summary_adjust[["store_code", "brand", "category", "assortment_adjust"]],
        on=["store_code", "brand", "category"],
        how="left"
    )
    df_all_assort_adjust.to_csv(os.path.join(out_dir, "cluster_assortment_sku_adjust.csv"), index=False)

if __name__ == "__main__":
    if len(sys.argv) != 6:
        print("Wrong # of arguments. Usage: python3 src/model/assortment_engine.py <cluster version> <brand type version> <rerun_agg / x> <config file> <version>")
    else:
        if sys.argv[3] == "rerun_agg":
            rerun_sales_agg = True
        else:
            rerun_sales_agg = False
        
        run_assortment(sys.argv[1], sys.argv[2], rerun_sales_agg, "monthly", sys.argv[4], sys.argv[5])