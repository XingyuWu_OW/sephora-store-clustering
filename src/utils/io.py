import json
import pandas as pd

def read_excel(filepath, sheet_name=None, skiprows=None):
    """
    Read excel
    """
    if sheet_name is not None:
        df = pd.read_excel(filepath, engine="openpyxl", sheet_name=sheet_name, skiprows=skiprows)
    else:
        df = pd.read_excel(filepath, engine="openpyxl", skiprows=skiprows)
    return df


def read_txt(filepath, encoding="utf-16", header_cols=None):
    """
    Read TXT with specific encoding
    """
    if header_cols is not None:
        df = pd.read_table(filepath, encoding=encoding, sep="\t", header=0, names=headers_cols)
    else:
        df = pd.read_table(filepath, encoding=encoding, sep="\t")
    
    return df

def load_config(filepath):
    """
    Load JSON configration file
    """
    with open(filepath, encoding="utf-8") as f:
        return json.load(f)
