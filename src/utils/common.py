import os
import pandas as pd

def convert_int(x):
    try:
        return int(x)
    except:
        return -99999999

def get_rank(df, rank_col, value_col, ascending=False):
    """
    Get ranking based on a column
    """
    df = df.sort_values(by=[value_col], ascending=ascending).reset_index(drop=True)
    df[rank_col] = df.index + 1
    return df

def reduce_size(df):
    """
    Reduce consumed memory size by altering data types for Pandas DataFrame
    """
    sel_cols = ['txn_ts', 'txn_date', 'txn_id', 'product_id', 'store_id', 'customer_id', 'qty', 
    'sales_at_fullprice', 'sales', 'raw_sales', 'discount', 'discount_pct']
    df = df[sel_cols]
    df["store_id"] = df["store_id"].astype("uint16")
    df["qty"] = df["qty"].astype("int32")
    df["product_id"] = df["product_id"].astype("uint32")
    df["customer_id"] = df["customer_id"].astype("uint32")
    df["txn_id"] = df["txn_id"].astype("uint32")
    print("| -- dtypes casted...")
    return df