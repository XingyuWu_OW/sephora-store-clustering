# Sephora Store Clustering and Assortment Optimization

## Prerequisites
- Python 3.6+
- Python packages `./environment/requirements.txt`
- tmux
- vim
- azure-cli

## Environment variables
Add common environment variables:    
```vi ~/.env```

Insert data root dir and azure blob connection string into the file
```
export DATA_DIR="/data/"
export AZURE_STORAGE_CONNECTION_STRING="xxx"
```
Save and exit

## Add project dir to python path
1. Edit `.bashrc` under home folder  
```vi ~/.bashrc```

2. Insert below line in the file  
```export PYTHONPATH=$PYTHONPATH:/PATH_TO_YOUR_PROJECT_ROOT_DIR/```

    where `PATH_TO_YOUR_PROJECT_ROOT_DIR` is the path of your project root dir, for example `/home/user_a/sephora-store-clustering`

3. Insert below line to load environment variables    
```source ~/.env```

4. Save and exit

5. Then run:
```source ~/.bashrc```

## How to run scripts
At project root dir, run scripts using below command separately
```
python3 src/input/dim_date.py
```

Alternatively, call pipeline script by
```
bash src/run_pipeline.sh
```